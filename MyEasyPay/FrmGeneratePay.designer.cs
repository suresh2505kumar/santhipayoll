﻿namespace MyEasyPay
{
    partial class FrmGeneratePay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.BtnExcelExport = new System.Windows.Forms.Button();
            this.BtnView = new System.Windows.Forms.Button();
            this.CmbMonth = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DataGridPay = new System.Windows.Forms.DataGridView();
            this.BtnExit = new System.Windows.Forms.Button();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPay)).BeginInit();
            this.SuspendLayout();
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.BtnExit);
            this.GrFront.Controls.Add(this.DataGridPay);
            this.GrFront.Controls.Add(this.BtnExcelExport);
            this.GrFront.Controls.Add(this.BtnView);
            this.GrFront.Controls.Add(this.CmbMonth);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(9, 2);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1052, 486);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // BtnExcelExport
            // 
            this.BtnExcelExport.Location = new System.Drawing.Point(450, 17);
            this.BtnExcelExport.Name = "BtnExcelExport";
            this.BtnExcelExport.Size = new System.Drawing.Size(113, 26);
            this.BtnExcelExport.TabIndex = 5;
            this.BtnExcelExport.Text = "Export to Excel";
            this.BtnExcelExport.UseVisualStyleBackColor = true;
            this.BtnExcelExport.Click += new System.EventHandler(this.Button1_Click);
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(282, 17);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(84, 26);
            this.BtnView.TabIndex = 3;
            this.BtnView.Text = "Generate";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // CmbMonth
            // 
            this.CmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMonth.FormattingEnabled = true;
            this.CmbMonth.Location = new System.Drawing.Point(71, 17);
            this.CmbMonth.Name = "CmbMonth";
            this.CmbMonth.Size = new System.Drawing.Size(205, 26);
            this.CmbMonth.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Month";
            // 
            // DataGridPay
            // 
            this.DataGridPay.AllowUserToAddRows = false;
            this.DataGridPay.BackgroundColor = System.Drawing.Color.White;
            this.DataGridPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridPay.Location = new System.Drawing.Point(6, 49);
            this.DataGridPay.Name = "DataGridPay";
            this.DataGridPay.RowHeadersVisible = false;
            this.DataGridPay.Size = new System.Drawing.Size(1041, 431);
            this.DataGridPay.TabIndex = 6;
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(962, 21);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(84, 26);
            this.BtnExit.TabIndex = 7;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // FrmGeneratePay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1068, 499);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmGeneratePay";
            this.Text = "Generate Pay";
            this.Load += new System.EventHandler(this.FrmGeneratePay_Load);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.ComboBox CmbMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnView;
        private System.Windows.Forms.Button BtnExcelExport;
        private System.Windows.Forms.DataGridView DataGridPay;
        private System.Windows.Forms.Button BtnExit;
    }
}