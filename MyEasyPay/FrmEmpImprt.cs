﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmEmpImprt : Form
    {
        public FrmEmpImprt()
        {
            InitializeComponent();
        }

        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SQLDBHelper db = new SQLDBHelper();

        SqlCommand qur = new SqlCommand();
        private void FrmEmpImprt_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

        }


        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbClass.Text == "Update Pan Number" || cmbClass.Text == "Update Blood Group" || cmbClass.Text == "Update Aadhar Number" || cmbClass.Text == "Update UAN Number" || cmbClass.Text == "Update Passport Number")
                {
                    ImportEmployee(connstring, txtSearch.Text);
                }
                else if (cmbClass.Text == "Update Bank Details")
                {
                    ImportEmployee(connstring, txtSearch.Text);
                }
                else
                {
                    ImportStudentClassWise(connstring, txtSearch.Text);
                    updateemp();
                    grpimport.Visible = true;
                    imprtgrid();
                }
                grStudentImport.Visible = false;
                grpimport.Visible = true;
                ChckcMismatchEmployee_CheckedChanged(sender, e);
            }
            catch (Exception)
            {

                throw;
            }

        }
        private void updateemp()
        {
            qur.Connection = conn;
            conn.Close();
            conn.Open();
            qur.CommandText = "exec sp_uploadempmast ";
            qur.ExecuteNonQuery();


        }
        private void imprtgrid()
        {

            conn.Close();
            conn.Open();

            string quy = "exec sp_empgird";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            if (tab.Rows.Count > 0)
            {

                MessageBox.Show("Check the Department or Designation or Employee type");

            }

            Importgrid.AutoGenerateColumns = false;
            Importgrid.Refresh();
            Importgrid.DataSource = null;
            Importgrid.Rows.Clear();

            Importgrid.RowHeadersVisible = false;
            Importgrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            Importgrid.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            Importgrid.ColumnCount = 17;

            Importgrid.Columns[0].Name = "slno";
            Importgrid.Columns[0].HeaderText = "slno";
            Importgrid.Columns[0].DataPropertyName = "slno";
            Importgrid.Columns[0].Width = 50;


            Importgrid.Columns[1].Name = "empcode";
            Importgrid.Columns[1].HeaderText = "EmpCode";
            Importgrid.Columns[1].DataPropertyName = "empcode";
            Importgrid.Columns[1].Width = 100;

            Importgrid.Columns[2].Name = "EMpname";
            Importgrid.Columns[2].HeaderText = "Employee Name";
            Importgrid.Columns[2].DataPropertyName = "EMpname";
            Importgrid.Columns[2].Width = 120;


            Importgrid.Columns[3].Name = "FatherName";
            Importgrid.Columns[3].HeaderText = "FatherName";
            Importgrid.Columns[3].DataPropertyName = "FatherName";
            Importgrid.Columns[3].Width = 100;

            Importgrid.Columns[4].Name = "DOB";
            Importgrid.Columns[4].HeaderText = "DOB";
            Importgrid.Columns[4].DataPropertyName = "DOB";
            Importgrid.Columns[4].Width = 100;

            Importgrid.Columns[5].Name = "DOJ";
            Importgrid.Columns[5].HeaderText = "DOJ";
            Importgrid.Columns[5].DataPropertyName = "DOJ";
            Importgrid.Columns[5].Width = 100;

            Importgrid.Columns[6].Name = "SEX";
            Importgrid.Columns[6].HeaderText = "SEX";
            Importgrid.Columns[6].DataPropertyName = "SEX";
            Importgrid.Columns[6].Width = 60;

            Importgrid.Columns[7].Name = "Emptype";
            Importgrid.Columns[7].HeaderText = "Emptype";
            Importgrid.Columns[7].DataPropertyName = "Emptype";
            Importgrid.Columns[7].Width = 60;

            Importgrid.Columns[8].Name = "Department";
            Importgrid.Columns[8].HeaderText = "Department";
            Importgrid.Columns[8].DataPropertyName = "Department";
            Importgrid.Columns[8].Width = 100;
            Importgrid.Columns[9].Name = "Designation";
            Importgrid.Columns[9].HeaderText = "Designation";
            Importgrid.Columns[9].DataPropertyName = "Designation";
            Importgrid.Columns[9].Width = 100;
            Importgrid.Columns[10].Name = "category";
            Importgrid.Columns[10].HeaderText = "category";
            Importgrid.Columns[10].DataPropertyName = "category";
            Importgrid.Columns[10].Width = 100;
            Importgrid.Columns[11].Name = "PFESI";
            Importgrid.Columns[11].HeaderText = "PFESI";
            Importgrid.Columns[11].DataPropertyName = "PFESI";
            Importgrid.Columns[11].Width = 100;
            Importgrid.Columns[12].Name = "GrossSalary";
            Importgrid.Columns[12].HeaderText = "GrossSalary";
            Importgrid.Columns[12].DataPropertyName = "GrossSalary";
            Importgrid.Columns[12].Width = 100;
            Importgrid.Columns[13].Name = "Aadhar";
            Importgrid.Columns[13].HeaderText = "Aadhar";
            Importgrid.Columns[13].DataPropertyName = "Aadhar";
            Importgrid.Columns[13].Width = 100;
            Importgrid.Columns[14].Name = "PanNo";
            Importgrid.Columns[14].HeaderText = "PanNo";
            Importgrid.Columns[14].DataPropertyName = "PanNo";
            Importgrid.Columns[14].Width = 100;
            Importgrid.Columns[15].Name = "Aaddress";
            Importgrid.Columns[15].HeaderText = "Aaddress";
            Importgrid.Columns[15].DataPropertyName = "Aaddress";
            Importgrid.Columns[15].Width = 100;
            Importgrid.Columns[16].Name = "PunchNo";
            Importgrid.Columns[16].HeaderText = "PunchNo";
            Importgrid.Columns[16].DataPropertyName = "PunchNo";
            Importgrid.Columns[16].Width = 100;

            Importgrid.DataSource = tab;



        }
        protected void ImportStudentClassWise(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();

                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    new DataColumn("SlNo", typeof(float));
                    new DataColumn("EmpCode", typeof(float));
                    new DataColumn("EmpName", typeof(string));
                    new DataColumn("FatherName", typeof(string));
                    new DataColumn("DOB", typeof(DateTime));
                    new DataColumn("DOJ", typeof(DateTime));
                    new DataColumn("Sex", typeof(string));
                    new DataColumn("Type", typeof(string));
                    new DataColumn("Department", typeof(string));
                    new DataColumn("Desingnation", typeof(string));
                    new DataColumn("Category", typeof(string));
                    new DataColumn("esipf", typeof(string));
                    new DataColumn("GrossSalary", typeof(float));
                    new DataColumn("Aadhar", typeof(string));
                    new DataColumn("PanNo", typeof(string));
                    new DataColumn("Address", typeof(string));
                    new DataColumn("PunchNo", typeof(string));

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        string query = "truncate table  ImportEMP ";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();

                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.ImportEMP";
                            sqlbulkcopy.ColumnMappings.Add("SlNo", "SLNo");
                            sqlbulkcopy.ColumnMappings.Add("EmpCode", "EMpCode");
                            sqlbulkcopy.ColumnMappings.Add("EmpName", "EmpName");
                            sqlbulkcopy.ColumnMappings.Add("FatherName", "FatherName");
                            sqlbulkcopy.ColumnMappings.Add("DOB", "DOB");
                            sqlbulkcopy.ColumnMappings.Add("DOJ", "DOJ");
                            sqlbulkcopy.ColumnMappings.Add("Sex", "Sex");
                            sqlbulkcopy.ColumnMappings.Add("Type", "EMPType");
                            sqlbulkcopy.ColumnMappings.Add("Department", "Department");
                            sqlbulkcopy.ColumnMappings.Add("Desingnation", "Designation");
                            sqlbulkcopy.ColumnMappings.Add("Category", "Category");
                            sqlbulkcopy.ColumnMappings.Add("esipf", "PFESI");
                            sqlbulkcopy.ColumnMappings.Add("GrossSalary", "GrossSalary");
                            sqlbulkcopy.ColumnMappings.Add("Aadhar", "Aadhar");
                            sqlbulkcopy.ColumnMappings.Add("PanNo", "PanNo");
                            sqlbulkcopy.ColumnMappings.Add("Address", "Address");
                            sqlbulkcopy.ColumnMappings.Add("PunchNo", "PunchNo");
                            conn.Open();
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "All Excel|*.xlsx";
            dialog.ShowDialog();
            txtSearch.Text = dialog.FileName;
            txtSearch.Visible = true;
            string extensionpath = Path.GetExtension(dialog.FileName);
            switch (extensionpath)
            {
                case ".xls":
                    connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx":
                    connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    break;
            }

        }

        private void FrmEmpImprt_Load_1(object sender, EventArgs e)
        {
            qur.Connection = conn;
            grpimport.Visible = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            grpimport.Visible = false;
            grStudentImport.Visible = true;
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbClass.Text == "Update Pan Number")
                {
                    lblFormat.Visible = true;
                }
                else if (cmbClass.Text == "Update Aadhar Number")
                {
                    lblFormat.Visible = true;
                }
                else if (cmbClass.Text == "Update UAN Number")
                {
                    lblFormat.Visible = true;
                }
                else if (cmbClass.Text == "Update Passport Number")
                {
                    lblFormat.Visible = true;
                }
                else if (cmbClass.Text == "Update Blood Group")
                {
                    lblFormat.Visible = true;
                }
                else if (cmbClass.Text == "Update Bank Details")
                {
                    lblFormat.Visible = true;
                }
                else
                {
                    lblFormat.Visible = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LblFormat_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                row1.Value = "EmpCode";

                if (cmbClass.Text == "Update Pan Number")
                {
                    worksheet.Name = "Update Pan";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    row2.Value = "PanNumber";
                }
                else if (cmbClass.Text == "Update Aadhar Number")
                {
                    worksheet.Name = "Update Aadhar";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    row2.Value = "AadharNumber";
                }
                else if (cmbClass.Text == "Update UAN Number")
                {
                    worksheet.Name = "Update Aadhar";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    row2.Value = "AadharNumber";
                }
                else if (cmbClass.Text == "Update Passport Number")
                {
                    worksheet.Name = "Update Aadhar";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    row2.Value = "AadharNumber";
                }
                else if (cmbClass.Text == "Update Blood Group")
                {
                    worksheet.Name = "Update BloodGroup";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    row2.Value = "BloodGroup";
                }
                else if (cmbClass.Text == "Update Bank Details")
                {
                    worksheet.Name = "Update Bank Details";
                    Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                    Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                    Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                    Microsoft.Office.Interop.Excel.Range row5 = worksheet.Rows.Cells[1, 5];
                    Microsoft.Office.Interop.Excel.Range row6 = worksheet.Rows.Cells[1, 6];
                    row2.Value = "Bank";
                    row3.Value = "Branch";
                    row4.Value = "Name";
                    row5.Value = "AcNumber";
                    row6.Value = "IfscCode";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ImportEmployee(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();
                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    if (cmbClass.Text == "Update Pan Number" || cmbClass.Text == "Update Blood Group" || cmbClass.Text == "Update Aadhar Number" || cmbClass.Text == "Update UAN Number" || cmbClass.Text == "Update Passport Number")
                    {
                        new DataColumn("EmpCode", typeof(string));
                        new DataColumn("F1", typeof(string));
                        using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                        {
                            oda.Fill(dtexcelData);
                        }
                        excel_Conn.Close();
                        string connString = conn.ConnectionString;
                        using (SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + ""))
                        {
                            string query = "Truncate table ImportEmpData";
                            conn.Open();
                            SqlCommand cmd = new SqlCommand(query, conn);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                            conn.Close();

                            using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                            {
                                sqlbulkcopy.DestinationTableName = "dbo.ImportEmpData";
                                sqlbulkcopy.ColumnMappings.Add("EmpCode", "EmpCode");
                                sqlbulkcopy.ColumnMappings.Add("F1", "F1");
                                conn.Open();
                                sqlbulkcopy.BulkCopyTimeout = 600;
                                sqlbulkcopy.WriteToServer(dtexcelData);
                                conn.Close();
                            }
                        }
                    }
                    else if (cmbClass.Text == "Update Bank Details")
                    {
                        new DataColumn("EmpCode", typeof(string));
                        new DataColumn("Bank", typeof(string));
                        new DataColumn("Branch", typeof(string));
                        new DataColumn("Name", typeof(string));
                        new DataColumn("AcNumber", typeof(string));
                        new DataColumn("IfscCode", typeof(string));
                        using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                        {
                            oda.Fill(dtexcelData);
                        }
                        excel_Conn.Close();
                        string connString = conn.ConnectionString;
                        using (SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + ""))
                        {
                            string query = "Truncate table ImportEmpData";
                            conn.Open();
                            SqlCommand cmd = new SqlCommand(query, conn);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                            conn.Close();

                            using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                            {
                                sqlbulkcopy.DestinationTableName = "dbo.ImportEmpData";
                                sqlbulkcopy.ColumnMappings.Add("EmpCode", "EmpCode");
                                sqlbulkcopy.ColumnMappings.Add("Bank", "F1");
                                sqlbulkcopy.ColumnMappings.Add("Branch", "F2");
                                sqlbulkcopy.ColumnMappings.Add("Name", "F3");
                                sqlbulkcopy.ColumnMappings.Add("AcNumber", "F4");
                                sqlbulkcopy.ColumnMappings.Add("IfscCode", "F5");
                                conn.Open();
                                sqlbulkcopy.BulkCopyTimeout = 600;
                                sqlbulkcopy.WriteToServer(dtexcelData);
                                conn.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckcMismatchEmployee_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dataTable = new DataTable();
                if(chckcMismatchEmployee.Checked == true)
                {
                    string Query = "Select EmpCode,F1,F2,F3,F4,F5 from ImportEmpData Where EmpCode Not in(Select EmpCode From Emp_mast)";
                    dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                }
                else
                {
                    string Query = "Select a.EmpCode,b.EmpName,F1,F2,F3,F4,F5 from ImportEmpData a Inner join Emp_mast b on a.Empcode = b.EmpCode order by a.EmpCode";
                    dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                }
                Importgrid.DataSource = null;
                Importgrid.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
