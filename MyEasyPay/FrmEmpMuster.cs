﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;


namespace MyEasyPay
{
    public partial class FrmEmpMuster : Form
    {
        public FrmEmpMuster()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();

        private void FrmEmpMuster_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            LoadMasters();
            Loadmonth();
            DataGridEmpPay.RowHeadersVisible = false;
            DataGridEmpPay.ReadOnly = false;
        }

        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                cmgfntbox.DataSource = null;
                cmgfntbox.DisplayMember = "DeptName";
                cmgfntbox.ValueMember = "DeptId";
                cmgfntbox.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                cmbmonth.DataSource = null;
                cmbmonth.DisplayMember = "PMonth";
                cmbmonth.ValueMember = "monid";
                cmbmonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void LoadPayDetails()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@MonId",cmbmonth.SelectedValue),
                    new SqlParameter("@Deptid",cmgfntbox.SelectedValue),
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GenerateMusterTemp", sqlParameters, conn);
                DataGridEmpPay.DataSource = null;
                DataGridEmpPay.AutoGenerateColumns = false;
                DataGridEmpPay.ColumnCount = 9;

                DataGridEmpPay.Columns[0].Name = "monid";
                DataGridEmpPay.Columns[0].HeaderText = "monid";
                DataGridEmpPay.Columns[0].DataPropertyName = "monid";
                DataGridEmpPay.Columns[0].Visible = false;

                DataGridEmpPay.Columns[1].Name = "EmpId";
                DataGridEmpPay.Columns[1].HeaderText = "EmpId";
                DataGridEmpPay.Columns[1].DataPropertyName = "EmpId";
                DataGridEmpPay.Columns[1].Visible = false;

                DataGridEmpPay.Columns[2].Name = "Empcode";
                DataGridEmpPay.Columns[2].HeaderText = "Empcode";
                DataGridEmpPay.Columns[2].DataPropertyName = "Empcode";
                DataGridEmpPay.Columns[2].Width = 80;

                DataGridEmpPay.Columns[3].Name = "Empname";
                DataGridEmpPay.Columns[3].HeaderText = "Employee Name";
                DataGridEmpPay.Columns[3].DataPropertyName = "Empname";
                DataGridEmpPay.Columns[3].Width = 200;

                DataGridEmpPay.Columns[4].Name = "Pre";
                DataGridEmpPay.Columns[4].HeaderText = "Pre";
                DataGridEmpPay.Columns[4].DataPropertyName = "Pre";
                DataGridEmpPay.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[4].Width = 50;

                DataGridEmpPay.Columns[5].Name = "LOP";
                DataGridEmpPay.Columns[5].HeaderText = "LOP";
                DataGridEmpPay.Columns[5].DataPropertyName = "LOP";
                DataGridEmpPay.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[5].Width = 60;

                DataGridEmpPay.Columns[6].Name = "CL";
                DataGridEmpPay.Columns[6].HeaderText = "CL";
                DataGridEmpPay.Columns[6].DataPropertyName = "CL";
                DataGridEmpPay.Columns[6].Width = 60;
                DataGridEmpPay.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[7].Name = "EL";
                DataGridEmpPay.Columns[7].HeaderText = "EL";
                DataGridEmpPay.Columns[7].DataPropertyName = "EL";
                DataGridEmpPay.Columns[7].Width = 60;
                DataGridEmpPay.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[8].Name = "OT";
                DataGridEmpPay.Columns[8].HeaderText = "OT";
                DataGridEmpPay.Columns[8].DataPropertyName = "OT";
                DataGridEmpPay.Columns[8].Width = 60;
                DataGridEmpPay.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridEmpPay_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridEmpPay.RowCount > 1)
            {
                if (DataGridEmpPay.CurrentRow.Cells[1].Value != null)
                {
                    if (DataGridEmpPay.CurrentRow.Cells[1].Value.ToString() != "0")
                    {
                        if (DataGridEmpPay.CurrentCell.ColumnIndex == 4 || DataGridEmpPay.CurrentCell.ColumnIndex == 5 || DataGridEmpPay.CurrentCell.ColumnIndex == 6 || DataGridEmpPay.CurrentCell.ColumnIndex == 7 || DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                        {
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@empid",DataGridEmpPay.CurrentRow.Cells[1].Value.ToString()),
                                new SqlParameter("@monid",DataGridEmpPay.CurrentRow.Cells[0].Value.ToString()),
                                new SqlParameter("@pre",DataGridEmpPay.CurrentRow.Cells[4].Value.ToString()),
                                new SqlParameter("@lop",DataGridEmpPay.CurrentRow.Cells[5].Value.ToString()),
                                new SqlParameter("@cl",DataGridEmpPay.CurrentRow.Cells[6].Value.ToString()),
                                new SqlParameter("@el",DataGridEmpPay.CurrentRow.Cells[7].Value.ToString()),
                                new SqlParameter("@ot",DataGridEmpPay.CurrentRow.Cells[8].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_SaveMMuster", sqlParameters, conn);
                        }
                    }
                }
            }
        }

        private void DataGridEmpPay_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            DataGridEmpPay_CellValueChanged(sender, e);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (cmgfntbox.ValueMember != "" && cmgfntbox.DisplayMember != "" && cmbmonth.ValueMember != "" && cmbmonth.DisplayMember != "")
            {
                LoadPayDetails();
            }
        }

        private void Cmbmonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbmonth.SelectedIndex != -1)
                {
                    string Query = "select Nod from paymonth Where MonId=" + cmbmonth.SelectedValue + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    if (dt.Rows.Count > 0)
                    {
                        TxtWDays.Text = dt.Rows[0]["Nod"].ToString();
                    }
                    else
                    {
                        TxtWDays.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridEmpPay_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter || e.KeyCode == Keys.F2)
                {
                    if (DataGridEmpPay.CurrentRow.Cells[0].Value.ToString() != "")
                    {
                        if (DataGridEmpPay.CurrentCell.ColumnIndex == 4)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[4];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 5)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[5];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 6)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[6];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 7)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[7];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[8];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridEmpPay_CellClick_2(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DataGridEmpPay.CurrentRow.Cells[0].Value.ToString() != "")
                {
                    if (DataGridEmpPay.CurrentCell.ColumnIndex == 4)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[4];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 5)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[5];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 6)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[6];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 7)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[7];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[8];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    }
}
