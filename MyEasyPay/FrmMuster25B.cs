﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.WinForms.DataGridConverter.Events;
using Syncfusion.XlsIO;

namespace MyEasyPay
{
    public partial class FrmMuster25B : Form
    {
        public FrmMuster25B()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");

        private void FrmMuster25B_Load(object sender, EventArgs e)
        {
            Department();
        }

        public void Department()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDept_Mast", conn);
                CmbDepartment.DataSource = null;
                CmbDepartment.DisplayMember = "DeptName";
                CmbDepartment.ValueMember = "DeptId";
                CmbDepartment.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnView_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridMuster.DataSource = null;
                int Tag;
                if(GeneralParameters.MusterTag == 1)
                {
                    Tag = 1;
                }
                else
                {
                    Tag = 2;
                }
                
                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    //new SqlParameter("@MON",fromDate.Month.ToString("00")),
                    //new SqlParameter("@YR",fromDate.Year),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    //new SqlParameter("@TAG",Tag)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME_NEW", sqlParameters, conn);
                //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME", sqlParameters, conn);

                string Query = string.Empty;
                if (fromDate.Day < 16)
                {
                    Query = @"select Empcode,EmpName,Dept,Grade,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,CL Pre,LOP 
                            from musterroll25B ORDER BY empcode,SNO";
                }
                else
                {
                    Query = @"select Empcode,EmpName,Dept,Grade,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,EL Pre,LOP 
                            from musterroll25B ORDER BY empcode,SNO";
                }
                DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                //DataTable dtMuster = new DataTable();
                //dtMuster.Columns.Add("EmployeeCode", typeof(string));
                //dtMuster.Columns.Add("EmployeeName", typeof(string));
                //dtMuster.Columns.Add("Department", typeof(string));
                //dtMuster.Columns.Add("Grade", typeof(string));
                //int id = 0;
                //if (fromDate.Day < 16)
                //{
                //    id = 1;
                //    dtMuster.Columns.Add("D1", typeof(string));
                //    dtMuster.Columns.Add("D2", typeof(string));
                //    dtMuster.Columns.Add("D3", typeof(string));
                //    dtMuster.Columns.Add("D4", typeof(string));
                //    dtMuster.Columns.Add("D5", typeof(string));
                //    dtMuster.Columns.Add("D6", typeof(string));
                //    dtMuster.Columns.Add("D7", typeof(string));
                //    dtMuster.Columns.Add("D8", typeof(string));
                //    dtMuster.Columns.Add("D9", typeof(string));
                //    dtMuster.Columns.Add("D10", typeof(string));
                //    dtMuster.Columns.Add("D11", typeof(string));
                //    dtMuster.Columns.Add("D12", typeof(string));
                //    dtMuster.Columns.Add("D13", typeof(string));
                //    dtMuster.Columns.Add("D14", typeof(string));
                //    dtMuster.Columns.Add("D15", typeof(string));
                //}
                //else
                //{
                //    id = 2;
                //    dtMuster.Columns.Add("D16", typeof(string));
                //    dtMuster.Columns.Add("D17", typeof(string));
                //    dtMuster.Columns.Add("D18", typeof(string));
                //    dtMuster.Columns.Add("D19", typeof(string));
                //    dtMuster.Columns.Add("D20", typeof(string));
                //    dtMuster.Columns.Add("D21", typeof(string));
                //    dtMuster.Columns.Add("D22", typeof(string));
                //    dtMuster.Columns.Add("D23", typeof(string));
                //    dtMuster.Columns.Add("D24", typeof(string));
                //    dtMuster.Columns.Add("D25", typeof(string));
                //    dtMuster.Columns.Add("D26", typeof(string));
                //    dtMuster.Columns.Add("D27", typeof(string));
                //    dtMuster.Columns.Add("D28", typeof(string));
                //    dtMuster.Columns.Add("D29", typeof(string));
                //    dtMuster.Columns.Add("D30", typeof(string));
                //    dtMuster.Columns.Add("D31", typeof(string));
                //}

                //dtMuster.Columns.Add("Pre", typeof(string));
                //dtMuster.Columns.Add("LOP", typeof(string));
                //dtMuster.Columns.Add("CL", typeof(string));
                //dtMuster.Columns.Add("EL", typeof(string));
                //decimal D1 = 0, D2 = 0, D3 = 0, D4 = 0, D5 = 0, D6 = 0, D7 = 0, D8 = 0, D9 = 0, D10 = 0, D11 = 0, D12 = 0, D13 = 0, D14 = 0, D15 = 0;
                //decimal D16 = 0, D17 = 0, D18 = 0, D19 = 0, D20 = 0, D21 = 0, D22 = 0, D23 = 0, D24 = 0, D25 = 0, D26 = 0, D27 = 0, D28 = 0, D29 = 0, D30 = 0, D31 = 0;
                //int absent = 0;
                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    string EmpCode = dataTable.Rows[i]["EmpCode"].ToString();
                //    string SNO = dataTable.Rows[i]["SNO"].ToString();
                //    if (SNO == "1")
                //    {
                //        if (id == 1)
                //        {
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D1"].ToString()))
                //            {
                //                D1 = 0;
                //                absent = 1;
                //            }
                //            else
                //            {
                //                D1 = 1;
                //            }

                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D2"].ToString()))
                //            {
                //                D2 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D2 = 1;
                //            }

                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D3"].ToString()))
                //            {
                //                D3 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D3 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D4"].ToString()))
                //            {
                //                D4 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D4 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D5"].ToString()))
                //            {
                //                D5 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D5 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D6"].ToString()))
                //            {
                //                D6 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D6 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D7"].ToString()))
                //            {
                //                D7 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D7 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D8"].ToString()))
                //            {
                //                D8 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D8 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D9"].ToString()))
                //            {
                //                D9 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D9 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D10"].ToString()))
                //            {
                //                D10 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D10 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D11"].ToString()))
                //            {
                //                D11 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D11 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D12"].ToString()))
                //            {
                //                D12 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D12 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D13"].ToString()))
                //            {
                //                D13 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D13 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D14"].ToString()))
                //            {
                //                D14 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D14 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D15"].ToString()))
                //            {
                //                D15 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D15 = 1;
                //            }
                //            dataTable.Rows[i]["Pre"] = D1 + D2 + D3 + D4 + D5 + D6 + D7 + D8 + D9 + D10 + D11 + D12 + D13 + D14 + D15;
                //            dataTable.Rows[i]["LOP"] = "0";
                //            dataTable.Rows[i]["LOP"] = absent;
                //        }
                //        else if (id == 2)
                //        {
                //            absent = 0;
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D16"].ToString()))
                //            {
                //                D16 = 0;
                //                absent = 1;
                //            }
                //            else
                //            {
                //                D16 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D17"].ToString()))
                //            {
                //                D17 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D17 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D18"].ToString()))
                //            {
                //                D18 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D18 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D19"].ToString()))
                //            {
                //                D19 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D19 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D20"].ToString()))
                //            {
                //                D20 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D20 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D21"].ToString()))
                //            {
                //                D21 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D21 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D22"].ToString()))
                //            {
                //                D22 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D22 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D23"].ToString()))
                //            {
                //                D23 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D23 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D24"].ToString()))
                //            {
                //                D24 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D24 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D25"].ToString()))
                //            {
                //                D25 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D25 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D26"].ToString()))
                //            {
                //                D26 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D26 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D27"].ToString()))
                //            {
                //                D27 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D27 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D28"].ToString()))
                //            {
                //                D28 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D28 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D29"].ToString()))
                //            {
                //                D29 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D29 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D30"].ToString()))
                //            {
                //                D30 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D30 = 1;
                //            }
                //            if (string.IsNullOrEmpty(dataTable.Rows[i]["D31"].ToString()))
                //            {
                //                D31 = 0;
                //                absent += 1;
                //            }
                //            else
                //            {
                //                D31 = 1;
                //            }
                //            dataTable.Rows[i]["Pre"] = D16 + D17 + D18 + D19 + D20 + D21 + D22 + D23 + D24 + D25 + D26 + D27 + D28 + D29 + D30 + D31;
                //            dataTable.Rows[i]["LOP"] = "0";
                //            dataTable.Rows[i]["LOP"] = absent;
                //        }
                //    }
                //}

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    System.Data.DataRow dataRow = dtMuster.NewRow();
                //    dataRow["EmployeeCode"] = dataTable.Rows[i]["EmpCode"].ToString();
                //    dataRow["EmployeeName"] = dataTable.Rows[i]["EmpName"].ToString();
                //    dataRow["Department"] = dataTable.Rows[i]["Dept"].ToString();
                //    dataRow["Grade"] = dataTable.Rows[i]["Grade"].ToString();
                //    if (id == 1)
                //    {
                //        dataRow["D1"] = dataTable.Rows[i]["D1"].ToString();
                //        dataRow["D2"] = dataTable.Rows[i]["D2"].ToString();
                //        dataRow["D3"] = dataTable.Rows[i]["D3"].ToString();
                //        dataRow["D4"] = dataTable.Rows[i]["D4"].ToString();
                //        dataRow["D5"] = dataTable.Rows[i]["D5"].ToString();
                //        dataRow["D6"] = dataTable.Rows[i]["D6"].ToString();
                //        dataRow["D7"] = dataTable.Rows[i]["D7"].ToString();
                //        dataRow["D8"] = dataTable.Rows[i]["D8"].ToString();
                //        dataRow["D9"] = dataTable.Rows[i]["D9"].ToString();
                //        dataRow["D10"] = dataTable.Rows[i]["D10"].ToString();
                //        dataRow["D11"] = dataTable.Rows[i]["D11"].ToString();
                //        dataRow["D12"] = dataTable.Rows[i]["D12"].ToString();
                //        dataRow["D13"] = dataTable.Rows[i]["D13"].ToString();
                //        dataRow["D14"] = dataTable.Rows[i]["D14"].ToString();
                //        dataRow["D15"] = dataTable.Rows[i]["D15"].ToString();
                //    }
                //    else
                //    {
                //        dataRow["D16"] = dataTable.Rows[i]["D16"].ToString();
                //        dataRow["D17"] = dataTable.Rows[i]["D17"].ToString();
                //        dataRow["D18"] = dataTable.Rows[i]["D18"].ToString();
                //        dataRow["D19"] = dataTable.Rows[i]["D19"].ToString();
                //        dataRow["D20"] = dataTable.Rows[i]["D20"].ToString();
                //        dataRow["D21"] = dataTable.Rows[i]["D21"].ToString();
                //        dataRow["D22"] = dataTable.Rows[i]["D22"].ToString();
                //        dataRow["D23"] = dataTable.Rows[i]["D23"].ToString();
                //        dataRow["D24"] = dataTable.Rows[i]["D24"].ToString();
                //        dataRow["D25"] = dataTable.Rows[i]["D25"].ToString();
                //        dataRow["D26"] = dataTable.Rows[i]["D26"].ToString();
                //        dataRow["D27"] = dataTable.Rows[i]["D27"].ToString();
                //        dataRow["D28"] = dataTable.Rows[i]["D28"].ToString();
                //        dataRow["D29"] = dataTable.Rows[i]["D29"].ToString();
                //        dataRow["D30"] = dataTable.Rows[i]["D30"].ToString();
                //        dataRow["D31"] = dataTable.Rows[i]["D31"].ToString();
                //    }
                //    dataRow["Pre"] = dataTable.Rows[i]["Pre"].ToString();
                //    dataRow["LOP"] = dataTable.Rows[i]["LOP"].ToString();
                //    dataRow["CL"] = dataTable.Rows[i]["CL"].ToString();
                //    dataRow["EL"] = dataTable.Rows[i]["EL"].ToString();
                //    dtMuster.Rows.Add(dataRow);
                //}
                DatagridMuster.DataSource = data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if(ChckMonthly.Checked == true)
                {
                    ExportFullMonth();
                }
                else
                {
                    ExportData();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ExportFullMonth()
        {
            try
            {
                DatagridMuster.DataSource = null;
                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text);;
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@MON",fromDate.Month.ToString("00")),
                    new SqlParameter("@YR",fromDate.Year),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    new SqlParameter("@TAG",1)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME", sqlParameters, conn);
                DatagridMuster.DataSource = dataTable;

                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Open(Application.StartupPath + "/MusterRoll.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                xlApp.DisplayAlerts = false;
                xlWb.SaveAs(Application.StartupPath + "/MusterRoll1.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                Microsoft.Office.Interop.Excel.Workbook xlWbN = xlApp.Workbooks.Open(Application.StartupPath + "/MusterRoll1.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                Microsoft.Office.Interop.Excel.Worksheet xlSht = xlWbN.Sheets[1];
                xlApp.Visible = true;

                Microsoft.Office.Interop.Excel.Range row1 = xlSht.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = xlSht.Rows.Cells[2, 1];
                Microsoft.Office.Interop.Excel.Range row3 = xlSht.Rows.Cells[3, 1];
                row1.Value = GeneralParameters.CompanyName;
                row2.Value = "Muster Roll 25B Report";
                row3.Value = "Month of " + Convert.ToDateTime(DtpFromDate.Text).ToString("MMM-yyyy");
                int j = 5;

                for (int i = 0; i < DatagridMuster.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range EmployeeCode = xlSht.Rows.Cells[j, 1];
                    Microsoft.Office.Interop.Excel.Range EmployeeName = xlSht.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range Department = xlSht.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range Grade = xlSht.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range D1 = xlSht.Rows.Cells[j, 5];
                    Microsoft.Office.Interop.Excel.Range D2 = xlSht.Rows.Cells[j, 6];
                    Microsoft.Office.Interop.Excel.Range D3 = xlSht.Rows.Cells[j, 7];
                    Microsoft.Office.Interop.Excel.Range D4 = xlSht.Rows.Cells[j, 8];
                    Microsoft.Office.Interop.Excel.Range D5 = xlSht.Rows.Cells[j, 9];
                    Microsoft.Office.Interop.Excel.Range D6 = xlSht.Rows.Cells[j, 10];
                    Microsoft.Office.Interop.Excel.Range D7 = xlSht.Rows.Cells[j, 11];
                    Microsoft.Office.Interop.Excel.Range D8 = xlSht.Rows.Cells[j, 12];
                    Microsoft.Office.Interop.Excel.Range D9 = xlSht.Rows.Cells[j, 13];
                    Microsoft.Office.Interop.Excel.Range D10 = xlSht.Rows.Cells[j, 14];
                    Microsoft.Office.Interop.Excel.Range D11 = xlSht.Rows.Cells[j, 15];
                    Microsoft.Office.Interop.Excel.Range D12 = xlSht.Rows.Cells[j, 16];
                    Microsoft.Office.Interop.Excel.Range D13 = xlSht.Rows.Cells[j, 17];
                    Microsoft.Office.Interop.Excel.Range D14 = xlSht.Rows.Cells[j, 18];
                    Microsoft.Office.Interop.Excel.Range D15 = xlSht.Rows.Cells[j, 19];
                    Microsoft.Office.Interop.Excel.Range D16 = xlSht.Rows.Cells[j, 20];
                    Microsoft.Office.Interop.Excel.Range D17 = xlSht.Rows.Cells[j, 21];
                    Microsoft.Office.Interop.Excel.Range D18 = xlSht.Rows.Cells[j, 22];
                    Microsoft.Office.Interop.Excel.Range D19 = xlSht.Rows.Cells[j, 23];
                    Microsoft.Office.Interop.Excel.Range D20 = xlSht.Rows.Cells[j, 24];
                    Microsoft.Office.Interop.Excel.Range D21 = xlSht.Rows.Cells[j, 25];
                    Microsoft.Office.Interop.Excel.Range D22 = xlSht.Rows.Cells[j, 26];
                    Microsoft.Office.Interop.Excel.Range D23 = xlSht.Rows.Cells[j, 27];
                    Microsoft.Office.Interop.Excel.Range D24 = xlSht.Rows.Cells[j, 28];
                    Microsoft.Office.Interop.Excel.Range D25 = xlSht.Rows.Cells[j, 29];
                    Microsoft.Office.Interop.Excel.Range D26 = xlSht.Rows.Cells[j, 30];
                    Microsoft.Office.Interop.Excel.Range D27 = xlSht.Rows.Cells[j, 31];
                    Microsoft.Office.Interop.Excel.Range D28 = xlSht.Rows.Cells[j, 32];
                    Microsoft.Office.Interop.Excel.Range D29 = xlSht.Rows.Cells[j, 33];
                    Microsoft.Office.Interop.Excel.Range D30 = xlSht.Rows.Cells[j, 34];
                    Microsoft.Office.Interop.Excel.Range D31 = xlSht.Rows.Cells[j, 35];
                    Microsoft.Office.Interop.Excel.Range Pre = xlSht.Rows.Cells[j, 36];
                    Microsoft.Office.Interop.Excel.Range Lop = xlSht.Rows.Cells[j, 37];
                    Microsoft.Office.Interop.Excel.Range CL = xlSht.Rows.Cells[j, 38];
                    Microsoft.Office.Interop.Excel.Range EL = xlSht.Rows.Cells[j, 39];
                    EmployeeCode.Value = DatagridMuster.Rows[i].Cells[1].Value.ToString();
                    EmployeeName.Value = DatagridMuster.Rows[i].Cells[3].Value.ToString();
                    Department.Value = DatagridMuster.Rows[i].Cells[4].Value.ToString();
                    Grade.Value = DatagridMuster.Rows[i].Cells[5].Value.ToString();
                    D1.Value = DatagridMuster.Rows[i].Cells[6].Value.ToString();
                    D2.Value = DatagridMuster.Rows[i].Cells[7].Value.ToString();
                    D3.Value = DatagridMuster.Rows[i].Cells[8].Value.ToString();
                    D4.Value = DatagridMuster.Rows[i].Cells[9].Value.ToString();
                    D5.Value = DatagridMuster.Rows[i].Cells[10].Value.ToString();
                    D6.Value = DatagridMuster.Rows[i].Cells[11].Value.ToString();
                    D7.Value = DatagridMuster.Rows[i].Cells[12].Value.ToString();
                    D8.Value = DatagridMuster.Rows[i].Cells[13].Value.ToString();
                    D9.Value = DatagridMuster.Rows[i].Cells[14].Value.ToString();
                    D10.Value = DatagridMuster.Rows[i].Cells[15].Value.ToString();
                    D11.Value = DatagridMuster.Rows[i].Cells[16].Value.ToString();
                    D12.Value = DatagridMuster.Rows[i].Cells[17].Value.ToString();
                    D13.Value = DatagridMuster.Rows[i].Cells[18].Value.ToString();
                    D14.Value = DatagridMuster.Rows[i].Cells[19].Value.ToString();
                    D15.Value = DatagridMuster.Rows[i].Cells[20].Value.ToString();
                    D16.Value = DatagridMuster.Rows[i].Cells[21].Value.ToString();
                    D17.Value = DatagridMuster.Rows[i].Cells[22].Value.ToString();
                    D18.Value = DatagridMuster.Rows[i].Cells[23].Value.ToString();
                    D19.Value = DatagridMuster.Rows[i].Cells[24].Value.ToString();
                    D20.Value = DatagridMuster.Rows[i].Cells[25].Value.ToString();
                    D21.Value = DatagridMuster.Rows[i].Cells[26].Value.ToString();
                    D22.Value = DatagridMuster.Rows[i].Cells[27].Value.ToString();
                    D23.Value = DatagridMuster.Rows[i].Cells[28].Value.ToString();
                    D24.Value = DatagridMuster.Rows[i].Cells[29].Value.ToString();
                    D25.Value = DatagridMuster.Rows[i].Cells[30].Value.ToString();
                    D26.Value = DatagridMuster.Rows[i].Cells[31].Value.ToString();
                    D27.Value = DatagridMuster.Rows[i].Cells[32].Value.ToString();
                    D28.Value = DatagridMuster.Rows[i].Cells[33].Value.ToString();
                    D29.Value = DatagridMuster.Rows[i].Cells[34].Value.ToString();
                    D30.Value = DatagridMuster.Rows[i].Cells[35].Value.ToString();
                    D31.Value = DatagridMuster.Rows[i].Cells[36].Value.ToString();

                    Pre.Value = DatagridMuster.Rows[i].Cells[37].Value.ToString();
                    Lop.Value = DatagridMuster.Rows[i].Cells[38].Value.ToString();
                    CL.Value = DatagridMuster.Rows[i].Cells[39].Value.ToString();
                    EL.Value = DatagridMuster.Rows[i].Cells[40].Value.ToString();
                    j += 1;
                }
                MessageBox.Show("Exported Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ExportData()
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "Muster Export";
            for (int i = 0; i < DatagridMuster.Columns.Count; i++)
            {
                if(i == 0 || i == 7 || i == 9 || i == 11 || i==13 || i == 15)
                {

                }
                else
                {
                    worksheet.Cells[1, i + 1] = DatagridMuster.Columns[i].HeaderText;
                }
                
            }
            for (int i = 0; i < DatagridMuster.Rows.Count; i++)
            {
                for (int j = 0; j < DatagridMuster.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = DatagridMuster.Rows[i].Cells[j].Value.ToString();
                }
            }
        }

        private void BtnWithTime_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridMuster.DataSource = null;

                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    //new SqlParameter("@MON",fromDate.Month.ToString("00")),
                    //new SqlParameter("@YR",fromDate.Year),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    //new SqlParameter("@TAG",Tag)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME_NEW", sqlParameters, conn);
                //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME", sqlParameters, conn);
                DatagridMuster.DataSource = dataTable;
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Muster Export";
                for (int i = 0; i < DatagridMuster.Columns.Count; i++)
                {
                    worksheet.Cells[1, i + 1] = DatagridMuster.Columns[i].HeaderText;
                }
                for (int i = 0; i < DatagridMuster.Rows.Count-1; i++)
                {
                    for (int j = 0; j < DatagridMuster.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = DatagridMuster.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ChckMonthly_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
