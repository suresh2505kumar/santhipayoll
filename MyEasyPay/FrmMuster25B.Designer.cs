﻿namespace MyEasyPay
{
    partial class FrmMuster25B
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grFront = new System.Windows.Forms.GroupBox();
            this.CmbDepartment = new System.Windows.Forms.ComboBox();
            this.ChckMonthly = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnView = new System.Windows.Forms.Button();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DatagridMuster = new System.Windows.Forms.DataGridView();
            this.BtnExportExcel = new System.Windows.Forms.Button();
            this.BtnWithTime = new System.Windows.Forms.Button();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridMuster)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.CmbDepartment);
            this.grFront.Controls.Add(this.ChckMonthly);
            this.grFront.Controls.Add(this.label3);
            this.grFront.Controls.Add(this.BtnView);
            this.grFront.Controls.Add(this.DtpToDate);
            this.grFront.Controls.Add(this.DtpFromDate);
            this.grFront.Controls.Add(this.label2);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.DatagridMuster);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(11, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(942, 499);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // CmbDepartment
            // 
            this.CmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDepartment.FormattingEnabled = true;
            this.CmbDepartment.Location = new System.Drawing.Point(85, 19);
            this.CmbDepartment.Name = "CmbDepartment";
            this.CmbDepartment.Size = new System.Drawing.Size(267, 26);
            this.CmbDepartment.TabIndex = 7;
            // 
            // ChckMonthly
            // 
            this.ChckMonthly.AutoSize = true;
            this.ChckMonthly.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckMonthly.Location = new System.Drawing.Point(838, 22);
            this.ChckMonthly.Name = "ChckMonthly";
            this.ChckMonthly.Size = new System.Drawing.Size(94, 22);
            this.ChckMonthly.TabIndex = 9;
            this.ChckMonthly.Text = "Full Month";
            this.ChckMonthly.UseVisualStyleBackColor = true;
            this.ChckMonthly.CheckedChanged += new System.EventHandler(this.ChckMonthly_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Department";
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(756, 17);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(76, 30);
            this.BtnView.TabIndex = 5;
            this.BtnView.Text = "View";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // DtpToDate
            // 
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToDate.Location = new System.Drawing.Point(631, 19);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(119, 26);
            this.DtpToDate.TabIndex = 3;
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpFromDate.Location = new System.Drawing.Point(434, 19);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(129, 26);
            this.DtpFromDate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(571, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "To Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(356, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "From Date";
            // 
            // DatagridMuster
            // 
            this.DatagridMuster.AllowUserToAddRows = false;
            this.DatagridMuster.BackgroundColor = System.Drawing.Color.White;
            this.DatagridMuster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatagridMuster.Location = new System.Drawing.Point(6, 53);
            this.DatagridMuster.Name = "DatagridMuster";
            this.DatagridMuster.RowHeadersVisible = false;
            this.DatagridMuster.Size = new System.Drawing.Size(922, 440);
            this.DatagridMuster.TabIndex = 8;
            // 
            // BtnExportExcel
            // 
            this.BtnExportExcel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExportExcel.Location = new System.Drawing.Point(820, 504);
            this.BtnExportExcel.Name = "BtnExportExcel";
            this.BtnExportExcel.Size = new System.Drawing.Size(130, 30);
            this.BtnExportExcel.TabIndex = 8;
            this.BtnExportExcel.Text = "Export Excel";
            this.BtnExportExcel.UseVisualStyleBackColor = true;
            this.BtnExportExcel.Click += new System.EventHandler(this.BtnExportExcel_Click);
            // 
            // BtnWithTime
            // 
            this.BtnWithTime.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWithTime.Location = new System.Drawing.Point(11, 504);
            this.BtnWithTime.Name = "BtnWithTime";
            this.BtnWithTime.Size = new System.Drawing.Size(196, 30);
            this.BtnWithTime.TabIndex = 10;
            this.BtnWithTime.Text = "Generate Muster With Time";
            this.BtnWithTime.UseVisualStyleBackColor = true;
            this.BtnWithTime.Click += new System.EventHandler(this.BtnWithTime_Click);
            // 
            // FrmMuster25B
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(957, 538);
            this.Controls.Add(this.BtnWithTime);
            this.Controls.Add(this.BtnExportExcel);
            this.Controls.Add(this.grFront);
            this.Name = "FrmMuster25B";
            this.Text = "Muster 25B";
            this.Load += new System.EventHandler(this.FrmMuster25B_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridMuster)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DateTimePicker DtpToDate;
        private System.Windows.Forms.DateTimePicker DtpFromDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbDepartment;
        private System.Windows.Forms.Button BtnExportExcel;
        private System.Windows.Forms.DataGridView DatagridMuster;
        private System.Windows.Forms.CheckBox ChckMonthly;
        private System.Windows.Forms.Button BtnWithTime;
    }
}