﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace MyEasyPay
{
    public partial class FrmEmpMast : Form
    {
        public FrmEmpMast()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int EmpId = 0;
        int Loadid = 0;
        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void FrmEmpMast_Load(object sender, EventArgs e)
        {
            LoadMasters();
            //LoadButton(3);
            //DataTable dt = GetData("SP_GetEmp_Mast");
            //FillEMp(dt);
            frpfnttt.Visible = false;
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            var theDate = DateTime.Now.AddMonths(-1);
            theDate.ToString("MM/yyyy");

            Frmdt.Format = DateTimePickerFormat.Custom;

            Frmdt.CustomFormat = theDate.ToString("MM/yyyy");
            DateTime str9 = Convert.ToDateTime(Frmdt.Text);

            loadgrid();
            //Loadsum();
        }
        private void loadapp()
        {
            conn.Close();
            conn.Open();
            string qur = "select empid,empname from emp_mast where isappriser=1  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cmbapp.DataSource = null;
            cmbapp.DataSource = tab;
            cmbapp.DisplayMember = "empname";
            cmbapp.ValueMember = "empid";
            cmbapp.SelectedIndex = -1;
            conn.Close();



        }
        private void Loadsum()
        {


            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            conn.Close();
            conn.Open();
            string qur = "exec EMPSTRENGTH_SP " + str9.Month + "," + str9.Year + ",'" + str9 + "'";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);





            fntgrid.AutoGenerateColumns = false;
            fntgrid.Refresh();
            fntgrid.DataSource = null;
            fntgrid.Rows.Clear();


            fntgrid.ColumnCount = tab.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tab.Columns)
            {
                fntgrid.Columns[Genclass.i].Name = column.ColumnName;
                fntgrid.Columns[Genclass.i].HeaderText = column.ColumnName;
                fntgrid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }


            fntgrid.ColumnCount = 6;





            fntgrid.Columns[0].Name = "DeptId";
            fntgrid.Columns[0].HeaderText = "DeptId";
            fntgrid.Columns[0].DataPropertyName = "DeptId";
            fntgrid.Columns[0].Visible = false;

            fntgrid.Columns[1].Name = "DeptName";
            fntgrid.Columns[1].HeaderText = "Category";
            fntgrid.Columns[1].DataPropertyName = "DeptName";
            fntgrid.Columns[1].Width = 190;

            fntgrid.Columns[2].Name = "STRENGTH";
            fntgrid.Columns[2].HeaderText = "OP Strength";
            fntgrid.Columns[2].DataPropertyName = "STRENGTH";
            fntgrid.Columns[2].Width = 120;
            fntgrid.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            fntgrid.Columns[3].Name = "ADDITION";
            fntgrid.Columns[3].HeaderText = "Joined";
            fntgrid.Columns[3].DataPropertyName = "ADDITION";
            fntgrid.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            fntgrid.Columns[3].Width = 80;
            fntgrid.Columns[4].Name = "DELETION";
            fntgrid.Columns[4].HeaderText = "Releived";
            fntgrid.Columns[4].DataPropertyName = "DELETION";
            fntgrid.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            fntgrid.Columns[4].Width = 120;

            fntgrid.Columns[5].Name = "TSTRENGTH";
            fntgrid.Columns[5].HeaderText = "Total";
            fntgrid.Columns[5].DataPropertyName = "TSTRENGTH";
            fntgrid.Columns[5].Width = 120;
            fntgrid.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;






            fntgrid.DataSource = tab;


            total();
            color();

        }
        private void total()
        {


            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum3 = 0;
            Genclass.sum4 = 0;
            for (int k = 0; k < fntgrid.Rows.Count; k++)
            {

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(fntgrid.Rows[k].Cells[2].Value);
                Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(fntgrid.Rows[k].Cells[3].Value);
                Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(fntgrid.Rows[k].Cells[4].Value);
                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(fntgrid.Rows[k].Cells[5].Value);

            }
            txtop.Text = Genclass.sum1.ToString();
            txtjoin.Text = Genclass.sum2.ToString();
            txtleft.Text = Genclass.sum3.ToString();
            txtclose.Text = Genclass.sum4.ToString();
            txtop.BackColor = Color.LightYellow;
            txtjoin.BackColor = Color.LightYellow;
            txtleft.BackColor = Color.LightYellow;
            txtclose.BackColor = Color.LightYellow;
        }

        private void color()
        {

            for (int l = 0; l < fntgrid.Rows.Count; l++)
            {
                if (fntgrid.Rows[l].Cells[0].Value == null)
                {
                    return;
                }
                else
                {


                    fntgrid.Columns[2].DefaultCellStyle.BackColor = Color.LightBlue;


                    fntgrid.Columns[3].DefaultCellStyle.BackColor = Color.LightGray;
                    fntgrid.Columns[4].DefaultCellStyle.BackColor = Color.LightGreen;
                    fntgrid.Columns[5].DefaultCellStyle.BackColor = Color.LightYellow;


                }



            }


        }
        private void loadgrid()

        {



            conn.Close();
            conn.Open();

            string quy = "exec SP_GetEmp_mastAll";
            Genclass.cmd = new SqlCommand(quy, conn);


            //string quy = "exec SP_GetEmp_mast "+ cmgfntbox.SelectedValue + "";
            //Genclass.cmd = new SqlCommand(quy, conn);



            SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);





            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.Refresh();
            DataGridEmployee.DataSource = null;
            DataGridEmployee.Rows.Clear();


            DataGridEmployee.ColumnCount = tab.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tab.Columns)
            {
                DataGridEmployee.Columns[Genclass.i].Name = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].HeaderText = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }


            DataGridEmployee.DataSource = null;
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.ColumnCount = 37;
            DataGridEmployee.Columns[0].Name = "EmpId";
            DataGridEmployee.Columns[0].HeaderText = "EmpId";
            DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
            DataGridEmployee.Columns[0].Visible = false;

            DataGridEmployee.Columns[1].Name = "EmpCode";
            DataGridEmployee.Columns[1].HeaderText = "EmpCode";
            DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
            DataGridEmployee.Columns[1].Width = 100;
            DataGridEmployee.Columns[2].Name = "EmpName";
            DataGridEmployee.Columns[2].HeaderText = "Employe Name";
            DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
            DataGridEmployee.Columns[2].Width = 200;

            //DataGridEmployee.Columns[2].Name = "EmpCode";
            //DataGridEmployee.Columns[2].HeaderText = "EmpCode";
            //DataGridEmployee.Columns[2].DataPropertyName = "EmpCode";
            //DataGridEmployee.Columns[2].Width = 70;

            DataGridEmployee.Columns[3].Name = "ShiftName";
            DataGridEmployee.Columns[3].HeaderText = "ShiftName";
            DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
            DataGridEmployee.Columns[3].Visible = false;
            DataGridEmployee.Columns[4].Name = "DeptId";
            DataGridEmployee.Columns[4].HeaderText = "DeptId";
            DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
            DataGridEmployee.Columns[4].Visible = false;

            DataGridEmployee.Columns[5].Name = "DesId";
            DataGridEmployee.Columns[5].HeaderText = "DesId";
            DataGridEmployee.Columns[5].DataPropertyName = "DesId";
            DataGridEmployee.Columns[5].Visible = false;

            DataGridEmployee.Columns[6].Name = "Grid";
            DataGridEmployee.Columns[6].HeaderText = "Grid";
            DataGridEmployee.Columns[6].DataPropertyName = "Grid";
            DataGridEmployee.Columns[6].Visible = false;

            DataGridEmployee.Columns[7].Name = "GrName";
            DataGridEmployee.Columns[7].HeaderText = "GrName";
            DataGridEmployee.Columns[7].DataPropertyName = "GrName";
            DataGridEmployee.Columns[7].Visible = false;

            DataGridEmployee.Columns[8].Name = "ShiftId";
            DataGridEmployee.Columns[8].HeaderText = "ShiftId";
            DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
            DataGridEmployee.Columns[8].Visible = false;

            DataGridEmployee.Columns[9].Name = "EmpTypeId";
            DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
            DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
            DataGridEmployee.Columns[9].Visible = false;

            DataGridEmployee.Columns[10].Name = "FName";
            DataGridEmployee.Columns[10].HeaderText = "FatherName";
            DataGridEmployee.Columns[10].DataPropertyName = "FName";
            DataGridEmployee.Columns[10].Width = 160;

            DataGridEmployee.Columns[11].Name = "DOB";
            DataGridEmployee.Columns[11].HeaderText = "DOB";
            DataGridEmployee.Columns[11].DataPropertyName = "DOB";
            DataGridEmployee.Columns[11].Visible = false;

            DataGridEmployee.Columns[12].Name = "DOJ";
            DataGridEmployee.Columns[12].HeaderText = "DOJ";
            DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
            DataGridEmployee.Columns[12].Visible = false;

            DataGridEmployee.Columns[13].Name = "Sex";
            DataGridEmployee.Columns[13].HeaderText = "Sex";
            DataGridEmployee.Columns[13].DataPropertyName = "Sex";
            DataGridEmployee.Columns[13].Visible = false;

            DataGridEmployee.Columns[14].Name = "EmpAddress";
            DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
            DataGridEmployee.Columns[14].DataPropertyName = "Street";
            DataGridEmployee.Columns[14].Visible = false;

            DataGridEmployee.Columns[15].Name = "City";
            DataGridEmployee.Columns[15].HeaderText = "City";
            DataGridEmployee.Columns[15].DataPropertyName = "City";
            DataGridEmployee.Columns[15].Visible = false;

            DataGridEmployee.Columns[16].Name = "Phone";
            DataGridEmployee.Columns[16].HeaderText = "Phone";
            DataGridEmployee.Columns[16].DataPropertyName = "Phone";
            DataGridEmployee.Columns[16].Visible = false;

            DataGridEmployee.Columns[17].Name = "Information";
            DataGridEmployee.Columns[17].HeaderText = "Information";
            DataGridEmployee.Columns[17].DataPropertyName = "Information";
            DataGridEmployee.Columns[17].Visible = false;
            DataGridEmployee.Columns[18].Name = "Dispensary";
            DataGridEmployee.Columns[18].HeaderText = "Dispensary";
            DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
            DataGridEmployee.Columns[18].Visible = false;

            DataGridEmployee.Columns[19].Name = "WeekOff";
            DataGridEmployee.Columns[19].HeaderText = "WeekOff";
            DataGridEmployee.Columns[19].DataPropertyName = "woff";
            DataGridEmployee.Columns[19].Visible = false;

            DataGridEmployee.Columns[20].Name = "CL";
            DataGridEmployee.Columns[20].HeaderText = "CL";
            DataGridEmployee.Columns[20].DataPropertyName = "CL";
            DataGridEmployee.Columns[20].Visible = false;
            DataGridEmployee.Columns[21].Name = "EL";
            DataGridEmployee.Columns[21].HeaderText = "EL";
            DataGridEmployee.Columns[21].DataPropertyName = "EL";
            DataGridEmployee.Columns[21].Visible = false;

            DataGridEmployee.Columns[22].Name = "ESINo";
            DataGridEmployee.Columns[22].HeaderText = "ESINo";
            DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
            DataGridEmployee.Columns[22].Visible = false;

            DataGridEmployee.Columns[23].Name = "PFNo";
            DataGridEmployee.Columns[23].HeaderText = "PFNo";
            DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
            DataGridEmployee.Columns[23].Visible = false;

            DataGridEmployee.Columns[24].Name = "UANNo";
            DataGridEmployee.Columns[24].HeaderText = "UANNo";
            DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
            DataGridEmployee.Columns[24].Visible = false;

            DataGridEmployee.Columns[25].Name = "PANNo";
            DataGridEmployee.Columns[25].HeaderText = "PANNo";
            DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
            DataGridEmployee.Columns[25].Visible = false;

            DataGridEmployee.Columns[26].Name = "AadharNo";
            DataGridEmployee.Columns[26].HeaderText = "AadharNo";
            DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
            DataGridEmployee.Columns[26].Visible = false;

            DataGridEmployee.Columns[27].Name = "empphoto";
            DataGridEmployee.Columns[27].HeaderText = "empphoto";
            DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
            DataGridEmployee.Columns[27].Visible = false;

            DataGridEmployee.Columns[28].Name = "DesName";
            DataGridEmployee.Columns[28].HeaderText = "DesName";
            DataGridEmployee.Columns[28].DataPropertyName = "DesName";
            DataGridEmployee.Columns[28].Visible = false;


            DataGridEmployee.Columns[29].Name = "DeptName";
            DataGridEmployee.Columns[29].HeaderText = "DeptName";
            DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
            DataGridEmployee.Columns[29].Width = 150;


            DataGridEmployee.Columns[30].Name = "Grade";
            DataGridEmployee.Columns[30].HeaderText = "Grade";
            DataGridEmployee.Columns[30].DataPropertyName = "Grade";
            DataGridEmployee.Columns[30].Width = 150;


            DataGridEmployee.Columns[31].Name = "TName";
            DataGridEmployee.Columns[31].HeaderText = "TName";
            DataGridEmployee.Columns[31].DataPropertyName = "TName";
            DataGridEmployee.Columns[31].Visible = false;

            DataGridEmployee.Columns[32].Name = "CardNo";
            DataGridEmployee.Columns[32].HeaderText = "CardNo";
            DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
            DataGridEmployee.Columns[32].Width = 100;


            DataGridEmployee.Columns[33].Name = "emialtag";
            DataGridEmployee.Columns[33].HeaderText = "emialtag";
            DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
            DataGridEmployee.Columns[33].Visible = false;


            DataGridEmployee.Columns[34].Name = "isappriser";
            DataGridEmployee.Columns[34].HeaderText = "isappriser";
            DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
            DataGridEmployee.Columns[34].Visible = false;

            DataGridEmployee.Columns[35].Name = "appid";
            DataGridEmployee.Columns[35].HeaderText = "appid";
            DataGridEmployee.Columns[35].DataPropertyName = "appid";
            DataGridEmployee.Columns[35].Visible = false;


            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;

            DataGridEmployee.DataSource = tab;
        }

        private void Loadgrid2()
        {
            //conn.Close();
            //conn.Open();
            //string quy = "exec SP_GetEmp_mast " + cmgfntbox.SelectedValue + "";
            //Genclass.cmd = new SqlCommand(quy, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);

            SqlParameter[] sqlParameters = { new SqlParameter("@deptid", cmgfntbox.SelectedValue) };
            DataTable tab = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mast", sqlParameters, conn);

            DataGridEmployee.DataSource = null;
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.ColumnCount = 37;
            DataGridEmployee.Columns[0].Name = "EmpId";
            DataGridEmployee.Columns[0].HeaderText = "EmpId";
            DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
            DataGridEmployee.Columns[0].Visible = false;

            DataGridEmployee.Columns[1].Name = "EmpCode";
            DataGridEmployee.Columns[1].HeaderText = "EmpCode";
            DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
            DataGridEmployee.Columns[1].Width = 100;
            DataGridEmployee.Columns[2].Name = "EmpName";
            DataGridEmployee.Columns[2].HeaderText = "Employe Name";
            DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
            DataGridEmployee.Columns[2].Width = 200;

            //DataGridEmployee.Columns[2].Name = "EmpCode";
            //DataGridEmployee.Columns[2].HeaderText = "EmpCode";
            //DataGridEmployee.Columns[2].DataPropertyName = "EmpCode";
            //DataGridEmployee.Columns[2].Width = 70;

            DataGridEmployee.Columns[3].Name = "ShiftName";
            DataGridEmployee.Columns[3].HeaderText = "ShiftName";
            DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
            DataGridEmployee.Columns[3].Visible = false;
            DataGridEmployee.Columns[4].Name = "DeptId";
            DataGridEmployee.Columns[4].HeaderText = "DeptId";
            DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
            DataGridEmployee.Columns[4].Visible = false;

            DataGridEmployee.Columns[5].Name = "DesId";
            DataGridEmployee.Columns[5].HeaderText = "DesId";
            DataGridEmployee.Columns[5].DataPropertyName = "DesId";
            DataGridEmployee.Columns[5].Visible = false;

            DataGridEmployee.Columns[6].Name = "Grid";
            DataGridEmployee.Columns[6].HeaderText = "Grid";
            DataGridEmployee.Columns[6].DataPropertyName = "Grid";
            DataGridEmployee.Columns[6].Visible = false;

            DataGridEmployee.Columns[7].Name = "GrName";
            DataGridEmployee.Columns[7].HeaderText = "GrName";
            DataGridEmployee.Columns[7].DataPropertyName = "GrName";
            DataGridEmployee.Columns[7].Visible = false;

            DataGridEmployee.Columns[8].Name = "ShiftId";
            DataGridEmployee.Columns[8].HeaderText = "ShiftId";
            DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
            DataGridEmployee.Columns[8].Visible = false;

            DataGridEmployee.Columns[9].Name = "EmpTypeId";
            DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
            DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
            DataGridEmployee.Columns[9].Visible = false;

            DataGridEmployee.Columns[10].Name = "FName";
            DataGridEmployee.Columns[10].HeaderText = "FatherName";
            DataGridEmployee.Columns[10].DataPropertyName = "FName";
            DataGridEmployee.Columns[10].Width = 160;

            DataGridEmployee.Columns[11].Name = "DOB";
            DataGridEmployee.Columns[11].HeaderText = "DOB";
            DataGridEmployee.Columns[11].DataPropertyName = "DOB";
            DataGridEmployee.Columns[11].Visible = false;

            DataGridEmployee.Columns[12].Name = "DOJ";
            DataGridEmployee.Columns[12].HeaderText = "DOJ";
            DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
            DataGridEmployee.Columns[12].Visible = false;

            DataGridEmployee.Columns[13].Name = "Sex";
            DataGridEmployee.Columns[13].HeaderText = "Sex";
            DataGridEmployee.Columns[13].DataPropertyName = "Sex";
            DataGridEmployee.Columns[13].Visible = false;

            DataGridEmployee.Columns[14].Name = "EmpAddress";
            DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
            DataGridEmployee.Columns[14].DataPropertyName = "Street";
            DataGridEmployee.Columns[14].Visible = false;

            DataGridEmployee.Columns[15].Name = "City";
            DataGridEmployee.Columns[15].HeaderText = "City";
            DataGridEmployee.Columns[15].DataPropertyName = "City";
            DataGridEmployee.Columns[15].Visible = false;

            DataGridEmployee.Columns[16].Name = "Phone";
            DataGridEmployee.Columns[16].HeaderText = "Phone";
            DataGridEmployee.Columns[16].DataPropertyName = "Phone";
            DataGridEmployee.Columns[16].Visible = false;

            DataGridEmployee.Columns[17].Name = "Information";
            DataGridEmployee.Columns[17].HeaderText = "Information";
            DataGridEmployee.Columns[17].DataPropertyName = "Information";
            DataGridEmployee.Columns[17].Visible = false;
            DataGridEmployee.Columns[18].Name = "Dispensary";
            DataGridEmployee.Columns[18].HeaderText = "Dispensary";
            DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
            DataGridEmployee.Columns[18].Visible = false;

            DataGridEmployee.Columns[19].Name = "WeekOff";
            DataGridEmployee.Columns[19].HeaderText = "WeekOff";
            DataGridEmployee.Columns[19].DataPropertyName = "woff";
            DataGridEmployee.Columns[19].Visible = false;

            DataGridEmployee.Columns[20].Name = "CL";
            DataGridEmployee.Columns[20].HeaderText = "CL";
            DataGridEmployee.Columns[20].DataPropertyName = "CL";
            DataGridEmployee.Columns[20].Visible = false;
            DataGridEmployee.Columns[21].Name = "EL";
            DataGridEmployee.Columns[21].HeaderText = "EL";
            DataGridEmployee.Columns[21].DataPropertyName = "EL";
            DataGridEmployee.Columns[21].Visible = false;

            DataGridEmployee.Columns[22].Name = "ESINo";
            DataGridEmployee.Columns[22].HeaderText = "ESINo";
            DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
            DataGridEmployee.Columns[22].Visible = false;

            DataGridEmployee.Columns[23].Name = "PFNo";
            DataGridEmployee.Columns[23].HeaderText = "PFNo";
            DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
            DataGridEmployee.Columns[23].Visible = false;

            DataGridEmployee.Columns[24].Name = "UANNo";
            DataGridEmployee.Columns[24].HeaderText = "UANNo";
            DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
            DataGridEmployee.Columns[24].Visible = false;

            DataGridEmployee.Columns[25].Name = "PANNo";
            DataGridEmployee.Columns[25].HeaderText = "PANNo";
            DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
            DataGridEmployee.Columns[25].Visible = false;

            DataGridEmployee.Columns[26].Name = "AadharNo";
            DataGridEmployee.Columns[26].HeaderText = "AadharNo";
            DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
            DataGridEmployee.Columns[26].Visible = false;

            DataGridEmployee.Columns[27].Name = "empphoto";
            DataGridEmployee.Columns[27].HeaderText = "empphoto";
            DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
            DataGridEmployee.Columns[27].Visible = false;

            DataGridEmployee.Columns[28].Name = "DesName";
            DataGridEmployee.Columns[28].HeaderText = "DesName";
            DataGridEmployee.Columns[28].DataPropertyName = "DesName";
            DataGridEmployee.Columns[28].Visible = false;


            DataGridEmployee.Columns[29].Name = "DeptName";
            DataGridEmployee.Columns[29].HeaderText = "DeptName";
            DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
            DataGridEmployee.Columns[29].Width = 150;

            DataGridEmployee.Columns[30].Name = "Grade";
            DataGridEmployee.Columns[30].HeaderText = "Grade";
            DataGridEmployee.Columns[30].DataPropertyName = "GrName";
            DataGridEmployee.Columns[30].Width = 150;

            DataGridEmployee.Columns[31].Name = "TName";
            DataGridEmployee.Columns[31].HeaderText = "TName";
            DataGridEmployee.Columns[31].DataPropertyName = "TName";
            DataGridEmployee.Columns[31].Visible = false;

            DataGridEmployee.Columns[32].Name = "CardNo";
            DataGridEmployee.Columns[32].HeaderText = "CardNo";
            DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
            DataGridEmployee.Columns[32].Width = 100;

            DataGridEmployee.Columns[33].Name = "emialtag";
            DataGridEmployee.Columns[33].HeaderText = "emialtag";
            DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
            DataGridEmployee.Columns[33].Visible = false;

            DataGridEmployee.Columns[34].Name = "isappriser";
            DataGridEmployee.Columns[34].HeaderText = "isappriser";
            DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
            DataGridEmployee.Columns[34].Visible = false;

            DataGridEmployee.Columns[35].Name = "appid";
            DataGridEmployee.Columns[35].HeaderText = "appid";
            DataGridEmployee.Columns[35].DataPropertyName = "appid";
            DataGridEmployee.Columns[35].Visible = false;

            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;
            DataGridEmployee.DataSource = tab;
        }

        private void Loadgrid1()
        {
            conn.Close();
            conn.Open();
            string quy = "exec SP_GetEmp_mastdelete";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.Refresh();
            DataGridEmployee.DataSource = null;
            DataGridEmployee.Rows.Clear();
            DataGridEmployee.ColumnCount = tab.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tab.Columns)
            {
                DataGridEmployee.Columns[Genclass.i].Name = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].HeaderText = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            DataGridEmployee.DataSource = null;
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.ColumnCount = 37;

            DataGridEmployee.Columns[0].Name = "EmpId";
            DataGridEmployee.Columns[0].HeaderText = "EmpId";
            DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
            DataGridEmployee.Columns[0].Visible = false;

            DataGridEmployee.Columns[1].Name = "EmpCode";
            DataGridEmployee.Columns[1].HeaderText = "EmpCode";
            DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
            DataGridEmployee.Columns[1].Width = 100;

            DataGridEmployee.Columns[2].Name = "EmpName";
            DataGridEmployee.Columns[2].HeaderText = "Employe Name";
            DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
            DataGridEmployee.Columns[2].Width = 200;

            DataGridEmployee.Columns[3].Name = "ShiftName";
            DataGridEmployee.Columns[3].HeaderText = "ShiftName";
            DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
            DataGridEmployee.Columns[3].Visible = false;

            DataGridEmployee.Columns[4].Name = "DeptId";
            DataGridEmployee.Columns[4].HeaderText = "DeptId";
            DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
            DataGridEmployee.Columns[4].Visible = false;

            DataGridEmployee.Columns[5].Name = "DesId";
            DataGridEmployee.Columns[5].HeaderText = "DesId";
            DataGridEmployee.Columns[5].DataPropertyName = "DesId";
            DataGridEmployee.Columns[5].Visible = false;

            DataGridEmployee.Columns[6].Name = "Grid";
            DataGridEmployee.Columns[6].HeaderText = "Grid";
            DataGridEmployee.Columns[6].DataPropertyName = "Grid";
            DataGridEmployee.Columns[6].Visible = false;

            DataGridEmployee.Columns[7].Name = "GrName";
            DataGridEmployee.Columns[7].HeaderText = "GrName";
            DataGridEmployee.Columns[7].DataPropertyName = "GrName";
            DataGridEmployee.Columns[7].Visible = false;

            DataGridEmployee.Columns[8].Name = "ShiftId";
            DataGridEmployee.Columns[8].HeaderText = "ShiftId";
            DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
            DataGridEmployee.Columns[8].Visible = false;

            DataGridEmployee.Columns[9].Name = "EmpTypeId";
            DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
            DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
            DataGridEmployee.Columns[9].Visible = false;

            DataGridEmployee.Columns[10].Name = "FName";
            DataGridEmployee.Columns[10].HeaderText = "FatherName";
            DataGridEmployee.Columns[10].DataPropertyName = "FName";
            DataGridEmployee.Columns[10].Width = 160;

            DataGridEmployee.Columns[11].Name = "DOB";
            DataGridEmployee.Columns[11].HeaderText = "DOB";
            DataGridEmployee.Columns[11].DataPropertyName = "DOB";
            DataGridEmployee.Columns[11].Visible = false;

            DataGridEmployee.Columns[12].Name = "DOJ";
            DataGridEmployee.Columns[12].HeaderText = "DOJ";
            DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
            DataGridEmployee.Columns[12].Visible = false;

            DataGridEmployee.Columns[13].Name = "Sex";
            DataGridEmployee.Columns[13].HeaderText = "Sex";
            DataGridEmployee.Columns[13].DataPropertyName = "Sex";
            DataGridEmployee.Columns[13].Visible = false;

            DataGridEmployee.Columns[14].Name = "EmpAddress";
            DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
            DataGridEmployee.Columns[14].DataPropertyName = "Street";
            DataGridEmployee.Columns[14].Visible = false;

            DataGridEmployee.Columns[15].Name = "City";
            DataGridEmployee.Columns[15].HeaderText = "City";
            DataGridEmployee.Columns[15].DataPropertyName = "City";
            DataGridEmployee.Columns[15].Visible = false;

            DataGridEmployee.Columns[16].Name = "Phone";
            DataGridEmployee.Columns[16].HeaderText = "Phone";
            DataGridEmployee.Columns[16].DataPropertyName = "Phone";
            DataGridEmployee.Columns[16].Visible = false;

            DataGridEmployee.Columns[17].Name = "Information";
            DataGridEmployee.Columns[17].HeaderText = "Information";
            DataGridEmployee.Columns[17].DataPropertyName = "Information";
            DataGridEmployee.Columns[17].Visible = false;

            DataGridEmployee.Columns[18].Name = "Dispensary";
            DataGridEmployee.Columns[18].HeaderText = "Dispensary";
            DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
            DataGridEmployee.Columns[18].Visible = false;

            DataGridEmployee.Columns[19].Name = "WeekOff";
            DataGridEmployee.Columns[19].HeaderText = "WeekOff";
            DataGridEmployee.Columns[19].DataPropertyName = "woff";
            DataGridEmployee.Columns[19].Visible = false;

            DataGridEmployee.Columns[20].Name = "CL";
            DataGridEmployee.Columns[20].HeaderText = "CL";
            DataGridEmployee.Columns[20].DataPropertyName = "CL";
            DataGridEmployee.Columns[20].Visible = false;

            DataGridEmployee.Columns[21].Name = "EL";
            DataGridEmployee.Columns[21].HeaderText = "EL";
            DataGridEmployee.Columns[21].DataPropertyName = "EL";
            DataGridEmployee.Columns[21].Visible = false;

            DataGridEmployee.Columns[22].Name = "ESINo";
            DataGridEmployee.Columns[22].HeaderText = "ESINo";
            DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
            DataGridEmployee.Columns[22].Visible = false;

            DataGridEmployee.Columns[23].Name = "PFNo";
            DataGridEmployee.Columns[23].HeaderText = "PFNo";
            DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
            DataGridEmployee.Columns[23].Visible = false;

            DataGridEmployee.Columns[24].Name = "UANNo";
            DataGridEmployee.Columns[24].HeaderText = "UANNo";
            DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
            DataGridEmployee.Columns[24].Visible = false;

            DataGridEmployee.Columns[25].Name = "PANNo";
            DataGridEmployee.Columns[25].HeaderText = "PANNo";
            DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
            DataGridEmployee.Columns[25].Visible = false;

            DataGridEmployee.Columns[26].Name = "AadharNo";
            DataGridEmployee.Columns[26].HeaderText = "AadharNo";
            DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
            DataGridEmployee.Columns[26].Visible = false;

            DataGridEmployee.Columns[27].Name = "empphoto";
            DataGridEmployee.Columns[27].HeaderText = "empphoto";
            DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
            DataGridEmployee.Columns[27].Visible = false;

            DataGridEmployee.Columns[28].Name = "DesName";
            DataGridEmployee.Columns[28].HeaderText = "DesName";
            DataGridEmployee.Columns[28].DataPropertyName = "DesName";
            DataGridEmployee.Columns[28].Visible = false;


            DataGridEmployee.Columns[29].Name = "DeptName";
            DataGridEmployee.Columns[29].HeaderText = "DeptName";
            DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
            DataGridEmployee.Columns[29].Width = 150;

            DataGridEmployee.Columns[30].Name = "Grade";
            DataGridEmployee.Columns[30].HeaderText = "Grade";
            DataGridEmployee.Columns[30].DataPropertyName = "Grade";
            DataGridEmployee.Columns[30].Width = 150;

            DataGridEmployee.Columns[31].Name = "TName";
            DataGridEmployee.Columns[31].HeaderText = "TName";
            DataGridEmployee.Columns[31].DataPropertyName = "TName";
            DataGridEmployee.Columns[31].Visible = false;

            DataGridEmployee.Columns[32].Name = "CardNo";
            DataGridEmployee.Columns[32].HeaderText = "CardNo";
            DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
            DataGridEmployee.Columns[32].Width = 100;

            DataGridEmployee.Columns[33].Name = "emialtag";
            DataGridEmployee.Columns[33].HeaderText = "emialtag";
            DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
            DataGridEmployee.Columns[33].Visible = false;

            DataGridEmployee.Columns[34].Name = "isappriser";
            DataGridEmployee.Columns[34].HeaderText = "isappriser";
            DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
            DataGridEmployee.Columns[34].Visible = false;

            DataGridEmployee.Columns[35].Name = "appid";
            DataGridEmployee.Columns[35].HeaderText = "appid";
            DataGridEmployee.Columns[35].DataPropertyName = "appid";
            DataGridEmployee.Columns[35].Visible = false;

            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;
            DataGridEmployee.DataSource = tab;
        }
        public DataTable GetData(string Procedurename)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void FillEMp(DataTable dataTable)
        {
            try
            {


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                DataTable dtDes = ds.Tables[1];
                DataTable DtEmpType = ds.Tables[2];
                DataTable dtGrade = ds.Tables[3];
                DataTable dtShift = ds.Tables[4];
                cmbDepartment.DataSource = null;
                cmbDepartment.DisplayMember = "DeptName";
                cmbDepartment.ValueMember = "DeptId";
                cmbDepartment.DataSource = dtDept;

                CmbQDepartment.DataSource = null;
                CmbQDepartment.DisplayMember = "DeptName";
                CmbQDepartment.ValueMember = "DeptId";
                CmbQDepartment.DataSource = dtDept;

                cmgfntbox.DataSource = null;
                cmgfntbox.DisplayMember = "DeptName";
                cmgfntbox.ValueMember = "DeptId";
                cmgfntbox.DataSource = dtDept;


                cmbDesinganation.DataSource = null;
                cmbDesinganation.DisplayMember = "DesName";
                cmbDesinganation.ValueMember = "DesId";
                cmbDesinganation.DataSource = dtDes;

                CmbQDesingnation.DataSource = null;
                CmbQDesingnation.DisplayMember = "DesName";
                CmbQDesingnation.ValueMember = "DesId";
                CmbQDesingnation.DataSource = dtDes;

                cmbCategory.DataSource = null;
                cmbCategory.DisplayMember = "GrName";
                cmbCategory.ValueMember = "GrId";
                cmbCategory.DataSource = dtGrade;

                cmbEmpType.DataSource = null;
                cmbEmpType.DisplayMember = "TSName";
                cmbEmpType.ValueMember = "TId";
                cmbEmpType.DataSource = DtEmpType;

                cmbShiftName.DataSource = null;
                cmbShiftName.DisplayMember = "ShiftName";
                cmbShiftName.ValueMember = "ShiftId";
                cmbShiftName.DataSource = dtShift;

                CmbQShiftName.DataSource = null;
                CmbQShiftName.DisplayMember = "ShiftName";
                CmbQShiftName.ValueMember = "ShiftId";
                CmbQShiftName.DataSource = dtShift;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            loadapp();
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            clextxt();
            //picEmpPhoto.Image = Image.FromFile
            //(System.Environment.GetFolderPath
            //(System.Environment.SpecialFolder.Personal)
            //+ @"\FEMP.jpg");

        }
        private void clextxt()

        {

            txtEmpCardNo.Text = string.Empty;
            txtEmpcode.Text = string.Empty;
            txtEmpName.Text = string.Empty;

            txtFatherName.Text = string.Empty;
            txtAadharNo.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtInfo.Text = string.Empty;

            txtmailid.Text = string.Empty;
            txtCL.Text = string.Empty;
            txtEL.Text = string.Empty;
            txtDispensary.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtPFNo.Text = string.Empty;

            txtESINo.Text = string.Empty;
            txtmailid.Text = string.Empty;
            txtUANNo.Text = string.Empty;


        }
        private void picEmpPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*";
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string sex = string.Empty;
                if (radioMale.Checked == true)
                {
                    sex = "M";
                }
                else
                {
                    sex = "F";
                }

                int chk = 0;
                if (IsAppriser.Checked == true)
                {
                    chk = 1;
                }
                else
                {
                    chk = 0;
                }
                int ll = 0;
                if (cmbapp.SelectedValue == null)

                {
                    ll = 0;

                }
                else
                {

                    ll = Convert.ToInt16(cmbapp.SelectedValue);
                }

                if(cmbDepartment.Text=="")
                    
                {
                    MessageBox.Show("Selcet the Department");
                    return;
                    
                }
                if (cmbDesinganation.Text == "")

                {
                    MessageBox.Show("Selcet the Desinganation");
                    return;

                }
                if (cmbCategory.Text == "")

                {
                    MessageBox.Show("Selcet the Category");
                    return;

                }
                if (cmbEmpType.Text == "")

                {
                    MessageBox.Show("Selcet the EmployeeType");
                    return;

                }
                if (cmbShiftName.Text == "")

                {
                    MessageBox.Show("Selcet the ShiftName");
                    return;

                }
                if (cmbGroup.Text == "")

                {
                    MessageBox.Show("Selcet the Group");
                    return;

                }
                if (cmbWeekOff.Text == "")

                {
                    MessageBox.Show("Selcet the WeekOff");
                    return;

                }
               
                byte[] photo_aray = null;
                //photo_aray.Length = byte[184978];
                if (picEmpPhoto.Image != null)
                {
                    MemoryStream ms = new MemoryStream();
                    picEmpPhoto.Image.Save(ms, ImageFormat.Jpeg);
                    photo_aray = new byte[ms.Length];
                    ms.Position = 0;
                    ms.Read(photo_aray, 0, photo_aray.Length);
                    //photo_aray = 184978;
                }
                SqlParameter[] parameters = {
                    new SqlParameter("@EmpId",EmpId),
                    new SqlParameter("@EmpName",txtEmpName.Text),
                    new SqlParameter("@EmpCode",txtEmpcode.Text),
                    new SqlParameter("@EmpCardNo",Convert.ToInt32(txtEmpCardNo.Text)),
                    new SqlParameter("@Deptid",Convert.ToInt32(cmbDepartment.SelectedValue)),
                    new SqlParameter("@DesId",Convert.ToInt32(cmbDesinganation.SelectedValue)),
                    new SqlParameter("@CtgyId",cmbCategory.SelectedValue),
                    new SqlParameter("@ShiftId",cmbShiftName.SelectedValue),
                    new SqlParameter("@EmpTypeId",cmbEmpType.SelectedValue),
                    new SqlParameter("@FatherName",txtFatherName.Text),
                    new SqlParameter("@DOB",Convert.ToDateTime(dtpDOB.Text)),
                    new SqlParameter("@DOJ",Convert.ToDateTime(dtpDOJ.Text)),
                    new SqlParameter("@Sex",sex),
                    new SqlParameter("@EmpAddress",txtAddress.Text),
                    new SqlParameter("@City",txtCity.Text),
                    new SqlParameter("@Phone",txtPhone.Text),
                    new SqlParameter("@Information",txtInfo.Text),
                    new SqlParameter("@Dispensary",GeneralParameters.Branch),
                    new SqlParameter("@WeekOff",cmbWeekOff.Text),
                    new SqlParameter("@CL",txtCL.Text),
                    new SqlParameter("@EL",txtEL.Text),
                    new SqlParameter("@ESINo",txtESINo.Text),
                    new SqlParameter("@PFNo",txtPFNo.Text),
                    new SqlParameter("@UANNo",txtUANNo.Text),
                    new SqlParameter("@PANNo",txtPanNo.Text),
                    new SqlParameter("@AadharNo",txtAadharNo.Text),
                    new SqlParameter("@Image",photo_aray),
                    new SqlParameter("@Eg",cmbGroup.Text),
                    new SqlParameter("@emailtag",txtmailid.Text),
                    new SqlParameter("@isappriser",chk),
                    new SqlParameter("@appid",ll),
                    new SqlParameter("@RtnMsg",SqlDbType.Int),
                    new SqlParameter("@pgid","0"),
                };
                parameters[31].Direction = ParameterDirection.Output;
                int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Emp_Mast", parameters, 31, conn);
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadButton(3);
                loadgrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Loadid = 1;
                clextxt();
                int Id = DataGridEmployee.SelectedCells[0].RowIndex;
                EmpId = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
                txtEmpName.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
                txtEmpcode.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
                txtEmpCardNo.Text = DataGridEmployee.Rows[Id].Cells[32].Value.ToString();
                cmbDepartment.SelectedValue = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
                cmbDesinganation.SelectedValue = DataGridEmployee.Rows[Id].Cells[5].Value.ToString();
                cmbCategory.SelectedValue = DataGridEmployee.Rows[Id].Cells[6].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[30].Value.ToString();
                if (string.IsNullOrEmpty(DataGridEmployee.Rows[Id].Cells[8].Value.ToString()))
                {
                    cmbShiftName.SelectedIndex = 1;
                }
                else
                {
                    cmbShiftName.SelectedValue = DataGridEmployee.Rows[Id].Cells[8].Value.ToString();
                }
                
                cmbEmpType.SelectedValue = DataGridEmployee.Rows[Id].Cells[9].Value.ToString();
                txtFatherName.Text = DataGridEmployee.Rows[Id].Cells[10].Value.ToString();
                dtpDOB.Text = DataGridEmployee.Rows[Id].Cells[11].Value.ToString();
                dtpDOJ.Text = DataGridEmployee.Rows[Id].Cells[12].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[13].Value.ToString() == "M")
                {
                    radioMale.Checked = true;
                }
                else
                {
                    RadioFemale.Checked = true;
                }
                txtAddress.Text = DataGridEmployee.Rows[Id].Cells[14].Value.ToString();
                txtCity.Text = DataGridEmployee.Rows[Id].Cells[15].Value.ToString();
                txtPhone.Text = DataGridEmployee.Rows[Id].Cells[16].Value.ToString();
                txtInfo.Text = DataGridEmployee.Rows[Id].Cells[17].Value.ToString();
                txtDispensary.Text = DataGridEmployee.Rows[Id].Cells[18].Value.ToString();
                cmbWeekOff.Text = DataGridEmployee.Rows[Id].Cells[19].Value.ToString();
                txtCL.Text = DataGridEmployee.Rows[Id].Cells[20].Value.ToString();
                txtEL.Text = DataGridEmployee.Rows[Id].Cells[21].Value.ToString();
                txtESINo.Text = DataGridEmployee.Rows[Id].Cells[22].Value.ToString();
                txtPFNo.Text = DataGridEmployee.Rows[Id].Cells[23].Value.ToString();
                txtUANNo.Text = DataGridEmployee.Rows[Id].Cells[24].Value.ToString();
                txtPanNo.Text = DataGridEmployee.Rows[Id].Cells[25].Value.ToString();
                txtAadharNo.Text = DataGridEmployee.Rows[Id].Cells[26].Value.ToString();
                cmbDepartment.Text = DataGridEmployee.Rows[Id].Cells[28].Value.ToString();
                cmbDesinganation.Text = DataGridEmployee.Rows[Id].Cells[29].Value.ToString();
                cmbCategory.Text = DataGridEmployee.Rows[Id].Cells[7].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[31].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[33].Value == null)
                {
                    txtmailid.Text = string.Empty;
                }
                else
                {
                    txtmailid.Text = DataGridEmployee.Rows[Id].Cells[33].Value.ToString();
                }
                if (DataGridEmployee.Rows[Id].Cells[34].Value.ToString() == "1")
                {
                    IsAppriser.Checked = true;
                }
                else
                {
                    IsAppriser.Checked = false;
                    if (DataGridEmployee.Rows[Id].Cells[33].Value != null)
                    {
                        cmbapp.SelectedValue = DataGridEmployee.Rows[Id].Cells[35].Value.ToString();
                    }
                    cmbapp.Text = DataGridEmployee.Rows[Id].Cells[36].Value.ToString();
                }
                conn.Close();
                conn.Open();
                string qur = "select empid,empname from emp_mast where isappriser=1 and empid<> " + EmpId + "";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cmbapp.DataSource = null;
                cmbapp.DataSource = tab;
                cmbapp.DisplayMember = "empname";
                cmbapp.ValueMember = "empid";
                cmbapp.SelectedIndex = -1;
                conn.Close();

                cmbShiftName.Text = DataGridEmployee.Rows[Id].Cells[3].Value.ToString();

                string qur1 = "Select      * from emp_mast where empid=" + EmpId + "";
                Genclass.cmd = new SqlCommand(qur1, conn);

                SqlDataAdapter adpt1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                adpt1.Fill(tap1);


                if (tap1.Rows[0]["Empphoto"].ToString() != string.Empty)
                {
                    byte[] photo_aray;
                    photo_aray = (byte[])tap1.Rows[0]["Empphoto"];
                    MemoryStream ms = new MemoryStream(photo_aray);
                    picEmpPhoto.Image = Image.FromStream(ms);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }

                LoadButton(2);
                Loadid = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*";
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                picEmpPhoto.Image = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //frpfnttt.Visible = false;
            //grFront.Visible = true;
            //grBack.Visible = false;
            //panFooter.Visible = true;
            //Loadsum();
            this.Close();
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            LoadButton(3);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frpfnttt.Visible = false;
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;

            loadgrid();
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                conn.Open();
                string qur = "Select a.EmpId,a.EmpCode,a.EmpName,f.ShiftName,a.DeptId,a.DesId,a.Grid,d.GrName,a.ShiftId,a.EmpTyp,a.FName,a.DOB,a.doj,a.Sex,a.street,a.City,a.phone,a.Information,a.Dispensary,a.woff,a.CL,a.EL,a.ESINo,a.EPFNo,a.UANNo,a.PANNo,a.AadharNo,a.empphoto,b.DesName,c.DeptName,a.EG as grade,e.TName,a.CardNo,a.emailtag,a.isappriser,a.appid,k.empname as kname from Emp_Mast a inner join Des_Mast b on a.DesId = b.DesId   inner  join Dept_Mast c on a.DeptId = c.DeptId inner join Gr_Mast d on a.GrId = d.GrId  inner  join EMP_TYPE e on a.EmpTyp = e.TId  inner       join Shift_Mast f on a.ShiftId = f.ShiftId  left  join emp_mast k on a.appid = k.EmpId where a.DeptId = " + cmgfntbox.SelectedValue + " and  a.EmpName like  '%" + txtSearch.Text + "%' or a.FName like  '%" + txtSearch.Text + "%'  or DeptName like  '%" + txtSearch.Text + "%'  or a.Eg like  '%" + txtSearch.Text + "%'  or a.CardNo like  '%" + txtSearch.Text + "%'    order by a.EmpId desc";
                Genclass.cmd = new SqlCommand(qur, conn);

                SqlDataAdapter adpt = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);

                DataGridEmployee.DataSource = null;
                DataGridEmployee.AutoGenerateColumns = false;
                DataGridEmployee.ColumnCount = 37;
                DataGridEmployee.Columns[0].Name = "EmpId";
                DataGridEmployee.Columns[0].HeaderText = "EmpId";
                DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
                DataGridEmployee.Columns[0].Visible = false;

                DataGridEmployee.Columns[1].Name = "EmpCode";
                DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
                DataGridEmployee.Columns[1].Width = 100;

                DataGridEmployee.Columns[2].Name = "EmpName";
                DataGridEmployee.Columns[2].HeaderText = "Employe Name";
                DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
                DataGridEmployee.Columns[2].Width = 200;

                DataGridEmployee.Columns[3].Name = "ShiftName";
                DataGridEmployee.Columns[3].HeaderText = "ShiftName";
                DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
                DataGridEmployee.Columns[3].Visible = false;

                DataGridEmployee.Columns[4].Name = "DeptId";
                DataGridEmployee.Columns[4].HeaderText = "DeptId";
                DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
                DataGridEmployee.Columns[4].Visible = false;

                DataGridEmployee.Columns[5].Name = "DesId";
                DataGridEmployee.Columns[5].HeaderText = "DesId";
                DataGridEmployee.Columns[5].DataPropertyName = "DesId";
                DataGridEmployee.Columns[5].Visible = false;

                DataGridEmployee.Columns[6].Name = "Grid";
                DataGridEmployee.Columns[6].HeaderText = "Grid";
                DataGridEmployee.Columns[6].DataPropertyName = "Grid";
                DataGridEmployee.Columns[6].Visible = false;

                DataGridEmployee.Columns[7].Name = "GrName";
                DataGridEmployee.Columns[7].HeaderText = "GrName";
                DataGridEmployee.Columns[7].DataPropertyName = "GrName";
                DataGridEmployee.Columns[7].Visible = false;

                DataGridEmployee.Columns[8].Name = "ShiftId";
                DataGridEmployee.Columns[8].HeaderText = "ShiftId";
                DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
                DataGridEmployee.Columns[8].Visible = false;

                DataGridEmployee.Columns[9].Name = "EmpTypeId";
                DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
                DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
                DataGridEmployee.Columns[9].Visible = false;

                DataGridEmployee.Columns[10].Name = "FName";
                DataGridEmployee.Columns[10].HeaderText = "FatherName";
                DataGridEmployee.Columns[10].DataPropertyName = "FName";
                DataGridEmployee.Columns[10].Width = 160;

                DataGridEmployee.Columns[11].Name = "DOB";
                DataGridEmployee.Columns[11].HeaderText = "DOB";
                DataGridEmployee.Columns[11].DataPropertyName = "DOB";
                DataGridEmployee.Columns[11].Visible = false;

                DataGridEmployee.Columns[12].Name = "DOJ";
                DataGridEmployee.Columns[12].HeaderText = "DOJ";
                DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
                DataGridEmployee.Columns[12].Visible = false;

                DataGridEmployee.Columns[13].Name = "Sex";
                DataGridEmployee.Columns[13].HeaderText = "Sex";
                DataGridEmployee.Columns[13].DataPropertyName = "Sex";
                DataGridEmployee.Columns[13].Visible = false;

                DataGridEmployee.Columns[14].Name = "EmpAddress";
                DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
                DataGridEmployee.Columns[14].DataPropertyName = "Street";
                DataGridEmployee.Columns[14].Visible = false;

                DataGridEmployee.Columns[15].Name = "City";
                DataGridEmployee.Columns[15].HeaderText = "City";
                DataGridEmployee.Columns[15].DataPropertyName = "City";
                DataGridEmployee.Columns[15].Visible = false;

                DataGridEmployee.Columns[16].Name = "Phone";
                DataGridEmployee.Columns[16].HeaderText = "Phone";
                DataGridEmployee.Columns[16].DataPropertyName = "Phone";
                DataGridEmployee.Columns[16].Visible = false;

                DataGridEmployee.Columns[17].Name = "Information";
                DataGridEmployee.Columns[17].HeaderText = "Information";
                DataGridEmployee.Columns[17].DataPropertyName = "Information";
                DataGridEmployee.Columns[17].Visible = false;
                DataGridEmployee.Columns[18].Name = "Dispensary";
                DataGridEmployee.Columns[18].HeaderText = "Dispensary";
                DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
                DataGridEmployee.Columns[18].Visible = false;

                DataGridEmployee.Columns[19].Name = "WeekOff";
                DataGridEmployee.Columns[19].HeaderText = "WeekOff";
                DataGridEmployee.Columns[19].DataPropertyName = "woff";
                DataGridEmployee.Columns[19].Visible = false;

                DataGridEmployee.Columns[20].Name = "CL";
                DataGridEmployee.Columns[20].HeaderText = "CL";
                DataGridEmployee.Columns[20].DataPropertyName = "CL";
                DataGridEmployee.Columns[20].Visible = false;
                DataGridEmployee.Columns[21].Name = "EL";
                DataGridEmployee.Columns[21].HeaderText = "EL";
                DataGridEmployee.Columns[21].DataPropertyName = "EL";
                DataGridEmployee.Columns[21].Visible = false;

                DataGridEmployee.Columns[22].Name = "ESINo";
                DataGridEmployee.Columns[22].HeaderText = "ESINo";
                DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
                DataGridEmployee.Columns[22].Visible = false;

                DataGridEmployee.Columns[23].Name = "PFNo";
                DataGridEmployee.Columns[23].HeaderText = "PFNo";
                DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
                DataGridEmployee.Columns[23].Visible = false;

                DataGridEmployee.Columns[24].Name = "UANNo";
                DataGridEmployee.Columns[24].HeaderText = "UANNo";
                DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
                DataGridEmployee.Columns[24].Visible = false;

                DataGridEmployee.Columns[25].Name = "PANNo";
                DataGridEmployee.Columns[25].HeaderText = "PANNo";
                DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
                DataGridEmployee.Columns[25].Visible = false;

                DataGridEmployee.Columns[26].Name = "AadharNo";
                DataGridEmployee.Columns[26].HeaderText = "AadharNo";
                DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
                DataGridEmployee.Columns[26].Visible = false;

                DataGridEmployee.Columns[27].Name = "empphoto";
                DataGridEmployee.Columns[27].HeaderText = "empphoto";
                DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
                DataGridEmployee.Columns[27].Visible = false;

                DataGridEmployee.Columns[28].Name = "DesName";
                DataGridEmployee.Columns[28].HeaderText = "DesName";
                DataGridEmployee.Columns[28].DataPropertyName = "DesName";
                DataGridEmployee.Columns[28].Visible = false;

                DataGridEmployee.Columns[29].Name = "DeptName";
                DataGridEmployee.Columns[29].HeaderText = "DeptName";
                DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
                DataGridEmployee.Columns[29].Width = 150;

                DataGridEmployee.Columns[30].Name = "Grade";
                DataGridEmployee.Columns[30].HeaderText = "Grade";
                DataGridEmployee.Columns[30].DataPropertyName = "Grade";
                DataGridEmployee.Columns[30].Width = 150;

                DataGridEmployee.Columns[31].Name = "TName";
                DataGridEmployee.Columns[31].HeaderText = "TName";
                DataGridEmployee.Columns[31].DataPropertyName = "TName";
                DataGridEmployee.Columns[31].Visible = false;

                DataGridEmployee.Columns[32].Name = "CardNo";
                DataGridEmployee.Columns[32].HeaderText = "CardNo";
                DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
                DataGridEmployee.Columns[32].Width = 100;
                DataGridEmployee.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frpfnttt.Visible = false;
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            Loadgrid1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmgfntbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Loadid == 0)
            {
                Loadgrid2();
            }
        }

        private void BtnQuickSave_Click(object sender, EventArgs e)
        {
            try
            {
                string sex = string.Empty;
                if (radioMale.Checked == true)
                {
                    sex = "Male";
                }
                else
                {
                    sex = "FeMale";
                }
                byte[] photo_aray = null;
                //photo_aray.Length = byte[184978];
                if (picEmpPhoto.Image != null)
                {
                    MemoryStream ms = new MemoryStream();
                    picEmpPhoto.Image.Save(ms, ImageFormat.Jpeg);
                    photo_aray = new byte[ms.Length];
                    ms.Position = 0;
                    ms.Read(photo_aray, 0, photo_aray.Length);
                    //photo_aray = 184978;
                }
                SqlParameter[] parameters = {
                    new SqlParameter("@EmpId","0"),
                    new SqlParameter("@EmpName",txtQEmpName.Text),
                    new SqlParameter("@EmpCode",txtQEmpCode.Text),
                    new SqlParameter("@EmpCardNo",Convert.ToInt32(txtQPunchCode.Text)),
                    new SqlParameter("@Deptid",Convert.ToInt32(CmbQDepartment.SelectedValue)),
                    new SqlParameter("@DesId",Convert.ToInt32(CmbQDesingnation.SelectedValue)),
                    new SqlParameter("@CtgyId",1),
                    new SqlParameter("@ShiftId",1),
                    new SqlParameter("@EmpTypeId",10),
                    new SqlParameter("@FatherName",txtQFatherName.Text),
                    new SqlParameter("@DOB",Convert.ToDateTime(DtpQDOB.Text)),
                    new SqlParameter("@DOJ",Convert.ToDateTime(DtpQDOJ.Text)),
                    new SqlParameter("@Sex",sex),
                    new SqlParameter("@EmpAddress",txtQAddress.Text),
                    new SqlParameter("@City",DBNull.Value),
                    new SqlParameter("@Phone",txtQPhone.Text),
                    new SqlParameter("@Information",DBNull.Value),
                    new SqlParameter("@Dispensary",DBNull.Value),
                    new SqlParameter("@WeekOff",CmbQWeekOff.Text),
                    new SqlParameter("@CL","0"),
                    new SqlParameter("@EL","0"),
                    new SqlParameter("@ESINo",DBNull.Value),
                    new SqlParameter("@PFNo",DBNull.Value),
                    new SqlParameter("@UANNo",DBNull.Value),
                    new SqlParameter("@PANNo",DBNull.Value),
                    new SqlParameter("@AadharNo",DBNull.Value),
                    new SqlParameter("@Image",photo_aray),
                    new SqlParameter("@Eg","STAFF"),
                    new SqlParameter("@emailtag",DBNull.Value),
                    new SqlParameter("@isappriser",DBNull.Value),
                    new SqlParameter("@appid",DBNull.Value),
                    new SqlParameter("@RtnMsg",SqlDbType.Int),
                };
                parameters[31].Direction = ParameterDirection.Output;
                int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Emp_Mast", parameters, 31, conn);
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearQuickControl();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void ClearQuickControl()
        {
            try
            {
                txtQEmpName.Text = string.Empty;
                txtQEmpCode.Text = string.Empty;
                txtQPunchCode.Text = string.Empty;
                CmbQDepartment.SelectedIndex = -1;
                CmbQDesingnation.SelectedIndex = -1;
                txtQFatherName.Text = string.Empty;
                txtQAddress.Text = string.Empty;
                CmbQShiftName.SelectedIndex = -1;
                CmbQWeekOff.SelectedIndex = -1;
                txtQPhone.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnQuickEmp_Click(object sender, EventArgs e)
        {
            grQuickEmp.Visible = true;
            panFooter.Visible = false;
            frpfnttt.Visible = false;
            grFront.Visible = false;
            grBack.Visible = false;
        }

        private void BtnQickBack_Click(object sender, EventArgs e)
        {
            loadgrid();
            grQuickEmp.Visible = false;
            panFooter.Visible = false;
            frpfnttt.Visible = true;
            grFront.Visible = false;
            grBack.Visible = false;
        }

        private void PicQEmpPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*";
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void frpfnttt_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
