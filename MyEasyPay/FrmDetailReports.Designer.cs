﻿namespace MyEasyPay
{
    partial class FrmDetailReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CmbReportType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.DataGridDepartment = new System.Windows.Forms.DataGridView();
            this.GrLateSummary = new System.Windows.Forms.GroupBox();
            this.BtnBack = new System.Windows.Forms.Button();
            this.BtnDownloadExcel = new System.Windows.Forms.Button();
            this.DataGridLateSummary = new System.Windows.Forms.DataGridView();
            this.ChckSelect = new System.Windows.Forms.CheckBox();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).BeginInit();
            this.GrLateSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLateSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.ChckSelect);
            this.GrFront.Controls.Add(this.DataGridDepartment);
            this.GrFront.Controls.Add(this.BtnExit);
            this.GrFront.Controls.Add(this.BtnPrint);
            this.GrFront.Controls.Add(this.label3);
            this.GrFront.Controls.Add(this.CmbReportType);
            this.GrFront.Controls.Add(this.label2);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Controls.Add(this.DtpToDate);
            this.GrFront.Controls.Add(this.dtpFromDate);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(7, 0);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(886, 496);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFromDate.Location = new System.Drawing.Point(416, 75);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(137, 26);
            this.dtpFromDate.TabIndex = 0;
            // 
            // DtpToDate
            // 
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToDate.Location = new System.Drawing.Point(416, 128);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(137, 26);
            this.DtpToDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(338, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "From Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(353, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "To Date";
            // 
            // CmbReportType
            // 
            this.CmbReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbReportType.FormattingEnabled = true;
            this.CmbReportType.Items.AddRange(new object[] {
            "Late Summary",
            "OT Summary"});
            this.CmbReportType.Location = new System.Drawing.Point(416, 182);
            this.CmbReportType.Name = "CmbReportType";
            this.CmbReportType.Size = new System.Drawing.Size(354, 26);
            this.CmbReportType.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(353, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "To Date";
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(416, 227);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(75, 33);
            this.BtnPrint.TabIndex = 6;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(513, 227);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(75, 33);
            this.BtnExit.TabIndex = 7;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // DataGridDepartment
            // 
            this.DataGridDepartment.AllowUserToAddRows = false;
            this.DataGridDepartment.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DataGridDepartment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDepartment.EnableHeadersVisualStyles = false;
            this.DataGridDepartment.Location = new System.Drawing.Point(6, 44);
            this.DataGridDepartment.Name = "DataGridDepartment";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.DataGridDepartment.RowHeadersVisible = false;
            this.DataGridDepartment.Size = new System.Drawing.Size(278, 439);
            this.DataGridDepartment.TabIndex = 8;
            // 
            // GrLateSummary
            // 
            this.GrLateSummary.Controls.Add(this.BtnBack);
            this.GrLateSummary.Controls.Add(this.BtnDownloadExcel);
            this.GrLateSummary.Controls.Add(this.DataGridLateSummary);
            this.GrLateSummary.Location = new System.Drawing.Point(7, 1);
            this.GrLateSummary.Name = "GrLateSummary";
            this.GrLateSummary.Size = new System.Drawing.Size(886, 496);
            this.GrLateSummary.TabIndex = 235;
            this.GrLateSummary.TabStop = false;
            this.GrLateSummary.Visible = false;
            // 
            // BtnBack
            // 
            this.BtnBack.Location = new System.Drawing.Point(6, 457);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(82, 33);
            this.BtnBack.TabIndex = 2;
            this.BtnBack.Text = "Back";
            this.BtnBack.UseVisualStyleBackColor = true;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // BtnDownloadExcel
            // 
            this.BtnDownloadExcel.Location = new System.Drawing.Point(745, 456);
            this.BtnDownloadExcel.Name = "BtnDownloadExcel";
            this.BtnDownloadExcel.Size = new System.Drawing.Size(128, 33);
            this.BtnDownloadExcel.TabIndex = 1;
            this.BtnDownloadExcel.Text = "Download Excel";
            this.BtnDownloadExcel.UseVisualStyleBackColor = true;
            this.BtnDownloadExcel.Click += new System.EventHandler(this.BtnDownloadExcel_Click);
            // 
            // DataGridLateSummary
            // 
            this.DataGridLateSummary.AllowUserToAddRows = false;
            this.DataGridLateSummary.BackgroundColor = System.Drawing.Color.White;
            this.DataGridLateSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridLateSummary.Location = new System.Drawing.Point(6, 19);
            this.DataGridLateSummary.Name = "DataGridLateSummary";
            this.DataGridLateSummary.RowHeadersVisible = false;
            this.DataGridLateSummary.Size = new System.Drawing.Size(869, 435);
            this.DataGridLateSummary.TabIndex = 0;
            // 
            // ChckSelect
            // 
            this.ChckSelect.AutoSize = true;
            this.ChckSelect.Location = new System.Drawing.Point(9, 20);
            this.ChckSelect.Name = "ChckSelect";
            this.ChckSelect.Size = new System.Drawing.Size(85, 22);
            this.ChckSelect.TabIndex = 9;
            this.ChckSelect.Text = "Select All";
            this.ChckSelect.UseVisualStyleBackColor = true;
            this.ChckSelect.CheckedChanged += new System.EventHandler(this.ChckSelect_CheckedChanged);
            // 
            // FrmDetailReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(899, 502);
            this.Controls.Add(this.GrLateSummary);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmDetailReports";
            this.Text = "FrmDetailReports";
            this.Load += new System.EventHandler(this.FrmDetailReports_Load);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).EndInit();
            this.GrLateSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLateSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.DateTimePicker DtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbReportType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DataGridDepartment;
        private System.Windows.Forms.GroupBox GrLateSummary;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.Button BtnDownloadExcel;
        private System.Windows.Forms.DataGridView DataGridLateSummary;
        private System.Windows.Forms.CheckBox ChckSelect;
    }
}