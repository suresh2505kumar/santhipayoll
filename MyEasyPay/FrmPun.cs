﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public partial class FrmPun : Form
    {
        public FrmPun()
        {
            InitializeComponent();
        }

        private DataGridViewComboBoxCell cbxComboRow6;
        private ComboBox cbxRow5;
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int EmpId = 0;
        private DataRow doc1;
        DataTable Docno = new DataTable(); int Loadid = 0;

        private void DtpDOB_ValueChanged(object sender, EventArgs e)
        {

        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Titlep();
            DateTime dateTime = Convert.ToDateTime(dtpDOB.Text);
            string quy = "Select  c.empid,a.empcode,c.empname,convert(nvarchar(5),a.in1,108) InTime," +
                "convert(nvarchar(5),a.Out1,108) as Breakin,convert(nvarchar(5),a.in2,108) as Breakout," +
                "convert(nvarchar(5),a.Out2,108) as Outtime,a.Lvtag " +
                "from Att_Reg" + dateTime.Month.ToString("00") + "  a  " +
                "inner join emp_mast c on a.empcode=c.empcode " +
                "Where a.Dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "' and LvTag ='" + CmbLeaveTag.Text + "'";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            for (int k = 0; k < tap.Rows.Count; k++)
            {
                var index = DataGridPunch.Rows.Add();
                DataGridPunch.Rows[index].Cells[0].Value = tap.Rows[k]["empid"].ToString();
                DataGridPunch.Rows[index].Cells[1].Value = tap.Rows[k]["empcode"].ToString();
                DataGridPunch.Rows[index].Cells[2].Value = tap.Rows[k]["empname"].ToString();
                DataGridPunch.Rows[index].Cells[3].Value = tap.Rows[k]["InTime"].ToString();
                DataGridPunch.Rows[index].Cells[4].Value = tap.Rows[k]["Breakin"].ToString();
                DataGridPunch.Rows[index].Cells[5].Value = tap.Rows[k]["Breakout"].ToString();
                DataGridPunch.Rows[index].Cells[6].Value = tap.Rows[k]["Outtime"].ToString();
                DataGridPunch.Rows[index].Cells[7].Value = tap.Rows[k]["lvtag"].ToString();
            }
        }

        private void Titlep()
        {

            DataGridPunch.AutoGenerateColumns = false;
            DataGridPunch.DataSource = null;
            DataGridPunch.ColumnCount = 8;

            DataGridPunch.Columns[0].Name = "EmpId";
            DataGridPunch.Columns[1].Name = "EmpCode";
            DataGridPunch.Columns[2].Name = "EmpName";
            DataGridPunch.Columns[3].Name = "In Time";
            DataGridPunch.Columns[4].Name = "Break In";
            DataGridPunch.Columns[5].Name = "Break Out";
            DataGridPunch.Columns[6].Name = "Out Time";
            DataGridPunch.Columns[7].Name = "Status";

            DataGridPunch.Columns[0].Visible = false;
            DataGridPunch.Columns[1].Width = 100;
            DataGridPunch.Columns[2].Width = 150;
            DataGridPunch.Columns[3].Width = 70;
            DataGridPunch.Columns[4].Width = 70;
            DataGridPunch.Columns[5].Width = 70;
            DataGridPunch.Columns[6].Width = 70;
            DataGridPunch.Columns[7].Width = 100;
        }
        private void FrmPun_Load(object sender, EventArgs e)
        {
            Loadid = 1;
            DataGridPunch.RowHeadersVisible = false;
            qur.Connection = conn;
            Titlep();
            DataTable dataTable = Ctype();
            CmbLeaveTag.DataSource = null;
            CmbLeaveTag.DisplayMember = "SName";
            CmbLeaveTag.ValueMember = "LvId";
            CmbLeaveTag.DataSource = dataTable;
            Loadid = 0;
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "")
            {
                if (DataGridPunch.CurrentCell.ColumnIndex == 3)
                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[3];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }
                else if (DataGridPunch.CurrentCell.ColumnIndex == 6)
                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[6];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }
                else if (DataGridPunch.CurrentCell.ColumnIndex == 7)
                {
                    DataGridViewComboBoxCell l_objGridDropbox = new DataGridViewComboBoxCell();
                    if (DataGridPunch.CurrentRow.Cells[7].Value == null || DataGridPunch.CurrentRow.Cells[7].Value.ToString() != "")
                    {
                        DataTable nm = Ctype();
                        DataGridPunch[e.ColumnIndex, e.RowIndex] = l_objGridDropbox;
                        l_objGridDropbox.DataSource = nm; // Bind combobox with datasource.  
                        l_objGridDropbox.DisplayMember = "sname";
                        l_objGridDropbox.ValueMember = "sname";
                    }
                }
            }
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.RowCount > 1)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "0")
                    {
                        if (DataGridPunch.CurrentCell.ColumnIndex == 3 || DataGridPunch.CurrentCell.ColumnIndex == 6 || DataGridPunch.CurrentCell.ColumnIndex == 7)
                        {
                            if (string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[3].Value.ToString()) || string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[6].Value.ToString()) || string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[7].Value.ToString()))
                            {
                                MessageBox.Show("Status and IN OUT ccan't empty", "Infrmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            if (DataGridPunch.CurrentRow.Cells[3].Value.ToString() != string.Empty && DataGridPunch.CurrentRow.Cells[6].Value.ToString() != string.Empty && DataGridPunch.CurrentRow.Cells[7].Value.ToString() != "Ir")
                            {
                                if (conn.State == ConnectionState.Open)
                                {
                                    conn.Close();
                                }
                                conn.Open();
                                DateTime dateTime = Convert.ToDateTime(dtpDOB.Text);
                                qur.CommandText = "update Att_Reg" + dateTime.Month.ToString("00") + " set  in1='" + DataGridPunch.CurrentRow.Cells[3].Value.ToString() + "' ,Out2='" + DataGridPunch.CurrentRow.Cells[6].Value.ToString() + "' ,lvtag='" + DataGridPunch.CurrentRow.Cells[7].Value.ToString() + "',Ashift=1  where  empid=" + DataGridPunch.CurrentRow.Cells[0].Value.ToString() + "  AND DT='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
                                qur.ExecuteNonQuery();
                                SqlParameter[] sqlParameters = {
                                    new SqlParameter("@empid", DataGridPunch.CurrentRow.Cells[0].Value.ToString()),
                                    new SqlParameter("@empcode",DataGridPunch.CurrentRow.Cells[1].Value.ToString()),
                                    new SqlParameter("@empname",DataGridPunch.CurrentRow.Cells[2].Value.ToString()),
                                    new SqlParameter("@intime",DataGridPunch.CurrentRow.Cells[3].Value.ToString()),
                                    new SqlParameter("@breakin",DataGridPunch.CurrentRow.Cells[4].Value.ToString()),
                                    new SqlParameter("@breakout",DataGridPunch.CurrentRow.Cells[5].Value.ToString()),
                                    new SqlParameter("@outtime",DataGridPunch.CurrentRow.Cells[6].Value.ToString()),
                                    new SqlParameter("@lvtag",DataGridPunch.CurrentRow.Cells[7].Value.ToString()),
                                    new SqlParameter("@dt",dateTime.ToString("yyyy-MM-dd"))
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_Pundetlogfile", sqlParameters, conn);
                                conn.Close();
                                Titlep();
                            }
                            else
                            {
                                MessageBox.Show("Status can't set Ir", "Infrmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                        }
                    }
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private DataTable Ctype()
        {
            DataTable tab = new DataTable();
            if (Loadid == 0)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    string qur = "select sname,lvid from leave Order by LvId";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    apt.Fill(tab);
                }
            }
            else
            {
                string qur = "select sname,lvid from leave Order by LvId";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                apt.Fill(tab);
                tab = tab.Select("sname in ('Ir','Ab','Wo')", null).CopyToDataTable();
            }
            return tab;
        }

        private void CbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.DataGridPunch.CurrentRow.Cells[7].Value = this.cbxRow5.Text;
        }

        private void HFIT_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (DataGridPunch.CurrentCell.ColumnIndex == 7)
            {
                if (DataGridPunch.CurrentCell.ColumnIndex == DataGridPunch.CurrentRow.Cells[7].ColumnIndex)
                {
                    if (e.Control is ComboBox)
                    {
                        cbxRow5 = e.Control as ComboBox;
                        cbxRow5.SelectionChangeCommitted -= new EventHandler(CbxItemCode_SelectionChangeCommitted);
                        cbxRow5.SelectionChangeCommitted += new EventHandler(CbxItemCode_SelectionChangeCommitted);
                    }
                }
            }
        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellClick(sender, e);
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellValueChanged(sender, e);
        }

        private void HFIT_EditingControlShowing_2(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            HFIT_EditingControlShowing_1(sender, e);
        }

        private void CmbLeaveTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Loadid == 0)
                {
                    DataGridPunch.Rows.Clear();
                    BtnExit_Click(sender, e);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void DataGridPunch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
