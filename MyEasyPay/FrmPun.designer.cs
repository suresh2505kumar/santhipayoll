﻿namespace MyEasyPay
{
    partial class FrmPun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.BtnPunchDetails = new System.Windows.Forms.Button();
            this.txtPincode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.DataGridPunch = new System.Windows.Forms.DataGridView();
            this.CmbLeaveTag = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPunch)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpDOB
            // 
            this.dtpDOB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(57, 12);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(111, 27);
            this.dtpDOB.TabIndex = 7;
            this.dtpDOB.ValueChanged += new System.EventHandler(this.DtpDOB_ValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(11, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 19);
            this.label27.TabIndex = 57;
            this.label27.Text = "Date";
            // 
            // BtnPunchDetails
            // 
            this.BtnPunchDetails.BackColor = System.Drawing.Color.White;
            this.BtnPunchDetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnPunchDetails.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPunchDetails.Location = new System.Drawing.Point(566, 14);
            this.BtnPunchDetails.Name = "BtnPunchDetails";
            this.BtnPunchDetails.Size = new System.Drawing.Size(124, 26);
            this.BtnPunchDetails.TabIndex = 59;
            this.BtnPunchDetails.Text = "Punch Details";
            this.BtnPunchDetails.UseVisualStyleBackColor = false;
            this.BtnPunchDetails.Visible = false;
            this.BtnPunchDetails.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // txtPincode
            // 
            this.txtPincode.Location = new System.Drawing.Point(409, 258);
            this.txtPincode.MaxLength = 6;
            this.txtPincode.Name = "txtPincode";
            this.txtPincode.Size = new System.Drawing.Size(120, 20);
            this.txtPincode.TabIndex = 61;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(621, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(52, 26);
            this.button1.TabIndex = 62;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // DataGridPunch
            // 
            this.DataGridPunch.BackgroundColor = System.Drawing.Color.White;
            this.DataGridPunch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridPunch.Location = new System.Drawing.Point(12, 49);
            this.DataGridPunch.Name = "DataGridPunch";
            this.DataGridPunch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DataGridPunch.Size = new System.Drawing.Size(661, 439);
            this.DataGridPunch.TabIndex = 401;
            this.DataGridPunch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick_1);
            this.DataGridPunch.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridPunch_CellContentClick);
            this.DataGridPunch.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged_1);
            this.DataGridPunch.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.HFIT_EditingControlShowing_2);
            // 
            // CmbLeaveTag
            // 
            this.CmbLeaveTag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbLeaveTag.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbLeaveTag.FormattingEnabled = true;
            this.CmbLeaveTag.Location = new System.Drawing.Point(253, 16);
            this.CmbLeaveTag.Name = "CmbLeaveTag";
            this.CmbLeaveTag.Size = new System.Drawing.Size(155, 26);
            this.CmbLeaveTag.TabIndex = 402;
            this.CmbLeaveTag.SelectedIndexChanged += new System.EventHandler(this.CmbLeaveTag_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(176, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 19);
            this.label1.TabIndex = 403;
            this.label1.Text = "Leave Tag";
            // 
            // FrmPun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(681, 492);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbLeaveTag);
            this.Controls.Add(this.DataGridPunch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtnPunchDetails);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(this.txtPincode);
            this.Name = "FrmPun";
            this.Text = "Employee Punch Details";
            this.Load += new System.EventHandler(this.FrmPun_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPunch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button BtnPunchDetails;
        private System.Windows.Forms.TextBox txtPincode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DataGridPunch;
        private System.Windows.Forms.ComboBox CmbLeaveTag;
        private System.Windows.Forms.Label label1;
    }
}