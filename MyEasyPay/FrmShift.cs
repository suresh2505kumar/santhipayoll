﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmShift : Form
    {
        public FrmShift()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SQLDBHelper db = new SQLDBHelper();
        int EditId = 0;
        private void FrmShift_Load(object sender, EventArgs e)
        {
            Loadshift();
            LoadButton(3);
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            try
            {
                clearcontrol();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridShift.SelectedCells[0].RowIndex;
                EditId = Convert.ToInt32(DataGridShift.Rows[Index].Cells[0].Value);
                txtShiftName.Text = DataGridShift.Rows[Index].Cells[1].Value.ToString();
                DateTime InTime = new DateTime();
                DateTime OutTime = new DateTime();
                DateTime BOutIme = new DateTime();
                DateTime BInTime = new DateTime();
                InTime = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[2].Value.ToString());
                OutTime = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[3].Value.ToString());
                BOutIme = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[4].Value.ToString());
                BInTime = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[5].Value.ToString());
                string InTimeS = InTime.Hour.ToString("00") + ":" + InTime.Minute.ToString("00");
                string OutTimeS = OutTime.Hour.ToString("00") + ":" + OutTime.Minute.ToString("00");
                string BOut = BOutIme.Hour.ToString("00") + ":" + BOutIme.Minute.ToString("00");
                string BIn = BInTime.Hour.ToString("00") + ":" + BInTime.Minute.ToString("00");
                txtInTime.Text = InTimeS;
                txtOutTime.Text = OutTimeS;
                txtBreakOut.Text = BOut;
                txtBreakIn.Text = BIn;
                if (DataGridShift.Rows[Index].Cells[6].Value.ToString() != "0")
                {
                    chckNightShift.Checked = true;
                }
                else
                {
                    chckNightShift.Checked = false;
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtShiftName.Text != string.Empty && txtInTime.Text != string.Empty && txtOutTime.Text != string.Empty)
                {
                    int NightShift = 0;
                    if(chckNightShift.Checked == true)
                    {
                        NightShift = 1;
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@ShiftId",EditId),
                        new SqlParameter("@ShiftName",txtShiftName.Text),
                        new SqlParameter("@InTime",txtInTime.Text),
                        new SqlParameter("@OutTime",txtOutTime.Text),
                        new SqlParameter("@BreakOut",txtBreakOut.Text),
                        new SqlParameter("@BreakIn",txtBreakIn.Text),
                        new SqlParameter("@NS",NightShift),
                        new SqlParameter("@ReturnMs",SqlDbType.Int)
                    };
                    parameters[7].Direction = ParameterDirection.Output;
                    int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Shift_Mast", parameters, 7, conn);
                    MessageBox.Show("Record has been saved sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearcontrol();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void clearcontrol()
        {
            txtShiftName.Text = string.Empty;
            txtInTime.Text = string.Empty;
            txtOutTime.Text = string.Empty;
            txtBreakIn.Text = string.Empty;
            txtBreakOut.Text = string.Empty;
        }

        protected void Loadshift()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetSHift_mast", conn);
                DataGridShift.DataSource = null;
                DataGridShift.AutoGenerateColumns = false;
                DataGridShift.ColumnCount = 7;

                DataGridShift.Columns[0].Name = "ID";
                DataGridShift.Columns[0].HeaderText = "ID";
                DataGridShift.Columns[0].DataPropertyName = "ShiftID";
                DataGridShift.Columns[0].Visible = false;

                DataGridShift.Columns[1].Name = "ShiftName";
                DataGridShift.Columns[1].HeaderText = "Shift Name";
                DataGridShift.Columns[1].DataPropertyName = "ShiftName";
                DataGridShift.Columns[1].Width = 150;

                DataGridShift.Columns[2].Name = "InTime";
                DataGridShift.Columns[2].HeaderText = "In Time";
                DataGridShift.Columns[2].DataPropertyName = "InTime";
                DataGridShift.Columns[2].DefaultCellStyle.Format = "HH:mm";

                DataGridShift.Columns[3].Name = "OutTime";
                DataGridShift.Columns[3].HeaderText = "Out Time";
                DataGridShift.Columns[3].DataPropertyName = "OutTime";
                DataGridShift.Columns[3].DefaultCellStyle.Format = "HH:mm";

                DataGridShift.Columns[4].Name = "BreakOut";
                DataGridShift.Columns[4].HeaderText = "Break Out";
                DataGridShift.Columns[4].DataPropertyName = "BreakOut";
                DataGridShift.Columns[4].DefaultCellStyle.Format = "HH:mm";

                DataGridShift.Columns[5].Name = "BreakIn";
                DataGridShift.Columns[5].HeaderText = "Break In";
                DataGridShift.Columns[5].DataPropertyName = "BreakIn";
                DataGridShift.Columns[5].DefaultCellStyle.Format = "HH:mm";

                DataGridShift.Columns[6].Name = "NS";
                DataGridShift.Columns[6].HeaderText = "NS";
                DataGridShift.Columns[6].DataPropertyName = "NS";
                DataGridShift.Columns[6].Visible = false;
                DataGridShift.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Loadshift();
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridShift.SelectedCells[0].RowIndex;
                EditId = Convert.ToInt32(DataGridShift.Rows[Index].Cells[0].Value);
                SqlParameter[] parameters = {
                    new SqlParameter("@ShiftId",EditId),
                    new SqlParameter("@RTN",SqlDbType.Int)
                };
                parameters[1].Direction = ParameterDirection.Output;
                int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteSHift_mast", parameters, 1, conn);
                if(id == 0)
                {
                    MessageBox.Show("Shift Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Shift refred to employees can't Delete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Loadshift();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
