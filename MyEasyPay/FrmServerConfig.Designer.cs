﻿namespace MyEasyPay
{
    partial class FrmServerConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grConfg = new System.Windows.Forms.GroupBox();
            this.BtnGenerate = new System.Windows.Forms.Button();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRead = new System.Windows.Forms.Button();
            this.chckLicense = new System.Windows.Forms.CheckBox();
            this.grConfg.SuspendLayout();
            this.SuspendLayout();
            // 
            // grConfg
            // 
            this.grConfg.Controls.Add(this.chckLicense);
            this.grConfg.Controls.Add(this.btnRead);
            this.grConfg.Controls.Add(this.BtnGenerate);
            this.grConfg.Controls.Add(this.txtCompanyId);
            this.grConfg.Controls.Add(this.txtDBName);
            this.grConfg.Controls.Add(this.txtPassword);
            this.grConfg.Controls.Add(this.txtUserName);
            this.grConfg.Controls.Add(this.txtServer);
            this.grConfg.Controls.Add(this.label5);
            this.grConfg.Controls.Add(this.label4);
            this.grConfg.Controls.Add(this.label3);
            this.grConfg.Controls.Add(this.label2);
            this.grConfg.Controls.Add(this.label1);
            this.grConfg.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grConfg.Location = new System.Drawing.Point(8, 0);
            this.grConfg.Name = "grConfg";
            this.grConfg.Size = new System.Drawing.Size(480, 285);
            this.grConfg.TabIndex = 0;
            this.grConfg.TabStop = false;
            // 
            // BtnGenerate
            // 
            this.BtnGenerate.Location = new System.Drawing.Point(113, 240);
            this.BtnGenerate.Name = "BtnGenerate";
            this.BtnGenerate.Size = new System.Drawing.Size(98, 32);
            this.BtnGenerate.TabIndex = 10;
            this.BtnGenerate.Text = "Generate";
            this.BtnGenerate.UseVisualStyleBackColor = true;
            this.BtnGenerate.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(113, 204);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(276, 26);
            this.txtCompanyId.TabIndex = 9;
            // 
            // txtDBName
            // 
            this.txtDBName.Location = new System.Drawing.Point(113, 163);
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(276, 26);
            this.txtDBName.TabIndex = 8;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(113, 119);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(276, 26);
            this.txtPassword.TabIndex = 7;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(113, 71);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(276, 26);
            this.txtUserName.TabIndex = 6;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(113, 25);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(276, 26);
            this.txtServer.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Company ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "DB Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "UserName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server Name";
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(217, 240);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(98, 32);
            this.btnRead.TabIndex = 11;
            this.btnRead.Text = "Read Config";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // chckLicense
            // 
            this.chckLicense.AutoSize = true;
            this.chckLicense.Location = new System.Drawing.Point(321, 246);
            this.chckLicense.Name = "chckLicense";
            this.chckLicense.Size = new System.Drawing.Size(73, 22);
            this.chckLicense.TabIndex = 12;
            this.chckLicense.Text = "License";
            this.chckLicense.UseVisualStyleBackColor = true;
            // 
            // FrmServerConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 297);
            this.Controls.Add(this.grConfg);
            this.Name = "FrmServerConfig";
            this.Text = "FrmServerConfig";
            this.Load += new System.EventHandler(this.FrmServerConfig_Load);
            this.grConfg.ResumeLayout(false);
            this.grConfg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grConfg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnGenerate;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.TextBox txtDBName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.CheckBox chckLicense;
    }
}