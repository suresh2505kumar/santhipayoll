﻿using System;
using System.Windows.Forms;
using System.IO;

namespace MyEasyPay
{
    public partial class FrmServerConfig : Form
    {
        public FrmServerConfig()
        {
            InitializeComponent();
        }
        string EnValue = string.Empty;
        private string key = "o7x8y6";
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string path;
                if (chckLicense.Checked == true)
                {
                    path = Path.Combine(Application.StartupPath, "configL.LCG");
                }
                else
                {
                    path = Path.Combine(Application.StartupPath, "config.LCG");
                }

                if (!File.Exists(path))
                {
                    FileStream fs1 = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs1.Close();

                }
                // If File is already Exists trucate and rewrite the Encrypted Value
                var fs = new FileStream(path, FileMode.Truncate);
                fs.Close();
                if (txtServer.Text != string.Empty && txtUserName.Text != string.Empty && txtDBName.Text != string.Empty)
                {
                    EnValue = string.Empty;
                    EnValue = txtServer.Text;
                    string encryptionServer = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionServer + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtUserName.Text;
                    string encryptionUserName = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionUserName + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtPassword.Text;
                    string encryptionPassword = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionPassword + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtDBName.Text;
                    string encryptionDbName = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionDbName + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtCompanyId.Text;
                    string encryptionCompanyID = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionCompanyID + Environment.NewLine);
                }
                MessageBox.Show("Generated Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtServer.Text = string.Empty;
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtDBName.Text = string.Empty;
                txtCompanyId.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                string path;
                if (chckLicense.Checked == true)
                {
                    path = Path.Combine(Application.StartupPath, "configL.LCG");
                }
                else
                {
                    path = Path.Combine(Application.StartupPath, "config.LCG");
                }

                string[] readText = File.ReadAllLines(path);
                for (int i = 0; i < readText.Length; i++)
                {
                    if (i == 0)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        txtServer.Text = txt;
                    }
                    if (i == 1)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        txtUserName.Text = txt;
                    }
                    if (i == 2)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        txtPassword.Text = txt;
                    }
                    if (i == 3)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        txtDBName.Text = txt;
                    }
                    if (i == 4)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        txtCompanyId.Text = txt;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void FrmServerConfig_Load(object sender, EventArgs e)
        {

        }
    }
}
