﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmGeneratePay : Form
    {
        public FrmGeneratePay()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SQLDBHelper db = new SQLDBHelper();

        private void FrmGeneratePay_Load(object sender, EventArgs e)
        {
            Loadmonth();
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnView_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbMonth.SelectedIndex != -1)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Monid",CmbMonth.SelectedValue)
                    };
                    DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Get_PayRoll", sqlParameters, conn);
                    System.Data.DataRow row = data.NewRow();
                    row["EMPCODE"] = "Total";
                    for (int i = 0; i < data.Columns.Count; i++)
                    {
                        string name = data.Columns[i].ToString();
                        if (name == "Basic")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["Basic"] = sum;
                        }
                        if (name == "GROSS")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["GROSS"] = sum;
                        }
                        if (name == "FOOD")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["FOOD"] = sum;
                        }
                        if (name == "EPF")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["EPF"] = sum;
                        }
                        if (name == "ESI")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["ESI"] = sum;
                        }
                        if (name == "LOAN")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["LOAN"] = sum;
                        }
                        if (name == "ADVANCE")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["ADVANCE"] = sum;
                        }
                        if (name == "TOTALDED")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["TOTALDED"] = sum;
                        }
                        if (name == "NET")
                        {
                            decimal sum = 0;
                            for (int j = 0; j < data.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(data.Rows[j][i].ToString()))
                                {
                                    sum += 0;
                                }
                                else
                                {
                                    sum += Convert.ToDecimal(data.Rows[j][i].ToString());
                                }
                            }
                            row["NET"] = sum;
                        }
                    }
                    data.Rows.Add(row);
                    DataGridPay.DataSource = null;
                    DataGridPay.DataSource = data;
                    DataGridPay.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Salary Register";
                for (int i = 0; i < DataGridPay.Columns.Count; i++)
                {
                    worksheet.Cells[1, i + 1] = DataGridPay.Columns[i].HeaderText;

                }
                for (int i = 0; i < DataGridPay.Rows.Count; i++)
                {
                    for (int j = 0; j < DataGridPay.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = DataGridPay.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
