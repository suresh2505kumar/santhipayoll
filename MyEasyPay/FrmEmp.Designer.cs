﻿namespace MyEasyPay
{
    partial class FrmEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grFront = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridEmployee = new System.Windows.Forms.DataGridView();
            this.panFooter = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.picEmpPhoto = new System.Windows.Forms.PictureBox();
            this.txtAadharNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPanNo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtUANNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtPFNo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtESINo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEL = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCL = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDispensary = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbShiftName = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbWeekOff = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbEmpType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInfo = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.radioMale = new System.Windows.Forms.RadioButton();
            this.dtpDOJ = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDesinganation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmpCardNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmpcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.frpfnttt = new System.Windows.Forms.Panel();
            this.fntgrid = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.Frmdt = new System.Windows.Forms.DateTimePicker();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).BeginInit();
            this.panFooter.SuspendLayout();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).BeginInit();
            this.frpfnttt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fntgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridEmployee);
            this.grFront.Location = new System.Drawing.Point(0, 0);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(922, 488);
            this.grFront.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(12, 12);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(896, 20);
            this.txtSearch.TabIndex = 3;
            // 
            // DataGridEmployee
            // 
            this.DataGridEmployee.AllowUserToAddRows = false;
            this.DataGridEmployee.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridEmployee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmployee.EnableHeadersVisualStyles = false;
            this.DataGridEmployee.Location = new System.Drawing.Point(11, 41);
            this.DataGridEmployee.Name = "DataGridEmployee";
            this.DataGridEmployee.ReadOnly = true;
            this.DataGridEmployee.RowHeadersVisible = false;
            this.DataGridEmployee.Size = new System.Drawing.Size(897, 439);
            this.DataGridEmployee.TabIndex = 2;
            // 
            // panFooter
            // 
            this.panFooter.BackColor = System.Drawing.Color.White;
            this.panFooter.Controls.Add(this.btnEdit);
            this.panFooter.Controls.Add(this.btnAdd);
            this.panFooter.Controls.Add(this.btnExit);
            this.panFooter.Controls.Add(this.btnSave);
            this.panFooter.Controls.Add(this.btnBack);
            this.panFooter.Controls.Add(this.btnDelete);
            this.panFooter.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panFooter.Location = new System.Drawing.Point(5, 486);
            this.panFooter.Name = "panFooter";
            this.panFooter.Size = new System.Drawing.Size(917, 41);
            this.panFooter.TabIndex = 4;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Location = new System.Drawing.Point(675, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(76, 28);
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Location = new System.Drawing.Point(597, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(76, 28);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Location = new System.Drawing.Point(830, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Location = new System.Drawing.Point(753, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Location = new System.Drawing.Point(831, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(76, 28);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Location = new System.Drawing.Point(753, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(76, 28);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.btnClear);
            this.grBack.Controls.Add(this.btnBrowse);
            this.grBack.Controls.Add(this.picEmpPhoto);
            this.grBack.Controls.Add(this.txtAadharNo);
            this.grBack.Controls.Add(this.label25);
            this.grBack.Controls.Add(this.txtPanNo);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.txtUANNo);
            this.grBack.Controls.Add(this.label23);
            this.grBack.Controls.Add(this.txtPFNo);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.txtESINo);
            this.grBack.Controls.Add(this.label21);
            this.grBack.Controls.Add(this.txtEL);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.txtCL);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.txtDispensary);
            this.grBack.Controls.Add(this.label18);
            this.grBack.Controls.Add(this.cmbShiftName);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.cmbWeekOff);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.cmbEmpType);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.cmbGroup);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.cmbCategory);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.txtPhone);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.txtCity);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.txtInfo);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtAddress);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.RadioFemale);
            this.grBack.Controls.Add(this.radioMale);
            this.grBack.Controls.Add(this.dtpDOJ);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.dtpDOB);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.txtFatherName);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.cmbDesinganation);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtEmpCardNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtEmpcode);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtEmpName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.cmbDepartment);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Location = new System.Drawing.Point(0, 87);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(922, 401);
            this.grBack.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(833, 362);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 32);
            this.btnClear.TabIndex = 109;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(731, 362);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 32);
            this.btnBrowse.TabIndex = 108;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // picEmpPhoto
            // 
            this.picEmpPhoto.BackColor = System.Drawing.Color.White;
            this.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picEmpPhoto.Location = new System.Drawing.Point(731, 129);
            this.picEmpPhoto.Name = "picEmpPhoto";
            this.picEmpPhoto.Size = new System.Drawing.Size(177, 226);
            this.picEmpPhoto.TabIndex = 107;
            this.picEmpPhoto.TabStop = false;
            // 
            // txtAadharNo
            // 
            this.txtAadharNo.Location = new System.Drawing.Point(469, 333);
            this.txtAadharNo.Name = "txtAadharNo";
            this.txtAadharNo.Size = new System.Drawing.Size(208, 20);
            this.txtAadharNo.TabIndex = 93;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(393, 336);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 13);
            this.label25.TabIndex = 106;
            this.label25.Text = "Aadhar No";
            // 
            // txtPanNo
            // 
            this.txtPanNo.Location = new System.Drawing.Point(469, 292);
            this.txtPanNo.Name = "txtPanNo";
            this.txtPanNo.Size = new System.Drawing.Size(208, 20);
            this.txtPanNo.TabIndex = 92;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(411, 295);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 105;
            this.label24.Text = "PAN No";
            // 
            // txtUANNo
            // 
            this.txtUANNo.Location = new System.Drawing.Point(469, 254);
            this.txtUANNo.Name = "txtUANNo";
            this.txtUANNo.Size = new System.Drawing.Size(208, 20);
            this.txtUANNo.TabIndex = 90;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(409, 257);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 104;
            this.label23.Text = "UAN No";
            // 
            // txtPFNo
            // 
            this.txtPFNo.Location = new System.Drawing.Point(469, 217);
            this.txtPFNo.Name = "txtPFNo";
            this.txtPFNo.Size = new System.Drawing.Size(208, 20);
            this.txtPFNo.TabIndex = 89;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(415, 220);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 103;
            this.label22.Text = "EPF No";
            // 
            // txtESINo
            // 
            this.txtESINo.Location = new System.Drawing.Point(469, 183);
            this.txtESINo.Name = "txtESINo";
            this.txtESINo.Size = new System.Drawing.Size(208, 20);
            this.txtESINo.TabIndex = 87;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(419, 186);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 102;
            this.label21.Text = "ESI No";
            // 
            // txtEL
            // 
            this.txtEL.Location = new System.Drawing.Point(469, 151);
            this.txtEL.Name = "txtEL";
            this.txtEL.Size = new System.Drawing.Size(135, 20);
            this.txtEL.TabIndex = 86;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(445, 155);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(20, 13);
            this.label20.TabIndex = 101;
            this.label20.Text = "EL";
            // 
            // txtCL
            // 
            this.txtCL.Location = new System.Drawing.Point(469, 117);
            this.txtCL.Name = "txtCL";
            this.txtCL.Size = new System.Drawing.Size(135, 20);
            this.txtCL.TabIndex = 85;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(444, 121);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 13);
            this.label19.TabIndex = 100;
            this.label19.Text = "CL";
            // 
            // txtDispensary
            // 
            this.txtDispensary.Location = new System.Drawing.Point(734, 82);
            this.txtDispensary.Name = "txtDispensary";
            this.txtDispensary.Size = new System.Drawing.Size(177, 20);
            this.txtDispensary.TabIndex = 83;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(661, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 99;
            this.label18.Text = "Dispensary";
            // 
            // cmbShiftName
            // 
            this.cmbShiftName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftName.FormattingEnabled = true;
            this.cmbShiftName.Location = new System.Drawing.Point(469, 82);
            this.cmbShiftName.Name = "cmbShiftName";
            this.cmbShiftName.Size = new System.Drawing.Size(191, 21);
            this.cmbShiftName.TabIndex = 82;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(390, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 13);
            this.label17.TabIndex = 98;
            this.label17.Text = "Shift Name";
            // 
            // cmbWeekOff
            // 
            this.cmbWeekOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWeekOff.FormattingEnabled = true;
            this.cmbWeekOff.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednsday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbWeekOff.Location = new System.Drawing.Point(734, 47);
            this.cmbWeekOff.Name = "cmbWeekOff";
            this.cmbWeekOff.Size = new System.Drawing.Size(177, 21);
            this.cmbWeekOff.TabIndex = 80;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(666, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 97;
            this.label16.Text = "Week Off";
            // 
            // cmbEmpType
            // 
            this.cmbEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpType.FormattingEnabled = true;
            this.cmbEmpType.Location = new System.Drawing.Point(469, 46);
            this.cmbEmpType.Name = "cmbEmpType";
            this.cmbEmpType.Size = new System.Drawing.Size(191, 21);
            this.cmbEmpType.TabIndex = 79;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(364, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 96;
            this.label15.Text = "Employee Type";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Items.AddRange(new object[] {
            "Staff",
            "Worker",
            "Executive",
            "Casuals",
            "Probation"});
            this.cmbGroup.Location = new System.Drawing.Point(734, 11);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(177, 21);
            this.cmbGroup.TabIndex = 78;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(685, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 13);
            this.label14.TabIndex = 95;
            this.label14.Text = "Group";
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(469, 11);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(191, 21);
            this.cmbCategory.TabIndex = 77;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(403, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 94;
            this.label13.Text = "Category";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(127, 397);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(262, 20);
            this.txtPhone.TabIndex = 73;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(73, 401);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 91;
            this.label12.Text = "Phone";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(127, 362);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(262, 20);
            this.txtCity.TabIndex = 72;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(89, 366);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 88;
            this.label11.Text = "City";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(127, 429);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(262, 46);
            this.txtInfo.TabIndex = 75;
            this.txtInfo.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(88, 429);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Info";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(127, 287);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(262, 68);
            this.txtAddress.TabIndex = 71;
            this.txtAddress.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(63, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 81;
            this.label9.Text = "Address";
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Location = new System.Drawing.Point(268, 256);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(60, 17);
            this.RadioFemale.TabIndex = 69;
            this.RadioFemale.TabStop = true;
            this.RadioFemale.Text = "FeMale";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // radioMale
            // 
            this.radioMale.AutoSize = true;
            this.radioMale.Location = new System.Drawing.Point(268, 218);
            this.radioMale.Name = "radioMale";
            this.radioMale.Size = new System.Drawing.Size(48, 17);
            this.radioMale.TabIndex = 68;
            this.radioMale.TabStop = true;
            this.radioMale.Text = "Male";
            this.radioMale.UseVisualStyleBackColor = true;
            // 
            // dtpDOJ
            // 
            this.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOJ.Location = new System.Drawing.Point(131, 254);
            this.dtpDOJ.Name = "dtpDOJ";
            this.dtpDOJ.Size = new System.Drawing.Size(110, 20);
            this.dtpDOJ.TabIndex = 66;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 76;
            this.label8.Text = "Date of Joining";
            // 
            // dtpDOB
            // 
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(131, 216);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(111, 20);
            this.dtpDOB.TabIndex = 64;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 74;
            this.label7.Text = "Date of Birth";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(131, 180);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(262, 20);
            this.txtFatherName.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 70;
            this.label6.Text = "Father\'s Name";
            // 
            // cmbDesinganation
            // 
            this.cmbDesinganation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDesinganation.FormattingEnabled = true;
            this.cmbDesinganation.Location = new System.Drawing.Point(131, 148);
            this.cmbDesinganation.Name = "cmbDesinganation";
            this.cmbDesinganation.Size = new System.Drawing.Size(262, 21);
            this.cmbDesinganation.TabIndex = 62;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "Desingnation";
            // 
            // txtEmpCardNo
            // 
            this.txtEmpCardNo.Location = new System.Drawing.Point(131, 79);
            this.txtEmpCardNo.Name = "txtEmpCardNo";
            this.txtEmpCardNo.Size = new System.Drawing.Size(135, 20);
            this.txtEmpCardNo.TabIndex = 59;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "Punch No";
            // 
            // txtEmpcode
            // 
            this.txtEmpcode.Location = new System.Drawing.Point(131, 46);
            this.txtEmpcode.Name = "txtEmpcode";
            this.txtEmpcode.Size = new System.Drawing.Size(135, 20);
            this.txtEmpcode.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Employee Code";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(131, 12);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(262, 20);
            this.txtEmpName.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "Employee Name";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(131, 116);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(262, 21);
            this.cmbDepartment.TabIndex = 60;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "Department";
            // 
            // frpfnttt
            // 
            this.frpfnttt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.frpfnttt.Controls.Add(this.fntgrid);
            this.frpfnttt.Controls.Add(this.button3);
            this.frpfnttt.Controls.Add(this.button2);
            this.frpfnttt.Controls.Add(this.button1);
            this.frpfnttt.Controls.Add(this.label26);
            this.frpfnttt.Controls.Add(this.Frmdt);
            this.frpfnttt.Location = new System.Drawing.Point(0, 0);
            this.frpfnttt.Name = "frpfnttt";
            this.frpfnttt.Size = new System.Drawing.Size(931, 527);
            this.frpfnttt.TabIndex = 6;
            // 
            // fntgrid
            // 
            this.fntgrid.AllowUserToAddRows = false;
            this.fntgrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fntgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.fntgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fntgrid.EnableHeadersVisualStyles = false;
            this.fntgrid.Location = new System.Drawing.Point(208, 62);
            this.fntgrid.Name = "fntgrid";
            this.fntgrid.ReadOnly = true;
            this.fntgrid.RowHeadersVisible = false;
            this.fntgrid.Size = new System.Drawing.Size(511, 381);
            this.fntgrid.TabIndex = 224;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(583, 480);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 28);
            this.button3.TabIndex = 229;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(415, 480);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(151, 28);
            this.button2.TabIndex = 228;
            this.button2.Text = "Employee List";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(240, 480);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 28);
            this.button1.TabIndex = 227;
            this.button1.Text = "Delete Employee";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(156, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(329, 19);
            this.label26.TabIndex = 226;
            this.label26.Text = "Category Wise  Employee Strength for the month ";
            // 
            // Frmdt
            // 
            this.Frmdt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frmdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Frmdt.Location = new System.Drawing.Point(491, 19);
            this.Frmdt.Name = "Frmdt";
            this.Frmdt.Size = new System.Drawing.Size(125, 24);
            this.Frmdt.TabIndex = 225;
            this.Frmdt.Value = new System.DateTime(2018, 12, 15, 0, 0, 0, 0);
            // 
            // FrmEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 530);
            this.Controls.Add(this.frpfnttt);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.panFooter);
            this.Name = "FrmEmp";
            this.Text = "Employee Master";
            this.Load += new System.EventHandler(this.FrmEmp_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).EndInit();
            this.panFooter.ResumeLayout(false);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).EndInit();
            this.frpfnttt.ResumeLayout(false);
            this.frpfnttt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fntgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridEmployee;
        private System.Windows.Forms.Panel panFooter;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel grBack;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox picEmpPhoto;
        private System.Windows.Forms.TextBox txtAadharNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPanNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtUANNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPFNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtESINo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtEL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCL;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDispensary;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbShiftName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbWeekOff;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbEmpType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox txtInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox txtAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton radioMale;
        private System.Windows.Forms.DateTimePicker dtpDOJ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDesinganation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmpCardNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmpcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel frpfnttt;
        private System.Windows.Forms.DataGridView fntgrid;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.DateTimePicker Frmdt;
    }
}