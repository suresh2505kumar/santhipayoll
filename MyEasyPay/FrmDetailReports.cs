﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmDetailReports : Form
    {
        public FrmDetailReports()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection connection = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");

        private void FrmDetailReports_Load(object sender, EventArgs e)
        {
            Department();
        }

        public void Department()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDept_Mast", connection);
                DataGridDepartment.DataSource = null;
                DataGridDepartment.AutoGenerateColumns = false;
                DataGridDepartment.ColumnCount = 2;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 50
                };
                DataGridDepartment.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridDepartment.Columns[1].Name = "Department";
                DataGridDepartment.Columns[1].HeaderText = "Department";
                DataGridDepartment.Columns[1].DataPropertyName = "DeptName";
                DataGridDepartment.Columns[1].Width = 190;

                DataGridDepartment.Columns[2].Name = "Department";
                DataGridDepartment.Columns[2].HeaderText = "Department";
                DataGridDepartment.Columns[2].DataPropertyName = "DeptId";
                DataGridDepartment.Columns[2].Visible = false;
                DataGridDepartment.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                GeneralParameters.Department = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Department == string.Empty)
                        {
                            GeneralParameters.Department = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Department = GeneralParameters.Department + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }
                if (CmbReportType.Text == "Late Summary")
                {
                    LateSummary();
                }
                else if (CmbReportType.Text == "OT Summary")
                {
                    OTSummary();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void OTSummary()
        {
            try
            {
                DateTime Fromdate = Convert.ToDateTime(dtpFromDate.Text);
                DateTime Todate = Convert.ToDateTime(DtpToDate.Text);
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@TBL","ATT_REG" + Fromdate.Month.ToString("00") + ""),
                        new SqlParameter("@DTF", Fromdate.ToString("yyyy-MM-dd")),
                        new SqlParameter("@DTT", Todate.ToString("yyyy-MM-dd"))
                    };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_OTSUMMARY", sqlParameters, connection);
                DataGridLateSummary.DataSource = null;
                DataGridLateSummary.DataSource = data;
                GrLateSummary.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LateSummary()
        {
            try
            {
                DateTime date = Convert.ToDateTime(dtpFromDate.Text);
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@TBL","ATT_REG" + date.Month.ToString("00") + ""),
                        new SqlParameter("@DT", date.ToString("yyyy-MM-dd"))
                    };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GET_LATESUMMARY", sqlParameters, connection);
                DataGridLateSummary.DataSource = null;
                DataGridLateSummary.DataSource = data;
                GrLateSummary.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (ChckSelect.Checked == true)
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = false;
                }
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            GrLateSummary.Visible = false;
        }

        private void BtnDownloadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Muster Export";
                for (int i = 0; i < DataGridLateSummary.Columns.Count; i++)
                {
                    worksheet.Cells[1, i + 1] = DataGridLateSummary.Columns[i].HeaderText;
                }
                for (int i = 0; i < DataGridLateSummary.Rows.Count; i++)
                {
                    for (int j = 0; j < DataGridLateSummary.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = DataGridLateSummary.Rows[i].Cells[j].Value.ToString();
                    }
                }
                MessageBox.Show("Download Completed ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}