﻿namespace MyEasyPay
{
    partial class FrmEmployeeReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeReports));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrpboxFront = new System.Windows.Forms.GroupBox();
            this.DataGridDepartment = new System.Windows.Forms.DataGridView();
            this.BtnShow = new System.Windows.Forms.Button();
            this.ChckShift = new System.Windows.Forms.CheckBox();
            this.DataGridShift = new System.Windows.Forms.DataGridView();
            this.s = new System.Windows.Forms.CheckBox();
            this.chckDepartment = new System.Windows.Forms.CheckBox();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.DatagridCategory = new System.Windows.Forms.DataGridView();
            this.BtnAttendanceProcess = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Frmdt = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.CmbTag = new System.Windows.Forms.ComboBox();
            this.cboop = new System.Windows.Forms.ComboBox();
            this.DataGridLateSummary = new System.Windows.Forms.DataGridView();
            this.BtnDownloadExcel = new System.Windows.Forms.Button();
            this.BtnBack = new System.Windows.Forms.Button();
            this.GrLateSummary = new System.Windows.Forms.GroupBox();
            this.GrpboxFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLateSummary)).BeginInit();
            this.GrLateSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpboxFront
            // 
            this.GrpboxFront.Controls.Add(this.DataGridDepartment);
            this.GrpboxFront.Controls.Add(this.BtnShow);
            this.GrpboxFront.Controls.Add(this.ChckShift);
            this.GrpboxFront.Controls.Add(this.DataGridShift);
            this.GrpboxFront.Controls.Add(this.s);
            this.GrpboxFront.Controls.Add(this.chckDepartment);
            this.GrpboxFront.Controls.Add(this.buttnext1);
            this.GrpboxFront.Controls.Add(this.DatagridCategory);
            this.GrpboxFront.Controls.Add(this.BtnAttendanceProcess);
            this.GrpboxFront.Controls.Add(this.label5);
            this.GrpboxFront.Controls.Add(this.BtnPrint);
            this.GrpboxFront.Controls.Add(this.label2);
            this.GrpboxFront.Controls.Add(this.Frmdt);
            this.GrpboxFront.Controls.Add(this.Label1);
            this.GrpboxFront.Controls.Add(this.CmbTag);
            this.GrpboxFront.Controls.Add(this.cboop);
            this.GrpboxFront.Location = new System.Drawing.Point(7, 3);
            this.GrpboxFront.Name = "GrpboxFront";
            this.GrpboxFront.Size = new System.Drawing.Size(993, 484);
            this.GrpboxFront.TabIndex = 0;
            this.GrpboxFront.TabStop = false;
            // 
            // DataGridDepartment
            // 
            this.DataGridDepartment.AllowUserToAddRows = false;
            this.DataGridDepartment.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridDepartment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDepartment.EnableHeadersVisualStyles = false;
            this.DataGridDepartment.Location = new System.Drawing.Point(10, 59);
            this.DataGridDepartment.Name = "DataGridDepartment";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridDepartment.Size = new System.Drawing.Size(257, 406);
            this.DataGridDepartment.TabIndex = 0;
            this.DataGridDepartment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridDepartment_CellContentClick_1);
            // 
            // BtnShow
            // 
            this.BtnShow.Location = new System.Drawing.Point(106, 36);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(87, 23);
            this.BtnShow.TabIndex = 233;
            this.BtnShow.Text = "Show Category";
            this.BtnShow.UseVisualStyleBackColor = true;
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // ChckShift
            // 
            this.ChckShift.AutoSize = true;
            this.ChckShift.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckShift.Location = new System.Drawing.Point(538, 36);
            this.ChckShift.Name = "ChckShift";
            this.ChckShift.Size = new System.Drawing.Size(85, 22);
            this.ChckShift.TabIndex = 232;
            this.ChckShift.Text = "Select All";
            this.ChckShift.UseVisualStyleBackColor = true;
            this.ChckShift.CheckedChanged += new System.EventHandler(this.CheckBox5_CheckedChanged);
            // 
            // DataGridShift
            // 
            this.DataGridShift.AllowUserToAddRows = false;
            this.DataGridShift.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridShift.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridShift.EnableHeadersVisualStyles = false;
            this.DataGridShift.Location = new System.Drawing.Point(538, 59);
            this.DataGridShift.Name = "DataGridShift";
            this.DataGridShift.RowHeadersVisible = false;
            this.DataGridShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridShift.Size = new System.Drawing.Size(195, 406);
            this.DataGridShift.TabIndex = 231;
            this.DataGridShift.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridShift_CellContentClick);
            // 
            // s
            // 
            this.s.AutoSize = true;
            this.s.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.s.Location = new System.Drawing.Point(273, 36);
            this.s.Name = "s";
            this.s.Size = new System.Drawing.Size(85, 22);
            this.s.TabIndex = 224;
            this.s.Text = "Select All";
            this.s.UseVisualStyleBackColor = true;
            this.s.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // chckDepartment
            // 
            this.chckDepartment.AutoSize = true;
            this.chckDepartment.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckDepartment.Location = new System.Drawing.Point(10, 36);
            this.chckDepartment.Name = "chckDepartment";
            this.chckDepartment.Size = new System.Drawing.Size(85, 22);
            this.chckDepartment.TabIndex = 223;
            this.chckDepartment.Text = "Select All";
            this.chckDepartment.UseVisualStyleBackColor = true;
            this.chckDepartment.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(805, 252);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 222;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.Buttnext1_Click);
            // 
            // DatagridCategory
            // 
            this.DatagridCategory.AllowUserToAddRows = false;
            this.DatagridCategory.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DatagridCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DatagridCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatagridCategory.EnableHeadersVisualStyles = false;
            this.DatagridCategory.Location = new System.Drawing.Point(273, 59);
            this.DatagridCategory.Name = "DatagridCategory";
            this.DatagridCategory.Size = new System.Drawing.Size(260, 406);
            this.DatagridCategory.TabIndex = 1;
            this.DatagridCategory.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatagridCategory_CellClick);
            this.DatagridCategory.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatagridCategory_CellContentClick);
            // 
            // BtnAttendanceProcess
            // 
            this.BtnAttendanceProcess.BackColor = System.Drawing.Color.White;
            this.BtnAttendanceProcess.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnAttendanceProcess.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAttendanceProcess.Location = new System.Drawing.Point(736, 146);
            this.BtnAttendanceProcess.Name = "BtnAttendanceProcess";
            this.BtnAttendanceProcess.Size = new System.Drawing.Size(191, 31);
            this.BtnAttendanceProcess.TabIndex = 230;
            this.BtnAttendanceProcess.Text = "ATTENDANCE PROCESS";
            this.BtnAttendanceProcess.UseVisualStyleBackColor = false;
            this.BtnAttendanceProcess.Click += new System.EventHandler(this.BtnAttendanceProcess_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(736, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 221;
            this.label5.Text = "Type";
            // 
            // BtnPrint
            // 
            this.BtnPrint.BackColor = System.Drawing.Color.White;
            this.BtnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPrint.Location = new System.Drawing.Point(736, 252);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(65, 30);
            this.BtnPrint.TabIndex = 217;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnPrint.UseVisualStyleBackColor = false;
            this.BtnPrint.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(736, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 18);
            this.label2.TabIndex = 216;
            this.label2.Text = "Type";
            // 
            // Frmdt
            // 
            this.Frmdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frmdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Frmdt.Location = new System.Drawing.Point(736, 107);
            this.Frmdt.Name = "Frmdt";
            this.Frmdt.Size = new System.Drawing.Size(115, 26);
            this.Frmdt.TabIndex = 207;
            this.Frmdt.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(736, 86);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(37, 18);
            this.Label1.TabIndex = 204;
            this.Label1.Text = "Date";
            // 
            // CmbTag
            // 
            this.CmbTag.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbTag.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbTag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTag.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbTag.FormattingEnabled = true;
            this.CmbTag.Items.AddRange(new object[] {
            "Strength Report",
            "In Report",
            "Daily Punch Report",
            "Irregular Punch Report",
            "Worked Hour Report (< 8 Hrs)",
            "Late In Report",
            "Permission Report",
            "Lunch Break Report",
            "Early Out Report",
            "Absent List",
            "Over Time Report",
            "Muster Roll Report",
            "MusterRoll (1 to 15 days)",
            "MusterRoll (16 to 31 days)",
            "Over Time Muster Roll Report",
            "Late Summary"});
            this.CmbTag.Location = new System.Drawing.Point(736, 210);
            this.CmbTag.Name = "CmbTag";
            this.CmbTag.Size = new System.Drawing.Size(252, 26);
            this.CmbTag.TabIndex = 220;
            // 
            // cboop
            // 
            this.cboop.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboop.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboop.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboop.FormattingEnabled = true;
            this.cboop.Items.AddRange(new object[] {
            "Employee List",
            "New Join Employee List",
            "Deleted Employee List",
            "Employee PF/ESI Details",
            "Employee Bank Account Details",
            "Employee Contact Details",
            "Employee Data Sheet(CSV format)"});
            this.cboop.Location = new System.Drawing.Point(736, 210);
            this.cboop.Name = "cboop";
            this.cboop.Size = new System.Drawing.Size(252, 26);
            this.cboop.TabIndex = 215;
            // 
            // DataGridLateSummary
            // 
            this.DataGridLateSummary.AllowUserToAddRows = false;
            this.DataGridLateSummary.BackgroundColor = System.Drawing.Color.White;
            this.DataGridLateSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridLateSummary.Location = new System.Drawing.Point(6, 19);
            this.DataGridLateSummary.Name = "DataGridLateSummary";
            this.DataGridLateSummary.RowHeadersVisible = false;
            this.DataGridLateSummary.Size = new System.Drawing.Size(981, 427);
            this.DataGridLateSummary.TabIndex = 0;
            // 
            // BtnDownloadExcel
            // 
            this.BtnDownloadExcel.Location = new System.Drawing.Point(859, 448);
            this.BtnDownloadExcel.Name = "BtnDownloadExcel";
            this.BtnDownloadExcel.Size = new System.Drawing.Size(128, 33);
            this.BtnDownloadExcel.TabIndex = 1;
            this.BtnDownloadExcel.Text = "Download Excel";
            this.BtnDownloadExcel.UseVisualStyleBackColor = true;
            // 
            // BtnBack
            // 
            this.BtnBack.Location = new System.Drawing.Point(6, 449);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(82, 33);
            this.BtnBack.TabIndex = 2;
            this.BtnBack.Text = "Back";
            this.BtnBack.UseVisualStyleBackColor = true;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // GrLateSummary
            // 
            this.GrLateSummary.Controls.Add(this.BtnBack);
            this.GrLateSummary.Controls.Add(this.BtnDownloadExcel);
            this.GrLateSummary.Controls.Add(this.DataGridLateSummary);
            this.GrLateSummary.Location = new System.Drawing.Point(7, 3);
            this.GrLateSummary.Name = "GrLateSummary";
            this.GrLateSummary.Size = new System.Drawing.Size(993, 484);
            this.GrLateSummary.TabIndex = 234;
            this.GrLateSummary.TabStop = false;
            this.GrLateSummary.Visible = false;
            // 
            // FrmEmployeeReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1008, 493);
            this.Controls.Add(this.GrpboxFront);
            this.Controls.Add(this.GrLateSummary);
            this.MaximizeBox = false;
            this.Name = "FrmEmployeeReports";
            this.Text = "Reports";
            this.Load += new System.EventHandler(this.FrmEmployeeReports_Load);
            this.GrpboxFront.ResumeLayout(false);
            this.GrpboxFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLateSummary)).EndInit();
            this.GrLateSummary.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpboxFront;
        private System.Windows.Forms.DataGridView DataGridDepartment;
        private System.Windows.Forms.DataGridView DatagridCategory;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DateTimePicker Frmdt;
        private System.Windows.Forms.ComboBox cboop;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.ComboBox CmbTag;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox s;
        private System.Windows.Forms.CheckBox chckDepartment;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnAttendanceProcess;
        private System.Windows.Forms.CheckBox ChckShift;
        private System.Windows.Forms.DataGridView DataGridShift;
        private System.Windows.Forms.Button BtnShow;
        private System.Windows.Forms.DataGridView DataGridLateSummary;
        private System.Windows.Forms.Button BtnDownloadExcel;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.GroupBox GrLateSummary;
    }
}