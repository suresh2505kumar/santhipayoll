﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmReportviewer : Form
    {
        public FrmReportviewer()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");

        private void FrmReportviewer_Load(object sender, EventArgs e)
        {
            if (GeneralParameters.ReportId == 1)
            {
                PerformanceReport();
            }
            else if (GeneralParameters.ReportId == 2)
            {
                IregularReport();
            }
            else if (GeneralParameters.ReportId == 3)
            {
                StrengthReport();
            }
            else if (GeneralParameters.ReportId == 4)
            {
                InReport();
            }
            else if (GeneralParameters.ReportId == 5)
            {
                WorkesHoursReport();
            }
            else if (GeneralParameters.ReportId == 6)
            {
                LunchBreakReport();
            }
            else if (GeneralParameters.ReportId == 7)
            {
                EmployeeList();
            }
            else if (GeneralParameters.ReportId == 8)
            {
                MusterRoll();
            }
            else if (GeneralParameters.ReportId == 9)
            {
                LateInReport();
            }
            else if (GeneralParameters.ReportId == 10)
            {
                PermissionReport();
            }
        }

        private void PermissionReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Perdt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetPermissonByDate", sqlParameters, conn);
                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPermission.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject txtReportTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtReportTitle"];
                    txtReportTitle.Text = "Permission Report";
                    //TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    //textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void LateInReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                    new SqlParameter("@grname",GeneralParameters.Desingnation)
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GetLateEntries", sqlParameters, conn);
                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryLate.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    //TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    //textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void MusterRoll()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                   
                   new SqlParameter("@SHIFTid",GeneralParameters.Shift),
                     new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@grname",GeneralParameters.Desingnation)
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryMu.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,ex.StackTrace);
                return;
            }
        }

        private void EmployeeList()
        {
            try
            {
                if(GeneralParameters.Shift == "1000")
                {
                    GeneralParameters.Shift = "1000";
                }
                else
                {
                    GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                }
                SqlParameter[] sqlParameters = {
                     new SqlParameter("@DEPTID",GeneralParameters.Department) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_Emp_list", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryEmp.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textdate.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void PerformanceReport()
        {
            try
            {
                string ReportTitle = string.Empty;
                if (GeneralParameters.LVTAG == "P")
                {
                    ReportTitle = "Performance Report";
                }
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyyMMdd")),
                    new SqlParameter("@TBLNAME",GeneralParameters.TablName),
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@LVTAG",GeneralParameters.LVTAG),
                    new SqlParameter("@SHIFT",GeneralParameters.Shift),
                    new SqlParameter("@grname",GeneralParameters.Desingnation)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_InOutRpt", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPerformance.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    TextObject txtReportTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtReportTitle"];
                    txtReportTitle.Text = ReportTitle;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void IregularReport()
        {
            try
            {
                string ReportTitle = string.Empty;
                if(GeneralParameters.LVTAG == "Ab")
                {
                    ReportTitle = "Absent Report";
                }
                else
                {
                    ReportTitle = "Iregular Report";
                }
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyyMMdd")),
                    new SqlParameter("@TBLNAME",GeneralParameters.TablName),
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@LVTAG",GeneralParameters.LVTAG),
                    new SqlParameter("@SHIFT",GeneralParameters.Shift),
                    new SqlParameter("@grname",GeneralParameters.Desingnation)


                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_InOutRpt", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPerformance.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    TextObject txtReportTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtReportTitle"];
                    txtReportTitle.Text = ReportTitle;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void StrengthReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
              
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                          new SqlParameter("@TBL",GeneralParameters.TablName),
                     new SqlParameter("@DEPTID",GeneralParameters.Department),
                     new SqlParameter("@grname",GeneralParameters.Desingnation)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "ATTABSTRACT_SP", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryStrengthRpt.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    //TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    //textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void InReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                      new SqlParameter("@DEPTID",GeneralParameters.Department),
                        new SqlParameter("@grname",GeneralParameters.Desingnation)

                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_INRPT", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryInReport.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void WorkesHoursReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                         new SqlParameter("@Shiftid",GeneralParameters.Shift),
                      new SqlParameter("@DEPTID",GeneralParameters.Department),
                        new SqlParameter("@grname",GeneralParameters.Desingnation)
                };

                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_WorkedHoursLessthan8", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryWorkedHours.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void LunchBreakReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                     new SqlParameter("@DEPTID",GeneralParameters.Department),
                        new SqlParameter("@grname",GeneralParameters.Desingnation)
                };

                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_LunchBreak", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryLunchBreak.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.CompanyName;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    txtCompany.Text = GeneralParameters.CompanyDt.Rows[0]["CompanyName"].ToString();
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CryReportviewer_Load(object sender, EventArgs e)
        {

        }
    }
}
