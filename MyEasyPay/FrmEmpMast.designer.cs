﻿namespace MyEasyPay
{
    partial class FrmEmpMast
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.cmbapp = new System.Windows.Forms.ComboBox();
            this.IsAppriser = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Appriser = new System.Windows.Forms.Label();
            this.txtmailid = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.picEmpPhoto = new System.Windows.Forms.PictureBox();
            this.txtAadharNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPanNo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtUANNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtPFNo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtESINo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEL = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCL = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDispensary = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbShiftName = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbWeekOff = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbEmpType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInfo = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.radioMale = new System.Windows.Forms.RadioButton();
            this.dtpDOJ = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDesinganation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmpCardNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmpcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panFooter = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.lblName = new System.Windows.Forms.Label();
            this.cmgfntbox = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridEmployee = new System.Windows.Forms.DataGridView();
            this.frpfnttt = new System.Windows.Forms.Panel();
            this.BtnQuickEmp = new System.Windows.Forms.Button();
            this.txtclose = new System.Windows.Forms.TextBox();
            this.txtleft = new System.Windows.Forms.TextBox();
            this.txtjoin = new System.Windows.Forms.TextBox();
            this.txtop = new System.Windows.Forms.TextBox();
            this.fntgrid = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.Frmdt = new System.Windows.Forms.DateTimePicker();
            this.grQuickEmp = new System.Windows.Forms.GroupBox();
            this.DataGridQEmployee = new System.Windows.Forms.DataGridView();
            this.txtQPhone = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.PicQEmpPhoto = new System.Windows.Forms.PictureBox();
            this.BtnQickBack = new System.Windows.Forms.Button();
            this.BtnQuickSave = new System.Windows.Forms.Button();
            this.CmbQShiftName = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.CmbQWeekOff = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtQAddress = new System.Windows.Forms.RichTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.DtpQDOJ = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.DtpQDOB = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.txtQFatherName = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.CmbQDesingnation = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtQPunchCode = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtQEmpCode = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtQEmpName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.CmbQDepartment = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).BeginInit();
            this.panFooter.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).BeginInit();
            this.frpfnttt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fntgrid)).BeginInit();
            this.grQuickEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridQEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicQEmpPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.cmbapp);
            this.grBack.Controls.Add(this.IsAppriser);
            this.grBack.Controls.Add(this.label29);
            this.grBack.Controls.Add(this.Appriser);
            this.grBack.Controls.Add(this.txtmailid);
            this.grBack.Controls.Add(this.label27);
            this.grBack.Controls.Add(this.btnClear);
            this.grBack.Controls.Add(this.btnBrowse);
            this.grBack.Controls.Add(this.picEmpPhoto);
            this.grBack.Controls.Add(this.txtAadharNo);
            this.grBack.Controls.Add(this.label25);
            this.grBack.Controls.Add(this.txtPanNo);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.txtUANNo);
            this.grBack.Controls.Add(this.label23);
            this.grBack.Controls.Add(this.txtPFNo);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.txtESINo);
            this.grBack.Controls.Add(this.label21);
            this.grBack.Controls.Add(this.txtEL);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.txtCL);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.txtDispensary);
            this.grBack.Controls.Add(this.label18);
            this.grBack.Controls.Add(this.cmbShiftName);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.cmbWeekOff);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.cmbEmpType);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.cmbGroup);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.cmbCategory);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.txtPhone);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.txtCity);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.txtInfo);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtAddress);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.RadioFemale);
            this.grBack.Controls.Add(this.radioMale);
            this.grBack.Controls.Add(this.dtpDOJ);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.dtpDOB);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.txtFatherName);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.cmbDesinganation);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtEmpCardNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtEmpcode);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtEmpName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.cmbDepartment);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(0, 5);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(926, 488);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            this.grBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // cmbapp
            // 
            this.cmbapp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbapp.FormattingEnabled = true;
            this.cmbapp.Location = new System.Drawing.Point(493, 433);
            this.cmbapp.Name = "cmbapp";
            this.cmbapp.Size = new System.Drawing.Size(214, 26);
            this.cmbapp.TabIndex = 60;
            // 
            // IsAppriser
            // 
            this.IsAppriser.AutoSize = true;
            this.IsAppriser.Location = new System.Drawing.Point(493, 403);
            this.IsAppriser.Name = "IsAppriser";
            this.IsAppriser.Size = new System.Drawing.Size(90, 22);
            this.IsAppriser.TabIndex = 59;
            this.IsAppriser.Text = "IsAppriser";
            this.IsAppriser.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(389, 439);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(98, 18);
            this.label29.TabIndex = 58;
            this.label29.Text = "AppriserName";
            // 
            // Appriser
            // 
            this.Appriser.AutoSize = true;
            this.Appriser.Location = new System.Drawing.Point(413, 407);
            this.Appriser.Name = "Appriser";
            this.Appriser.Size = new System.Drawing.Size(61, 18);
            this.Appriser.TabIndex = 57;
            this.Appriser.Text = "Appriser";
            // 
            // txtmailid
            // 
            this.txtmailid.Location = new System.Drawing.Point(463, 369);
            this.txtmailid.Name = "txtmailid";
            this.txtmailid.Size = new System.Drawing.Size(244, 26);
            this.txtmailid.TabIndex = 55;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(403, 372);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 18);
            this.label27.TabIndex = 56;
            this.label27.Text = "EmailId";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(830, 334);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 32);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(728, 334);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 32);
            this.btnBrowse.TabIndex = 53;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
            // 
            // picEmpPhoto
            // 
            this.picEmpPhoto.BackColor = System.Drawing.Color.White;
            this.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picEmpPhoto.Image = global::MyEasyPay.Properties.Resources.MEMP;
            this.picEmpPhoto.Location = new System.Drawing.Point(725, 136);
            this.picEmpPhoto.Name = "picEmpPhoto";
            this.picEmpPhoto.Size = new System.Drawing.Size(159, 184);
            this.picEmpPhoto.TabIndex = 52;
            this.picEmpPhoto.TabStop = false;
            this.picEmpPhoto.Click += new System.EventHandler(this.picEmpPhoto_Click);
            // 
            // txtAadharNo
            // 
            this.txtAadharNo.Location = new System.Drawing.Point(463, 331);
            this.txtAadharNo.Name = "txtAadharNo";
            this.txtAadharNo.Size = new System.Drawing.Size(208, 26);
            this.txtAadharNo.TabIndex = 26;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(387, 334);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 18);
            this.label25.TabIndex = 50;
            this.label25.Text = "Aadhar No";
            // 
            // txtPanNo
            // 
            this.txtPanNo.Location = new System.Drawing.Point(463, 299);
            this.txtPanNo.Name = "txtPanNo";
            this.txtPanNo.Size = new System.Drawing.Size(208, 26);
            this.txtPanNo.TabIndex = 25;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(405, 302);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 18);
            this.label24.TabIndex = 48;
            this.label24.Text = "PAN No";
            // 
            // txtUANNo
            // 
            this.txtUANNo.Location = new System.Drawing.Point(463, 261);
            this.txtUANNo.Name = "txtUANNo";
            this.txtUANNo.Size = new System.Drawing.Size(208, 26);
            this.txtUANNo.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(403, 264);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 18);
            this.label23.TabIndex = 46;
            this.label23.Text = "UAN No";
            // 
            // txtPFNo
            // 
            this.txtPFNo.Location = new System.Drawing.Point(463, 224);
            this.txtPFNo.Name = "txtPFNo";
            this.txtPFNo.Size = new System.Drawing.Size(208, 26);
            this.txtPFNo.TabIndex = 23;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(409, 227);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 18);
            this.label22.TabIndex = 44;
            this.label22.Text = "EPF No";
            // 
            // txtESINo
            // 
            this.txtESINo.Location = new System.Drawing.Point(463, 190);
            this.txtESINo.Name = "txtESINo";
            this.txtESINo.Size = new System.Drawing.Size(208, 26);
            this.txtESINo.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(413, 193);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 18);
            this.label21.TabIndex = 42;
            this.label21.Text = "ESI No";
            // 
            // txtEL
            // 
            this.txtEL.Location = new System.Drawing.Point(463, 158);
            this.txtEL.Name = "txtEL";
            this.txtEL.Size = new System.Drawing.Size(135, 26);
            this.txtEL.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(439, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 18);
            this.label20.TabIndex = 40;
            this.label20.Text = "EL";
            // 
            // txtCL
            // 
            this.txtCL.Location = new System.Drawing.Point(463, 124);
            this.txtCL.Name = "txtCL";
            this.txtCL.Size = new System.Drawing.Size(135, 26);
            this.txtCL.TabIndex = 20;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(438, 128);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 18);
            this.label19.TabIndex = 38;
            this.label19.Text = "CL";
            // 
            // txtDispensary
            // 
            this.txtDispensary.Location = new System.Drawing.Point(728, 89);
            this.txtDispensary.Name = "txtDispensary";
            this.txtDispensary.Size = new System.Drawing.Size(177, 26);
            this.txtDispensary.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(655, 93);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 18);
            this.label18.TabIndex = 36;
            this.label18.Text = "Dispensary";
            // 
            // cmbShiftName
            // 
            this.cmbShiftName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftName.FormattingEnabled = true;
            this.cmbShiftName.Location = new System.Drawing.Point(463, 89);
            this.cmbShiftName.Name = "cmbShiftName";
            this.cmbShiftName.Size = new System.Drawing.Size(191, 26);
            this.cmbShiftName.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(384, 93);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 18);
            this.label17.TabIndex = 34;
            this.label17.Text = "Shift Name";
            // 
            // cmbWeekOff
            // 
            this.cmbWeekOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWeekOff.FormattingEnabled = true;
            this.cmbWeekOff.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednsday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbWeekOff.Location = new System.Drawing.Point(728, 54);
            this.cmbWeekOff.Name = "cmbWeekOff";
            this.cmbWeekOff.Size = new System.Drawing.Size(177, 26);
            this.cmbWeekOff.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(660, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 18);
            this.label16.TabIndex = 32;
            this.label16.Text = "Week Off";
            // 
            // cmbEmpType
            // 
            this.cmbEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpType.FormattingEnabled = true;
            this.cmbEmpType.Location = new System.Drawing.Point(463, 53);
            this.cmbEmpType.Name = "cmbEmpType";
            this.cmbEmpType.Size = new System.Drawing.Size(191, 26);
            this.cmbEmpType.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(358, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 18);
            this.label15.TabIndex = 30;
            this.label15.Text = "Employee Type";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Items.AddRange(new object[] {
            "Staff",
            "Worker",
            "Executive",
            "Casuals",
            "Probation"});
            this.cmbGroup.Location = new System.Drawing.Point(728, 18);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(177, 26);
            this.cmbGroup.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(679, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 18);
            this.label14.TabIndex = 28;
            this.label14.Text = "Group";
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(463, 18);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(191, 26);
            this.cmbCategory.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(397, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 18);
            this.label13.TabIndex = 26;
            this.label13.Text = "Category";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(121, 404);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(262, 26);
            this.txtPhone.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 408);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Phone";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(121, 369);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(262, 26);
            this.txtCity.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(83, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "City";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(121, 436);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(262, 46);
            this.txtInfo.TabIndex = 13;
            this.txtInfo.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(82, 436);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "Info";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(121, 294);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(262, 68);
            this.txtAddress.TabIndex = 10;
            this.txtAddress.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(57, 294);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "Address";
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Location = new System.Drawing.Point(262, 263);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(72, 22);
            this.RadioFemale.TabIndex = 9;
            this.RadioFemale.TabStop = true;
            this.RadioFemale.Text = "FeMale";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // radioMale
            // 
            this.radioMale.AutoSize = true;
            this.radioMale.Location = new System.Drawing.Point(262, 225);
            this.radioMale.Name = "radioMale";
            this.radioMale.Size = new System.Drawing.Size(57, 22);
            this.radioMale.TabIndex = 8;
            this.radioMale.TabStop = true;
            this.radioMale.Text = "Male";
            this.radioMale.UseVisualStyleBackColor = true;
            // 
            // dtpDOJ
            // 
            this.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOJ.Location = new System.Drawing.Point(125, 261);
            this.dtpDOJ.Name = "dtpDOJ";
            this.dtpDOJ.Size = new System.Drawing.Size(110, 26);
            this.dtpDOJ.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "Date of Joining";
            // 
            // dtpDOB
            // 
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(125, 223);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(111, 26);
            this.dtpDOB.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Date of Birth";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(125, 187);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(262, 26);
            this.txtFatherName.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Father\'s Name";
            // 
            // cmbDesinganation
            // 
            this.cmbDesinganation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDesinganation.FormattingEnabled = true;
            this.cmbDesinganation.Location = new System.Drawing.Point(125, 155);
            this.cmbDesinganation.Name = "cmbDesinganation";
            this.cmbDesinganation.Size = new System.Drawing.Size(262, 26);
            this.cmbDesinganation.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Desingnation";
            // 
            // txtEmpCardNo
            // 
            this.txtEmpCardNo.Location = new System.Drawing.Point(125, 86);
            this.txtEmpCardNo.Name = "txtEmpCardNo";
            this.txtEmpCardNo.Size = new System.Drawing.Size(135, 26);
            this.txtEmpCardNo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Punch No";
            // 
            // txtEmpcode
            // 
            this.txtEmpcode.Location = new System.Drawing.Point(125, 53);
            this.txtEmpcode.Name = "txtEmpcode";
            this.txtEmpcode.Size = new System.Drawing.Size(135, 26);
            this.txtEmpcode.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Employee Code";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(125, 19);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(262, 26);
            this.txtEmpName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Employee Name";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(125, 123);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(262, 26);
            this.cmbDepartment.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Department";
            // 
            // panFooter
            // 
            this.panFooter.BackColor = System.Drawing.Color.White;
            this.panFooter.Controls.Add(this.btnEdit);
            this.panFooter.Controls.Add(this.btnAdd);
            this.panFooter.Controls.Add(this.btnDelete);
            this.panFooter.Controls.Add(this.btnSave);
            this.panFooter.Controls.Add(this.btnExit);
            this.panFooter.Controls.Add(this.btnBack);
            this.panFooter.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panFooter.Location = new System.Drawing.Point(5, 493);
            this.panFooter.Name = "panFooter";
            this.panFooter.Size = new System.Drawing.Size(918, 39);
            this.panFooter.TabIndex = 3;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = global::MyEasyPay.Properties.Resources.edit;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(675, 6);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(76, 28);
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = global::MyEasyPay.Properties.Resources.Add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(597, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(76, 28);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = global::MyEasyPay.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(753, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = global::MyEasyPay.Properties.Resources.eee;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(831, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = global::MyEasyPay.Properties.Resources.cancel;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(831, 6);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(76, 28);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::MyEasyPay.Properties.Resources.exit3;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(753, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(76, 28);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.lblName);
            this.grFront.Controls.Add(this.cmgfntbox);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridEmployee);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, 5);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(919, 491);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(228, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(83, 18);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "Department";
            // 
            // cmgfntbox
            // 
            this.cmgfntbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmgfntbox.FormattingEnabled = true;
            this.cmgfntbox.Location = new System.Drawing.Point(331, 14);
            this.cmgfntbox.Name = "cmgfntbox";
            this.cmgfntbox.Size = new System.Drawing.Size(278, 26);
            this.cmgfntbox.TabIndex = 17;
            this.cmgfntbox.SelectedIndexChanged += new System.EventHandler(this.cmgfntbox_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(5, 58);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(896, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            // 
            // DataGridEmployee
            // 
            this.DataGridEmployee.AllowUserToAddRows = false;
            this.DataGridEmployee.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridEmployee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmployee.EnableHeadersVisualStyles = false;
            this.DataGridEmployee.Location = new System.Drawing.Point(6, 87);
            this.DataGridEmployee.Name = "DataGridEmployee";
            this.DataGridEmployee.ReadOnly = true;
            this.DataGridEmployee.RowHeadersVisible = false;
            this.DataGridEmployee.Size = new System.Drawing.Size(897, 396);
            this.DataGridEmployee.TabIndex = 0;
            // 
            // frpfnttt
            // 
            this.frpfnttt.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.frpfnttt.Controls.Add(this.BtnQuickEmp);
            this.frpfnttt.Controls.Add(this.txtclose);
            this.frpfnttt.Controls.Add(this.txtleft);
            this.frpfnttt.Controls.Add(this.txtjoin);
            this.frpfnttt.Controls.Add(this.txtop);
            this.frpfnttt.Controls.Add(this.fntgrid);
            this.frpfnttt.Controls.Add(this.button3);
            this.frpfnttt.Controls.Add(this.button2);
            this.frpfnttt.Controls.Add(this.button1);
            this.frpfnttt.Controls.Add(this.label26);
            this.frpfnttt.Controls.Add(this.Frmdt);
            this.frpfnttt.Location = new System.Drawing.Point(0, 5);
            this.frpfnttt.Name = "frpfnttt";
            this.frpfnttt.Size = new System.Drawing.Size(926, 527);
            this.frpfnttt.TabIndex = 8;
            this.frpfnttt.Paint += new System.Windows.Forms.PaintEventHandler(this.frpfnttt_Paint);
            // 
            // BtnQuickEmp
            // 
            this.BtnQuickEmp.BackColor = System.Drawing.Color.White;
            this.BtnQuickEmp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnQuickEmp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuickEmp.Image = global::MyEasyPay.Properties.Resources.Add;
            this.BtnQuickEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnQuickEmp.Location = new System.Drawing.Point(510, 460);
            this.BtnQuickEmp.Name = "BtnQuickEmp";
            this.BtnQuickEmp.Size = new System.Drawing.Size(131, 39);
            this.BtnQuickEmp.TabIndex = 258;
            this.BtnQuickEmp.Text = "Add Employee";
            this.BtnQuickEmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnQuickEmp.UseVisualStyleBackColor = false;
            this.BtnQuickEmp.Click += new System.EventHandler(this.BtnQuickEmp_Click);
            // 
            // txtclose
            // 
            this.txtclose.Enabled = false;
            this.txtclose.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclose.ForeColor = System.Drawing.Color.Black;
            this.txtclose.Location = new System.Drawing.Point(663, 404);
            this.txtclose.Margin = new System.Windows.Forms.Padding(4);
            this.txtclose.Name = "txtclose";
            this.txtclose.Size = new System.Drawing.Size(106, 26);
            this.txtclose.TabIndex = 257;
            this.txtclose.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtleft
            // 
            this.txtleft.Enabled = false;
            this.txtleft.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtleft.ForeColor = System.Drawing.Color.Black;
            this.txtleft.Location = new System.Drawing.Point(556, 404);
            this.txtleft.Margin = new System.Windows.Forms.Padding(4);
            this.txtleft.Name = "txtleft";
            this.txtleft.Size = new System.Drawing.Size(106, 26);
            this.txtleft.TabIndex = 256;
            this.txtleft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtjoin
            // 
            this.txtjoin.Enabled = false;
            this.txtjoin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjoin.ForeColor = System.Drawing.Color.Black;
            this.txtjoin.Location = new System.Drawing.Point(447, 404);
            this.txtjoin.Margin = new System.Windows.Forms.Padding(4);
            this.txtjoin.Name = "txtjoin";
            this.txtjoin.Size = new System.Drawing.Size(106, 26);
            this.txtjoin.TabIndex = 255;
            this.txtjoin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtop
            // 
            this.txtop.Enabled = false;
            this.txtop.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtop.ForeColor = System.Drawing.Color.Black;
            this.txtop.Location = new System.Drawing.Point(338, 404);
            this.txtop.Margin = new System.Windows.Forms.Padding(4);
            this.txtop.Name = "txtop";
            this.txtop.Size = new System.Drawing.Size(106, 26);
            this.txtop.TabIndex = 254;
            this.txtop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fntgrid
            // 
            this.fntgrid.AllowUserToAddRows = false;
            this.fntgrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fntgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.fntgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.fntgrid.DefaultCellStyle = dataGridViewCellStyle9;
            this.fntgrid.EnableHeadersVisualStyles = false;
            this.fntgrid.Location = new System.Drawing.Point(143, 62);
            this.fntgrid.Name = "fntgrid";
            this.fntgrid.ReadOnly = true;
            this.fntgrid.RowHeadersVisible = false;
            this.fntgrid.Size = new System.Drawing.Size(650, 339);
            this.fntgrid.TabIndex = 224;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::MyEasyPay.Properties.Resources.eee;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(644, 460);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(76, 39);
            this.button3.TabIndex = 229;
            this.button3.Text = "Exit";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::MyEasyPay.Properties.Resources.acttt;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(382, 459);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 40);
            this.button2.TabIndex = 228;
            this.button2.Text = "Employee List";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::MyEasyPay.Properties.Resources.exit3;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(238, 460);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 39);
            this.button1.TabIndex = 227;
            this.button1.Text = "Delete Employee";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(183, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(329, 19);
            this.label26.TabIndex = 226;
            this.label26.Text = "Category Wise  Employee Strength for the month ";
            // 
            // Frmdt
            // 
            this.Frmdt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frmdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Frmdt.Location = new System.Drawing.Point(518, 18);
            this.Frmdt.Name = "Frmdt";
            this.Frmdt.Size = new System.Drawing.Size(125, 24);
            this.Frmdt.TabIndex = 225;
            this.Frmdt.Value = new System.DateTime(2018, 12, 15, 0, 0, 0, 0);
            // 
            // grQuickEmp
            // 
            this.grQuickEmp.Controls.Add(this.DataGridQEmployee);
            this.grQuickEmp.Controls.Add(this.txtQPhone);
            this.grQuickEmp.Controls.Add(this.label40);
            this.grQuickEmp.Controls.Add(this.PicQEmpPhoto);
            this.grQuickEmp.Controls.Add(this.BtnQickBack);
            this.grQuickEmp.Controls.Add(this.BtnQuickSave);
            this.grQuickEmp.Controls.Add(this.CmbQShiftName);
            this.grQuickEmp.Controls.Add(this.label38);
            this.grQuickEmp.Controls.Add(this.CmbQWeekOff);
            this.grQuickEmp.Controls.Add(this.label39);
            this.grQuickEmp.Controls.Add(this.txtQAddress);
            this.grQuickEmp.Controls.Add(this.label28);
            this.grQuickEmp.Controls.Add(this.radioButton1);
            this.grQuickEmp.Controls.Add(this.radioButton2);
            this.grQuickEmp.Controls.Add(this.DtpQDOJ);
            this.grQuickEmp.Controls.Add(this.label30);
            this.grQuickEmp.Controls.Add(this.DtpQDOB);
            this.grQuickEmp.Controls.Add(this.label31);
            this.grQuickEmp.Controls.Add(this.txtQFatherName);
            this.grQuickEmp.Controls.Add(this.label32);
            this.grQuickEmp.Controls.Add(this.CmbQDesingnation);
            this.grQuickEmp.Controls.Add(this.label33);
            this.grQuickEmp.Controls.Add(this.txtQPunchCode);
            this.grQuickEmp.Controls.Add(this.label34);
            this.grQuickEmp.Controls.Add(this.txtQEmpCode);
            this.grQuickEmp.Controls.Add(this.label35);
            this.grQuickEmp.Controls.Add(this.txtQEmpName);
            this.grQuickEmp.Controls.Add(this.label36);
            this.grQuickEmp.Controls.Add(this.CmbQDepartment);
            this.grQuickEmp.Controls.Add(this.label37);
            this.grQuickEmp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grQuickEmp.Location = new System.Drawing.Point(0, -3);
            this.grQuickEmp.Name = "grQuickEmp";
            this.grQuickEmp.Size = new System.Drawing.Size(926, 535);
            this.grQuickEmp.TabIndex = 258;
            this.grQuickEmp.TabStop = false;
            this.grQuickEmp.Visible = false;
            // 
            // DataGridQEmployee
            // 
            this.DataGridQEmployee.BackgroundColor = System.Drawing.Color.White;
            this.DataGridQEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridQEmployee.Location = new System.Drawing.Point(581, 22);
            this.DataGridQEmployee.Name = "DataGridQEmployee";
            this.DataGridQEmployee.Size = new System.Drawing.Size(339, 471);
            this.DataGridQEmployee.TabIndex = 56;
            // 
            // txtQPhone
            // 
            this.txtQPhone.Location = new System.Drawing.Point(390, 386);
            this.txtQPhone.Name = "txtQPhone";
            this.txtQPhone.Size = new System.Drawing.Size(185, 26);
            this.txtQPhone.TabIndex = 54;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(336, 389);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 18);
            this.label40.TabIndex = 55;
            this.label40.Text = "Phone";
            // 
            // PicQEmpPhoto
            // 
            this.PicQEmpPhoto.BackColor = System.Drawing.Color.White;
            this.PicQEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicQEmpPhoto.Image = global::MyEasyPay.Properties.Resources.MEMP;
            this.PicQEmpPhoto.Location = new System.Drawing.Point(416, 22);
            this.PicQEmpPhoto.Name = "PicQEmpPhoto";
            this.PicQEmpPhoto.Size = new System.Drawing.Size(159, 184);
            this.PicQEmpPhoto.TabIndex = 53;
            this.PicQEmpPhoto.TabStop = false;
            this.PicQEmpPhoto.Click += new System.EventHandler(this.PicQEmpPhoto_Click);
            // 
            // BtnQickBack
            // 
            this.BtnQickBack.Location = new System.Drawing.Point(206, 468);
            this.BtnQickBack.Name = "BtnQickBack";
            this.BtnQickBack.Size = new System.Drawing.Size(75, 29);
            this.BtnQickBack.TabIndex = 44;
            this.BtnQickBack.Text = "Back";
            this.BtnQickBack.UseVisualStyleBackColor = true;
            this.BtnQickBack.Click += new System.EventHandler(this.BtnQickBack_Click);
            // 
            // BtnQuickSave
            // 
            this.BtnQuickSave.Location = new System.Drawing.Point(130, 468);
            this.BtnQuickSave.Name = "BtnQuickSave";
            this.BtnQuickSave.Size = new System.Drawing.Size(75, 29);
            this.BtnQuickSave.TabIndex = 43;
            this.BtnQuickSave.Text = "Save";
            this.BtnQuickSave.UseVisualStyleBackColor = true;
            this.BtnQuickSave.Click += new System.EventHandler(this.BtnQuickSave_Click);
            // 
            // CmbQShiftName
            // 
            this.CmbQShiftName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbQShiftName.FormattingEnabled = true;
            this.CmbQShiftName.Location = new System.Drawing.Point(130, 384);
            this.CmbQShiftName.Name = "CmbQShiftName";
            this.CmbQShiftName.Size = new System.Drawing.Size(191, 26);
            this.CmbQShiftName.TabIndex = 40;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(45, 388);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(76, 18);
            this.label38.TabIndex = 42;
            this.label38.Text = "Shift Name";
            // 
            // CmbQWeekOff
            // 
            this.CmbQWeekOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbQWeekOff.FormattingEnabled = true;
            this.CmbQWeekOff.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednsday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.CmbQWeekOff.Location = new System.Drawing.Point(130, 428);
            this.CmbQWeekOff.Name = "CmbQWeekOff";
            this.CmbQWeekOff.Size = new System.Drawing.Size(177, 26);
            this.CmbQWeekOff.TabIndex = 39;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(56, 432);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 18);
            this.label39.TabIndex = 41;
            this.label39.Text = "Week Off";
            // 
            // txtQAddress
            // 
            this.txtQAddress.Location = new System.Drawing.Point(130, 297);
            this.txtQAddress.Name = "txtQAddress";
            this.txtQAddress.Size = new System.Drawing.Size(262, 68);
            this.txtQAddress.TabIndex = 35;
            this.txtQAddress.Text = "";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(63, 297);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 18);
            this.label28.TabIndex = 38;
            this.label28.Text = "Address";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(259, 266);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(72, 22);
            this.radioButton1.TabIndex = 33;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "FeMale";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(259, 228);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(57, 22);
            this.radioButton2.TabIndex = 31;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Male";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // DtpQDOJ
            // 
            this.DtpQDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpQDOJ.Location = new System.Drawing.Point(130, 264);
            this.DtpQDOJ.Name = "DtpQDOJ";
            this.DtpQDOJ.Size = new System.Drawing.Size(110, 26);
            this.DtpQDOJ.TabIndex = 30;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(21, 268);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 18);
            this.label30.TabIndex = 37;
            this.label30.Text = "Date of Joining";
            // 
            // DtpQDOB
            // 
            this.DtpQDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpQDOB.Location = new System.Drawing.Point(130, 226);
            this.DtpQDOB.Name = "DtpQDOB";
            this.DtpQDOB.Size = new System.Drawing.Size(111, 26);
            this.DtpQDOB.TabIndex = 28;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(35, 230);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(86, 18);
            this.label31.TabIndex = 36;
            this.label31.Text = "Date of Birth";
            // 
            // txtQFatherName
            // 
            this.txtQFatherName.Location = new System.Drawing.Point(130, 190);
            this.txtQFatherName.Name = "txtQFatherName";
            this.txtQFatherName.Size = new System.Drawing.Size(262, 26);
            this.txtQFatherName.TabIndex = 27;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(24, 194);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(97, 18);
            this.label32.TabIndex = 34;
            this.label32.Text = "Father\'s Name";
            // 
            // CmbQDesingnation
            // 
            this.CmbQDesingnation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbQDesingnation.FormattingEnabled = true;
            this.CmbQDesingnation.Location = new System.Drawing.Point(130, 158);
            this.CmbQDesingnation.Name = "CmbQDesingnation";
            this.CmbQDesingnation.Size = new System.Drawing.Size(262, 26);
            this.CmbQDesingnation.TabIndex = 26;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(32, 162);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(89, 18);
            this.label33.TabIndex = 32;
            this.label33.Text = "Desingnation";
            // 
            // txtQPunchCode
            // 
            this.txtQPunchCode.Location = new System.Drawing.Point(130, 89);
            this.txtQPunchCode.Name = "txtQPunchCode";
            this.txtQPunchCode.Size = new System.Drawing.Size(135, 26);
            this.txtQPunchCode.TabIndex = 23;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(54, 93);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 18);
            this.label34.TabIndex = 29;
            this.label34.Text = "Punch No";
            // 
            // txtQEmpCode
            // 
            this.txtQEmpCode.Location = new System.Drawing.Point(130, 56);
            this.txtQEmpCode.Name = "txtQEmpCode";
            this.txtQEmpCode.Size = new System.Drawing.Size(135, 26);
            this.txtQEmpCode.TabIndex = 21;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(16, 60);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 18);
            this.label35.TabIndex = 25;
            this.label35.Text = "Employee Code";
            // 
            // txtQEmpName
            // 
            this.txtQEmpName.Location = new System.Drawing.Point(130, 22);
            this.txtQEmpName.Name = "txtQEmpName";
            this.txtQEmpName.Size = new System.Drawing.Size(262, 26);
            this.txtQEmpName.TabIndex = 20;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(110, 18);
            this.label36.TabIndex = 22;
            this.label36.Text = "Employee Name";
            // 
            // CmbQDepartment
            // 
            this.CmbQDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbQDepartment.FormattingEnabled = true;
            this.CmbQDepartment.Location = new System.Drawing.Point(130, 126);
            this.CmbQDepartment.Name = "CmbQDepartment";
            this.CmbQDepartment.Size = new System.Drawing.Size(262, 26);
            this.CmbQDepartment.TabIndex = 24;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(38, 130);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(83, 18);
            this.label37.TabIndex = 19;
            this.label37.Text = "Department";
            // 
            // FrmEmpMast
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(926, 537);
            this.Controls.Add(this.panFooter);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grQuickEmp);
            this.Controls.Add(this.frpfnttt);
            this.MaximizeBox = false;
            this.Name = "FrmEmpMast";
            this.Text = "Employee Master";
            this.Load += new System.EventHandler(this.FrmEmpMast_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).EndInit();
            this.panFooter.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).EndInit();
            this.frpfnttt.ResumeLayout(false);
            this.frpfnttt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fntgrid)).EndInit();
            this.grQuickEmp.ResumeLayout(false);
            this.grQuickEmp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridQEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicQEmpPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panFooter;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmpcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDesinganation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpDOJ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox txtInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox txtAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton radioMale;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbWeekOff;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbEmpType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbShiftName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDispensary;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtEmpCardNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUANNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPFNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtESINo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtEL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCL;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAadharNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPanNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox picEmpPhoto;
        private System.Windows.Forms.DataGridView DataGridEmployee;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Panel frpfnttt;
        private System.Windows.Forms.DataGridView fntgrid;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.DateTimePicker Frmdt;
        private System.Windows.Forms.ComboBox cmgfntbox;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtclose;
        private System.Windows.Forms.TextBox txtleft;
        private System.Windows.Forms.TextBox txtjoin;
        private System.Windows.Forms.TextBox txtop;
        private System.Windows.Forms.ComboBox cmbapp;
        private System.Windows.Forms.CheckBox IsAppriser;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label Appriser;
        private System.Windows.Forms.TextBox txtmailid;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox grQuickEmp;
        private System.Windows.Forms.ComboBox CmbQShiftName;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox CmbQWeekOff;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.RichTextBox txtQAddress;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.DateTimePicker DtpQDOJ;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker DtpQDOB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtQFatherName;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox CmbQDesingnation;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtQPunchCode;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtQEmpCode;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtQEmpName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox CmbQDepartment;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button BtnQickBack;
        private System.Windows.Forms.Button BtnQuickSave;
        private System.Windows.Forms.PictureBox PicQEmpPhoto;
        private System.Windows.Forms.TextBox txtQPhone;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button BtnQuickEmp;
        private System.Windows.Forms.DataGridView DataGridQEmployee;
    }
}