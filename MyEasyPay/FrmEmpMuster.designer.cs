﻿namespace MyEasyPay
{
    partial class FrmEmpMuster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.cmgfntbox = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.DataGridEmpPay = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbmonth = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWDays = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmpPay)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(14, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(86, 19);
            this.lblName.TabIndex = 20;
            this.lblName.Text = "Department";
            // 
            // cmgfntbox
            // 
            this.cmgfntbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmgfntbox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmgfntbox.FormattingEnabled = true;
            this.cmgfntbox.Location = new System.Drawing.Point(103, 12);
            this.cmgfntbox.Name = "cmgfntbox";
            this.cmgfntbox.Size = new System.Drawing.Size(223, 27);
            this.cmgfntbox.TabIndex = 19;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(630, 481);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 22;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // DataGridEmpPay
            // 
            this.DataGridEmpPay.BackgroundColor = System.Drawing.Color.White;
            this.DataGridEmpPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmpPay.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridEmpPay.Location = new System.Drawing.Point(9, 74);
            this.DataGridEmpPay.Name = "DataGridEmpPay";
            this.DataGridEmpPay.Size = new System.Drawing.Size(697, 401);
            this.DataGridEmpPay.TabIndex = 23;
            this.DataGridEmpPay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridEmpPay_CellClick_2);
            this.DataGridEmpPay.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridEmpPay_CellValueChanged_1);
            this.DataGridEmpPay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridEmpPay_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(45, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 19);
            this.label7.TabIndex = 25;
            this.label7.Text = "Month ";
            // 
            // cmbmonth
            // 
            this.cmbmonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbmonth.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbmonth.FormattingEnabled = true;
            this.cmbmonth.Location = new System.Drawing.Point(103, 45);
            this.cmbmonth.Name = "cmbmonth";
            this.cmbmonth.Size = new System.Drawing.Size(130, 27);
            this.cmbmonth.TabIndex = 26;
            this.cmbmonth.SelectedIndexChanged += new System.EventHandler(this.Cmbmonth_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(239, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 27;
            this.button1.Text = "Muster Generations";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(439, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "Working Days";
            // 
            // TxtWDays
            // 
            this.TxtWDays.Enabled = false;
            this.TxtWDays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWDays.Location = new System.Drawing.Point(540, 18);
            this.TxtWDays.Name = "TxtWDays";
            this.TxtWDays.Size = new System.Drawing.Size(100, 26);
            this.TxtWDays.TabIndex = 30;
            // 
            // FrmEmpMuster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(717, 513);
            this.Controls.Add(this.TxtWDays);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbmonth);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.DataGridEmpPay);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.cmgfntbox);
            this.Name = "FrmEmpMuster";
            this.Text = "Muster Details";
            this.Load += new System.EventHandler(this.FrmEmpMuster_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmpPay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cmgfntbox;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView DataGridEmpPay;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbmonth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtWDays;
    }
}