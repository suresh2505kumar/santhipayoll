﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace MyEasyPay
{
    public partial class FrmEmp : Form
    {
        public FrmEmp()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        
        private void FrmEmp_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            LoadMasters();
            //LoadButton(4);
            frpfnttt.Visible = true;
            grFront.Visible = false;
            grBack.Visible = false;
            panFooter.Visible = false;
            var theDate = DateTime.Now.AddMonths(-1);
            theDate.ToString("MM/yyyy");

            Frmdt.Format = DateTimePickerFormat.Custom;

            Frmdt.CustomFormat = theDate.ToString("MM/yyyy");
            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
           
         
            Loadsum();
        }
        public DataTable GetData1(string Procedurename, int Month, int year, DateTime date)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetData(string Procedurename)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        private void Loadsum()
        {


            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            conn.Close();
            conn.Open();
            string qur = "exec EMPSTRENGTH_SP " + str9.Month + "," + str9.Year + ",'" + str9 + "'";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);





            fntgrid.AutoGenerateColumns = false;
            fntgrid.Refresh();
            fntgrid.DataSource = null;
            fntgrid.Rows.Clear();


            fntgrid.ColumnCount = tab.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tab.Columns)
            {
                fntgrid.Columns[Genclass.i].Name = column.ColumnName;
                fntgrid.Columns[Genclass.i].HeaderText = column.ColumnName;
                fntgrid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }


            fntgrid.ColumnCount = 6;


            fntgrid.Columns[0].Name = "DeptId";
            fntgrid.Columns[0].HeaderText = "DeptId";
            fntgrid.Columns[0].DataPropertyName = "DeptId";
            fntgrid.Columns[0].Visible = false;

            fntgrid.Columns[1].Name = "DeptName";
            fntgrid.Columns[1].HeaderText = "Category";
            fntgrid.Columns[1].DataPropertyName = "DeptName";
            fntgrid.Columns[1].Width = 120;

            fntgrid.Columns[2].Name = "STRENGTH";
            fntgrid.Columns[2].HeaderText = "OP Strength";
            fntgrid.Columns[2].DataPropertyName = "STRENGTH";
            fntgrid.Columns[2].Width = 120;

            fntgrid.Columns[3].Name = "ADDITION";
            fntgrid.Columns[3].HeaderText = "Joined";
            fntgrid.Columns[3].DataPropertyName = "ADDITION";
            fntgrid.Columns[3].Width = 120;
            fntgrid.Columns[4].Name = "DELETION";
            fntgrid.Columns[4].HeaderText = "Releived";
            fntgrid.Columns[4].DataPropertyName = "DELETION";
            fntgrid.Columns[4].Width = 120;

            fntgrid.Columns[5].Name = "TSTRENGTH";
            fntgrid.Columns[5].HeaderText = "Total";
            fntgrid.Columns[5].DataPropertyName = "TSTRENGTH";
            fntgrid.Columns[5].Width = 120;






            fntgrid.DataSource = tab;


            //tottal1();


        }



        private void FillEMpstr(DataTable dataTable)
        {
            try
            {


                fntgrid.DataSource = null;
                fntgrid.AutoGenerateColumns = false;
                fntgrid.ColumnCount = 6;
                fntgrid.Columns[0].Name = "DeptId";
                fntgrid.Columns[0].HeaderText = "DeptId";
                fntgrid.Columns[0].DataPropertyName = "DeptId";
                fntgrid.Columns[0].Visible = false;

                fntgrid.Columns[1].Name = "DeptName";
                fntgrid.Columns[1].HeaderText = "Category";
                fntgrid.Columns[1].DataPropertyName = "DeptName";
                fntgrid.Columns[1].Width = 120;

                fntgrid.Columns[2].Name = "STRENGTH";
                fntgrid.Columns[2].HeaderText = "OP Strength";
                fntgrid.Columns[2].DataPropertyName = "STRENGTH";
                fntgrid.Columns[2].Width = 120;

                fntgrid.Columns[3].Name = "ADDITION";
                fntgrid.Columns[3].HeaderText = "Joined";
                fntgrid.Columns[3].DataPropertyName = "ADDITION";
                fntgrid.Columns[3].Width = 120;
                fntgrid.Columns[4].Name = "DELETION";
                fntgrid.Columns[4].HeaderText = "Releived";
                fntgrid.Columns[4].DataPropertyName = "DELETION";
                fntgrid.Columns[4].Width = 120;

                fntgrid.Columns[5].Name = "TSTRENGTH";
                fntgrid.Columns[5].HeaderText = "Total";
                fntgrid.Columns[5].DataPropertyName = "TSTRENGTH";
                fntgrid.Columns[5].Width = 120;






                fntgrid.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillEMp(DataTable dataTable)
        {
            try
            {

                DataGridEmployee.DataSource = null;
                DataGridEmployee.AutoGenerateColumns = false;
                DataGridEmployee.ColumnCount = 33;
                DataGridEmployee.Columns[0].Name = "EmpId";
                DataGridEmployee.Columns[0].HeaderText = "EmpId";
                DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
                DataGridEmployee.Columns[0].Visible = false;

                DataGridEmployee.Columns[1].Name = "EmpCode";
                DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
                DataGridEmployee.Columns[1].Width = 70;
                DataGridEmployee.Columns[2].Name = "EmpName";
                DataGridEmployee.Columns[2].HeaderText = "Employe Name";
                DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
                DataGridEmployee.Columns[2].Width = 120;

                //DataGridEmployee.Columns[2].Name = "EmpCode";
                //DataGridEmployee.Columns[2].HeaderText = "EmpCode";
                //DataGridEmployee.Columns[2].DataPropertyName = "EmpCode";
                //DataGridEmployee.Columns[2].Width = 70;

                DataGridEmployee.Columns[3].Name = "ShiftName";
                DataGridEmployee.Columns[3].HeaderText = "ShiftName";
                DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
                DataGridEmployee.Columns[3].Visible = false;
                DataGridEmployee.Columns[4].Name = "DeptId";
                DataGridEmployee.Columns[4].HeaderText = "DeptId";
                DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
                DataGridEmployee.Columns[4].Visible = false;

                DataGridEmployee.Columns[5].Name = "DesId";
                DataGridEmployee.Columns[5].HeaderText = "DesId";
                DataGridEmployee.Columns[5].DataPropertyName = "DesId";
                DataGridEmployee.Columns[5].Visible = false;

                DataGridEmployee.Columns[6].Name = "Grid";
                DataGridEmployee.Columns[6].HeaderText = "Grid";
                DataGridEmployee.Columns[6].DataPropertyName = "Grid";
                DataGridEmployee.Columns[6].Visible = false;

                DataGridEmployee.Columns[7].Name = "GrName";
                DataGridEmployee.Columns[7].HeaderText = "GrName";
                DataGridEmployee.Columns[7].DataPropertyName = "GrName";
                DataGridEmployee.Columns[7].Visible = false;

                DataGridEmployee.Columns[8].Name = "ShiftId";
                DataGridEmployee.Columns[8].HeaderText = "ShiftId";
                DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
                DataGridEmployee.Columns[8].Visible = false;

                DataGridEmployee.Columns[9].Name = "EmpTypeId";
                DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
                DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
                DataGridEmployee.Columns[9].Visible = false;

                DataGridEmployee.Columns[10].Name = "FName";
                DataGridEmployee.Columns[10].HeaderText = "FatherName";
                DataGridEmployee.Columns[10].DataPropertyName = "FName";
                DataGridEmployee.Columns[10].Width = 120;

                DataGridEmployee.Columns[11].Name = "DOB";
                DataGridEmployee.Columns[11].HeaderText = "DOB";
                DataGridEmployee.Columns[11].DataPropertyName = "DOB";
                DataGridEmployee.Columns[11].Visible = false;

                DataGridEmployee.Columns[12].Name = "DOJ";
                DataGridEmployee.Columns[12].HeaderText = "DOJ";
                DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
                DataGridEmployee.Columns[12].Visible = false;

                DataGridEmployee.Columns[13].Name = "Sex";
                DataGridEmployee.Columns[13].HeaderText = "Sex";
                DataGridEmployee.Columns[13].DataPropertyName = "Sex";
                DataGridEmployee.Columns[13].Visible = false;

                DataGridEmployee.Columns[14].Name = "EmpAddress";
                DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
                DataGridEmployee.Columns[14].DataPropertyName = "Street";
                DataGridEmployee.Columns[14].Visible = false;

                DataGridEmployee.Columns[15].Name = "City";
                DataGridEmployee.Columns[15].HeaderText = "City";
                DataGridEmployee.Columns[15].DataPropertyName = "City";
                DataGridEmployee.Columns[15].Visible = false;

                DataGridEmployee.Columns[16].Name = "Phone";
                DataGridEmployee.Columns[16].HeaderText = "Phone";
                DataGridEmployee.Columns[16].DataPropertyName = "Phone";
                DataGridEmployee.Columns[16].Visible = false;

                DataGridEmployee.Columns[17].Name = "Information";
                DataGridEmployee.Columns[17].HeaderText = "Information";
                DataGridEmployee.Columns[17].DataPropertyName = "Information";
                DataGridEmployee.Columns[17].Visible = false;
                DataGridEmployee.Columns[18].Name = "Dispensary";
                DataGridEmployee.Columns[18].HeaderText = "Dispensary";
                DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
                DataGridEmployee.Columns[18].Visible = false;

                DataGridEmployee.Columns[19].Name = "WeekOff";
                DataGridEmployee.Columns[19].HeaderText = "WeekOff";
                DataGridEmployee.Columns[19].DataPropertyName = "woff";
                DataGridEmployee.Columns[19].Visible = false;

                DataGridEmployee.Columns[20].Name = "CL";
                DataGridEmployee.Columns[20].HeaderText = "CL";
                DataGridEmployee.Columns[20].DataPropertyName = "CL";
                DataGridEmployee.Columns[20].Visible = false;
                DataGridEmployee.Columns[21].Name = "EL";
                DataGridEmployee.Columns[21].HeaderText = "EL";
                DataGridEmployee.Columns[21].DataPropertyName = "EL";
                DataGridEmployee.Columns[21].Visible = false;

                DataGridEmployee.Columns[22].Name = "ESINo";
                DataGridEmployee.Columns[22].HeaderText = "ESINo";
                DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
                DataGridEmployee.Columns[22].Visible = false;

                DataGridEmployee.Columns[23].Name = "PFNo";
                DataGridEmployee.Columns[23].HeaderText = "PFNo";
                DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
                DataGridEmployee.Columns[23].Visible = false;

                DataGridEmployee.Columns[24].Name = "UANNo";
                DataGridEmployee.Columns[24].HeaderText = "UANNo";
                DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
                DataGridEmployee.Columns[24].Visible = false;

                DataGridEmployee.Columns[25].Name = "PANNo";
                DataGridEmployee.Columns[25].HeaderText = "PANNo";
                DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
                DataGridEmployee.Columns[25].Visible = false;

                DataGridEmployee.Columns[26].Name = "AadharNo";
                DataGridEmployee.Columns[26].HeaderText = "AadharNo";
                DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
                DataGridEmployee.Columns[26].Visible = false;

                DataGridEmployee.Columns[27].Name = "empphoto";
                DataGridEmployee.Columns[27].HeaderText = "empphoto";
                DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
                DataGridEmployee.Columns[27].Visible = false;

                DataGridEmployee.Columns[28].Name = "DesName";
                DataGridEmployee.Columns[28].HeaderText = "DesName";
                DataGridEmployee.Columns[28].DataPropertyName = "DesName";
                DataGridEmployee.Columns[28].Visible = false; Width = 100;


                DataGridEmployee.Columns[29].Name = "DeptName";
                DataGridEmployee.Columns[29].HeaderText = "DeptName";
                DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
                DataGridEmployee.Columns[29].Width = 120;


                DataGridEmployee.Columns[30].Name = "Grade";
                DataGridEmployee.Columns[30].HeaderText = "Grade";
                DataGridEmployee.Columns[30].DataPropertyName = "Grade";
                DataGridEmployee.Columns[30].Width = 120;


                DataGridEmployee.Columns[31].Name = "TName";
                DataGridEmployee.Columns[31].HeaderText = "TName";
                DataGridEmployee.Columns[31].DataPropertyName = "TName";
                DataGridEmployee.Columns[31].Visible = false;

                DataGridEmployee.Columns[32].Name = "CardNo";
                DataGridEmployee.Columns[32].HeaderText = "CardNo";
                DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
                DataGridEmployee.Columns[32].Width = 100;


                DataGridEmployee.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                DataTable dtDes = ds.Tables[1];
                DataTable DtEmpType = ds.Tables[2];
                DataTable dtGrade = ds.Tables[3];
                DataTable dtShift = ds.Tables[4];
                cmbDepartment.DataSource = null;
                cmbDepartment.DisplayMember = "DeptName";
                cmbDepartment.ValueMember = "DeptId";
                cmbDepartment.DataSource = dtDept;

                cmbDesinganation.DataSource = null;
                cmbDesinganation.DisplayMember = "DesName";
                cmbDesinganation.ValueMember = "DesId";
                cmbDesinganation.DataSource = dtDes;

                cmbCategory.DataSource = null;
                cmbCategory.DisplayMember = "GrName";
                cmbCategory.ValueMember = "GrId";
                cmbCategory.DataSource = dtGrade;

                cmbEmpType.DataSource = null;
                cmbEmpType.DisplayMember = "TName";
                cmbEmpType.ValueMember = "TId";
                cmbEmpType.DataSource = DtEmpType;

                cmbShiftName.DataSource = null;
                cmbShiftName.DisplayMember = "ShiftName";
                cmbShiftName.ValueMember = "ShiftId";
                cmbShiftName.DataSource = dtShift;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frpfnttt.Visible = false;
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            DataTable dt = GetData("SP_GetEmp_Mast");
            FillEMp(dt);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }
    }
}
