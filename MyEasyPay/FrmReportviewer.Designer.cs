﻿namespace MyEasyPay
{
    partial class FrmReportviewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CryReportviewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // CryReportviewer
            // 
            this.CryReportviewer.ActiveViewIndex = -1;
            this.CryReportviewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CryReportviewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.CryReportviewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CryReportviewer.Location = new System.Drawing.Point(0, 0);
            this.CryReportviewer.Name = "CryReportviewer";
            this.CryReportviewer.Size = new System.Drawing.Size(986, 543);
            this.CryReportviewer.TabIndex = 0;
            this.CryReportviewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            this.CryReportviewer.Load += new System.EventHandler(this.CryReportviewer_Load);
            // 
            // FrmReportviewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 543);
            this.Controls.Add(this.CryReportviewer);
            this.Name = "FrmReportviewer";
            this.Text = "Report";
            this.Load += new System.EventHandler(this.FrmReportviewer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer CryReportviewer;
    }
}