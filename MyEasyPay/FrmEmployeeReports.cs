﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;


namespace MyEasyPay
{
    public partial class FrmEmployeeReports : Form
    {
        public FrmEmployeeReports()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + GeneralParameters.ServerName + ";User Id=" + GeneralParameters.DbUserID + ";Password=" + GeneralParameters.Password + ";Initial Catalog=" + GeneralParameters.DbName + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
        }
        private void Titlep4()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
        }

        private void Titlep3()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
            DatagridCategory.Columns.Add(checkColumn);

        }
        private void Titlep2()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
            DataGridDepartment.Columns.Add(checkColumn);


        }
        private void FrmEmployeeReports_Load(object sender, EventArgs e)
        {

            if (GeneralParametrs.MenyKey == 5)
            {
                Genclass.Dtype = 1;
                this.Name = "Employee Reports";
                //cboop.Visible = false;
                //comboBox2.Visible = true;
                BtnAttendanceProcess.Visible = false;
                CmbTag.Visible = false;
                cboop.Visible = true;
            }
            else if (GeneralParametrs.MenyKey == 6 || GeneralParametrs.MenyKey == 0)
            {
                Genclass.Dtype = 2;
                this.Name = "Daily Reports";
                CmbTag.Visible = true;
                cboop.Visible = false;
            }

            DataGridDepartment.RowHeadersVisible = false;
            DatagridCategory.RowHeadersVisible = false;
            qur.Connection = conn;
            Frmdt.Value = DateTime.Now;
            Dept();
            if (Genclass.Dtype == 1)
            {
                Label1.Visible = false;
                Frmdt.Visible = false;
                label5.Visible = false;
                CmbTag.Visible = false;
                DataGridShift.Visible = false;
                ChckShift.Visible = false;
            }
            else
            {
                label2.Visible = false;
                cboop.Visible = false;
                Label1.Visible = true;
                Frmdt.Visible = true;
                label5.Visible = true;
                CmbTag.Visible = true;
                DataGridShift.Visible = true;
                ChckShift.Visible = true;
            }
            Shift();
            chckDepartment.Checked = false;
        }

        public void Shift()
        {
            string qur = "select ShiftName, ShiftId from Shift_Mast ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            DataGridShift.DataSource = null;
            DataGridShift.ColumnCount = 2;
            DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
            {
                HeaderText = "Chck",
                Name = "Chck",
                Width = 50
            };
            DataGridShift.Columns.Insert(0, dataGridViewCheckBoxColumn);

            DataGridShift.Columns[1].Name = "Shift";
            DataGridShift.Columns[1].HeaderText = "Shift";
            DataGridShift.Columns[1].DataPropertyName = "ShiftName";

            DataGridShift.Columns[2].Name = "Shift";
            DataGridShift.Columns[2].HeaderText = "Shift";
            DataGridShift.Columns[2].DataPropertyName = "ShiftId";
            DataGridShift.Columns[2].Visible = false;
            DataGridShift.DataSource = tab;
        }
        private void DataGridDepartment_MouseClick(object sender, MouseEventArgs e)
        {

        }

        public void Dept()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDept_Mast", conn);
                DataGridDepartment.DataSource = null;
                DataGridDepartment.AutoGenerateColumns = false;
                DataGridDepartment.ColumnCount = 2;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 50
                };
                DataGridDepartment.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridDepartment.Columns[1].Name = "Department";
                DataGridDepartment.Columns[1].HeaderText = "Department";
                DataGridDepartment.Columns[1].DataPropertyName = "DeptName";
                DataGridDepartment.Columns[1].Width = 190;

                DataGridDepartment.Columns[2].Name = "Department";
                DataGridDepartment.Columns[2].HeaderText = "Department";
                DataGridDepartment.Columns[2].DataPropertyName = "DeptId";
                DataGridDepartment.Columns[2].Visible = false;
                DataGridDepartment.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        public void Category()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[2].Value.ToString());
            string qur = "select distinct  c.TName as Categroy from emp_mast a inner join Dept_Mast b on a.DeptId=b.DeptId  inner join emp_type c on a.eg=c.TNAME left join tmpitid nn on a.deptid=nn.id ";
            Genclass.cmd = new SqlCommand(qur, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            DatagridCategory.AutoGenerateColumns = false;
            DatagridCategory.Refresh();
            DatagridCategory.DataSource = null;
            DatagridCategory.Rows.Clear();

            DatagridCategory.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                DatagridCategory.Columns[Genclass.i].Name = column.ColumnName;
                DatagridCategory.Columns[Genclass.i].HeaderText = column.ColumnName;
                DatagridCategory.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            DatagridCategory.Columns[1].Width = 115;

            DatagridCategory.DataSource = tap;
        }
        public void Group()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[1].Value.ToString());
            string qur1 = "select grname as GroupName,Grid from Gr_Mast ";
            Genclass.cmd = new SqlCommand(qur1, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
        }

        public void Esip()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[1].Value.ToString());
            string qur = "select distinct type  from emp_type ";


            Genclass.cmd = new SqlCommand(qur, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
        }

        private void DataGridDepartment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                GeneralParameters.Department = string.Empty;
                GeneralParameters.Desingnation = string.Empty;
                GeneralParameters.Shift = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Department == string.Empty)
                        {
                            GeneralParameters.Department = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Department = GeneralParameters.Department + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

                foreach (DataGridViewRow dataGridRow in DatagridCategory.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Desingnation == string.Empty)
                        {
                            GeneralParameters.Desingnation = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Desingnation = GeneralParameters.Desingnation + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

                foreach (DataGridViewRow dataGridRow in DataGridShift.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Shift == string.Empty)
                        {
                            GeneralParameters.Shift = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Shift = GeneralParameters.Shift + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }
              
                if (CmbTag.Visible == true)
                {
                    DailyReport();
                }
                else
                {
                    EmployeeReport();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void EmployeeReport()
        {
            try
            {
                if (cboop.Text == "Employee List")
                {
                    if (GeneralParameters.Department.Length > 2)
                    {
                        GeneralParameters.Department = "1000";
                    }
                    GeneralParameters.ReportId = 7;
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (cboop.Text == "New Join Employee List")
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void DailyReport()
        {
            try
            {
                if (CmbTag.Text == "Daily Punch Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 1;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "P";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Irregular Punch Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 2;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "Ir";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Absent List")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 2;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "Ab";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Strength Report")
                {
                    //if (GeneralParameters.Shift.Length > 2)
                    //{
                    //    GeneralParameters.Shift = "1000";
                    //}
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 3;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "In Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 4;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Worked Hour Report (< 8 Hrs)")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 5;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Lunch Break Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 6;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Muster Roll Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 8;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "MusterRoll (1 to 15 days)" || CmbTag.Text == "MusterRoll (16 to 31 days)")
                {
                    GeneralParameters.MusterTag = 2;
                    FrmMuster25B frmMB = new FrmMuster25B
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    frmMB.ShowDialog();
                }
                else if (CmbTag.Text == "Over Time Muster Roll Report")
                {
                    GeneralParameters.MusterTag = 2;
                    FrmMuster25B frmMB = new FrmMuster25B
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    frmMB.ShowDialog();
                }
                else if (CmbTag.Text == "Late In Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 9;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Over Time Report")
                {
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    string TableName = "ATT_REG" + date.Month.ToString("00") + "";
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@TBL",TableName),
                        new SqlParameter("@DT",date.ToString("yyyy-MM-dd"))
                    };
                    DataTable table = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_OTREPORT", sqlParameters, conn);
                    if(table.Rows.Count > 0)
                    {
                        Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                        Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                        Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                        app.Visible = false;
                        worksheet = workbook.Sheets["Sheet1"];
                        worksheet = workbook.ActiveSheet;
                        worksheet.Name = "OT Entry";

                        Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                        Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                        Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                        Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                        Microsoft.Office.Interop.Excel.Range row5 = worksheet.Rows.Cells[1, 5];
                        Microsoft.Office.Interop.Excel.Range row6 = worksheet.Rows.Cells[1, 6];
                        Microsoft.Office.Interop.Excel.Range row7 = worksheet.Rows.Cells[1, 7];
                        Microsoft.Office.Interop.Excel.Range row8 = worksheet.Rows.Cells[1, 8];
                        Microsoft.Office.Interop.Excel.Range row9 = worksheet.Rows.Cells[1, 9];
                        row1.Value = "EmpCode";
                        row2.Value = "EmpName";
                        row3.Value = "DepartmentName";
                        row4.Value = "IN1";
                        row5.Value = "OUT2";
                        row6.Value = "TotalHours";
                        row7.Value = "ShitHours";
                        row8.Value = "WorkedHours";
                        row9.Value = "OTMinutes";

                        int j = 2;
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            Microsoft.Office.Interop.Excel.Range EmpCode = worksheet.Rows.Cells[j, 1];
                            Microsoft.Office.Interop.Excel.Range EmpName = worksheet.Rows.Cells[j, 2];
                            Microsoft.Office.Interop.Excel.Range DepartmentName = worksheet.Rows.Cells[j, 3];
                            Microsoft.Office.Interop.Excel.Range IN1 = worksheet.Rows.Cells[j, 4];
                            Microsoft.Office.Interop.Excel.Range OUT2 = worksheet.Rows.Cells[j, 5];
                            Microsoft.Office.Interop.Excel.Range TotalHours = worksheet.Rows.Cells[j, 6];
                            Microsoft.Office.Interop.Excel.Range ShitHours = worksheet.Rows.Cells[j, 7];
                            Microsoft.Office.Interop.Excel.Range WorkedHours = worksheet.Rows.Cells[j, 8];
                            Microsoft.Office.Interop.Excel.Range OTMinutes = worksheet.Rows.Cells[j, 9];
                            EmpCode.Value = table.Rows[i]["EMPCODE"].ToString();
                            EmpName.Value = table.Rows[i]["EMPNAME"].ToString();
                            DepartmentName.Value = table.Rows[i]["DEPTNAME"].ToString();
                            IN1.Value = table.Rows[i]["IN1"].ToString();
                            OUT2.Value = table.Rows[i]["OUT2"].ToString();
                            TotalHours.Value = table.Rows[i]["thrs"].ToString();
                            ShitHours.Value = table.Rows[i]["SHIFTHOURS"].ToString();
                            WorkedHours.Value = table.Rows[i]["WORKEDHRS"].ToString();
                            OTMinutes.Value = table.Rows[i]["OTMinutes"].ToString();
                            j++;
                        }
                        MessageBox.Show("Completed Suucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        app.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                   
                }
                else if (CmbTag.Text == "Late Summary")
                {
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    string TableName = "ATT_REG" + date.Month.ToString("00") + "";
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@TBL",TableName),
                        new SqlParameter("@DT",date.ToString("yyyy-MM-dd"))
                    };
                    DataTable table = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GET_LATESUMMARY", sqlParameters, conn);
                    if (table.Rows.Count > 0)
                    {
                        Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                        Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                        Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                        app.Visible = false;
                        worksheet = workbook.Sheets["Sheet1"];
                        worksheet = workbook.ActiveSheet;
                        worksheet.Name = "OT Entry";

                        Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                        Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                        Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                        Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                        Microsoft.Office.Interop.Excel.Range row5 = worksheet.Rows.Cells[1, 5];
                        Microsoft.Office.Interop.Excel.Range row6 = worksheet.Rows.Cells[1, 6];
                        Microsoft.Office.Interop.Excel.Range row7 = worksheet.Rows.Cells[1, 7];
                        row1.Value = "EmpCode";
                        row2.Value = "EmpName";
                        row3.Value = "Department";
                        row4.Value = "DesName";
                        row5.Value = "Gross";
                        row6.Value = "LateCount";
                        row7.Value = "LateBy";

                        int j = 2;
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            Microsoft.Office.Interop.Excel.Range EmpCode = worksheet.Rows.Cells[j, 1];
                            Microsoft.Office.Interop.Excel.Range EmpName = worksheet.Rows.Cells[j, 2];
                            Microsoft.Office.Interop.Excel.Range DepartmentName = worksheet.Rows.Cells[j, 3];
                            Microsoft.Office.Interop.Excel.Range IN1 = worksheet.Rows.Cells[j, 4];
                            Microsoft.Office.Interop.Excel.Range OUT2 = worksheet.Rows.Cells[j, 5];
                            Microsoft.Office.Interop.Excel.Range TotalHours = worksheet.Rows.Cells[j, 6];
                            Microsoft.Office.Interop.Excel.Range ShitHours = worksheet.Rows.Cells[j, 7];
                            EmpCode.Value = table.Rows[i]["empcode"].ToString();
                            EmpName.Value = table.Rows[i]["empname"].ToString();
                            DepartmentName.Value = table.Rows[i]["deptname"].ToString();
                            IN1.Value = table.Rows[i]["desname"].ToString();
                            OUT2.Value = table.Rows[i]["gross"].ToString();
                            TotalHours.Value = table.Rows[i]["LATECOUNT"].ToString();
                            ShitHours.Value = table.Rows[i]["lateby"].ToString();
                            j++;
                        }
                        MessageBox.Show("Completed Suucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        app.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (CmbTag.Text == "Permission Report")
                {
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    string Query = "Select * from Permisson_Entry Where Perdt = '" + date.ToString("yyyy-MM-dd") + "'";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    if (dataTable.Rows.Count == 0)
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        GeneralParameters.ReportDate = Frmdt.Text;
                        GeneralParameters.ReportId = 10;
                        FrmReportviewer crv = new FrmReportviewer
                        {
                            StartPosition = FormStartPosition.CenterScreen
                        };
                        crv.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chckDepartment.Checked == true)
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = false;
                }
            }
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (s.Checked == true)
            {
                for (int i = 0; i < DatagridCategory.RowCount; i++)
                {
                    DatagridCategory[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DatagridCategory.RowCount; i++)
                {
                    DatagridCategory[0, i].Value = false;
                }
            }
        }

        private void DataGridDepartment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)DataGridDepartment.Rows[DataGridDepartment.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
            {
                ch1.Value = false;
            }
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;
                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        break;
                    }
            }
        }
        public void Allgrid()
        {
            try
            {
                string DeptId = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (DeptId == string.Empty)
                        {
                            DeptId = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            DeptId = DeptId + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid ";
            qur.ExecuteNonQuery();
            int i = 0;
            foreach (DataGridViewRow row in DataGridDepartment.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {

                    Genclass.Gbtxtid = Convert.ToInt16(DataGridDepartment.Rows[i].Cells[2].Value.ToString());

                    qur.CommandText = "delete from tmpitid where id=" + Genclass.Gbtxtid + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid values (" + Genclass.Gbtxtid + ")";
                    qur.ExecuteNonQuery();
                }
                i = i + 1;
            }
            DatagridCategory.Visible = true;
            s.Visible = true;
            Titlep3();
            Category();
            Titlep1();
            Group();
            if (Genclass.Dtype == 1)
            {
                Titlep4();
                Esip();
            }
        }
        public void Gridcategory()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid2 ";
            qur.ExecuteNonQuery();
            int k = 0;
            foreach (DataGridViewRow row in DatagridCategory.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    Genclass.bomstr = Convert.ToString(DatagridCategory.Rows[k].Cells[1].Value.ToString());
                    qur.CommandText = "delete from tmpitid2 where category='" + Genclass.bomstr + "'";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid2 values ('" + Genclass.bomstr + "')";
                    qur.ExecuteNonQuery();
                }
                k = k + 1;
            }
        }
        public void Gridgroup()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid1 ";
            qur.ExecuteNonQuery();
        }

        public void Gridpfesi()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid3 ";
            qur.ExecuteNonQuery();
        }

        private void DatagridCategory_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)DatagridCategory.Rows[DatagridCategory.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
            {
                ch1.Value = false;
            }
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
            Gridcategory();
        }

        private void BtnAttendanceProcess_Click(object sender, EventArgs e)
        {
            try
            {
                int Process = 0;
                DateTime dateTime = Convert.ToDateTime(Frmdt.Text);
                string check = "Select *from Att_Reg" + dateTime.Month.ToString("00") + " Where Dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, check, conn);
                if (dt.Rows.Count == 0)
                {
                    Process = 1;
                }
                else
                {
                    DialogResult result = MessageBox.Show("Already Attendance Processed for this date Do you Want Re process this date?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        Process = 1;
                    }
                    else
                    {
                        Process = 0;
                    }
                }
                if (Process == 1)
                {
                    SqlParameter[] parameters = {
                    new SqlParameter("@Dt",dateTime.Date),
                    new SqlParameter("@Mon",dateTime.Month.ToString("00"))
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_AttProcess", parameters, conn);
                    MessageBox.Show("Process Completed Successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridCategory.DataSource = null;
                string DeptId = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (DeptId == string.Empty)
                        {
                            DeptId = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            DeptId = DeptId + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }
                if (DeptId != string.Empty)
                {
                    string Query = @"Select Distinct b.GrName,b.Grid from EMp_Mast a 
                                Inner join Gr_Mast b On a.GrId = b.Grid Where a.DeptId in (" + DeptId + ")";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    DatagridCategory.ColumnCount = 2;
                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                    {
                        HeaderText = "Chck",
                        Name = "Chck",
                        Width = 50
                    };
                    DatagridCategory.Columns.Insert(0, dataGridViewCheckBoxColumn);

                    DatagridCategory.Columns[1].Name = "GrName";
                    DatagridCategory.Columns[1].HeaderText = "Grade";
                    DatagridCategory.Columns[1].DataPropertyName = "GrName";
                    DatagridCategory.Columns[1].Width = 170;

                    DatagridCategory.Columns[2].Name = "Grid";
                    DatagridCategory.Columns[2].HeaderText = "Grid";
                    DatagridCategory.Columns[2].DataPropertyName = "Grid";
                    DatagridCategory.Columns[2].Visible = false;
                    DatagridCategory.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Information");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChckShift.Checked == true)
            {
                for (int i = 0; i < DataGridShift.RowCount; i++)
                {
                    DataGridShift[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DataGridShift.RowCount; i++)
                {
                    DataGridShift[0, i].Value = false;
                }
            }
        }

        private void DataGridDepartment_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DatagridCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridShift_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            GrLateSummary.Visible = false;
        }
    }
}
